/*
A simple in-application message bus package for Go.

One application can have multiple Buses, or any application which only needs one Bus may use the default Bus. (See the variable "Default" and the package functions "Subscribe", "Unsubscribe", "UnsubscribeAll", and "Publish".)

Messages are published via a Bus' Publish receiver func and received via a receiver. Receivers receive all Messages with names to which they are subscribed.

Names are slices of strings. Messages are delievered to receivers subscribed to names which have the message's name as a prefix. (However, individual elements in a name slice are atomic. So, a message with the name `[]string{"one", "two", "three"}` will be delivered to a receiver subscribed to `[]string{"one", "two"}`, but not to a receiver subscribed to `[]string{"one", "two", "th"}`.)

Example usage:

	package main

	import (
		"gitlab.org/AntiMS/govtt/bus"
	)

	func main() {
		b := bus.New()
		r := bus.NewReceiver()
		b.Subscribe(r, []string{"eberron", "khorvaire", "brealand"})
		go func() {
			b.Publish(&bus.Message{
				[]string{"eberron", "khorvaire", "brealand", "sharn"},
				"Founding",
			})
			b.Publish(&bus.Message{
				[]string{"eberron", "khorvaire", "brealand", "sharn"},
				"The Fall",
			})
			b.Publish(&bus.Message{
				[]string{"eberron", "khorvaire"},
				"The Last War",
			})
			b.Publish(&bus.Message{
				[]string{"eberron", "khorvaire"},
				"Day of Mourning",
			})
			b.Publish(&bus.Message{
				[]string{"eberron", "xendrik"},
				"The Giant, Dragon War",
			})
		}()
		for m := range r {
			print(m.value.(string))
			print("\n")
		}
	}

The above will output:

	Founding
	The Fall
*/
package bus

// The pointers to this type are the objects which pass through the Bus, sent to a Bus' Publish receiver func and received by receivers.
type Message struct {
	// Used for routing. A message will be delivered to receivers subscribed to a name which is a prefix of this name.
	Name []string

	// Any value wanted to be delivered to receivers.
	Value any
}

// Receives messages and routes them to receivers.
type Bus struct {
	subscribed map[chan<- *Message]struct{}
	children   map[string]*Bus
	childsem   chan struct{}
}

var senders map[<-chan *Message]chan<- *Message = make(map[<-chan *Message]chan<- *Message)

// Creates a receiver which can be subscribed to a Bus. See the Subscribe receiver function. Returns a receive-only channel which produces pointers to Messages which the Bus has routed to this receiver. One receiver may be subscribed to multiple Buses. However, UnsubscribeAll must be called on all Buses to which a receiver is subscribed before closing the receiver.
func NewReceiver() <-chan *Message {
	out := make(chan *Message, 64)
	oout := (<-chan *Message)(out)
	senders[oout] = (chan<- *Message)(out)
	return oout
}

// Disposes of a receiver. You must unsubscribe from all names on all Buses to which this receiver is subscribed before calling CloseReceiver.
func CloseReceiver(receiver <-chan *Message) {
	close(senders[receiver])
	delete(senders, receiver)
}

func (b *Bus) subscribe(sender chan<- *Message, name []string) {
	if len(name) == 0 {
		b.subscribed[sender] = struct{}{}
		return
	}

	b.childsem <- struct{}{}

	cb, ok := b.children[name[0]]
	if !ok {
		cb = New()
		b.children[name[0]] = cb
	}

	<-b.childsem

	cb.subscribe(sender, name[1:])
}

// Subscribes a given receiver to a name on this Bus.
func (b *Bus) Subscribe(receiver <-chan *Message, name []string) {
	sender := senders[receiver]

	b.subscribe(sender, name)

	n := make([]string, len(name)+2)
	n[0] = "bus"
	n[1] = "newsubscription"
	copy(n[2:], name)

	b.Publish(&Message{
		Name:  n,
		Value: sender,
	})
}

// Unsubscribes a given receiver from a specific name on this Bus. Does not unsubscribe the receiver from any child names.
func (b *Bus) Unsubscribe(receiver <-chan *Message, name []string) {
	if len(name) > 0 {
		c, ok := b.children[name[0]]
		if !ok {
			return
		}

		c.Unsubscribe(receiver, name[1:])

		return
	}

	delete(b.subscribed, senders[receiver])
}

func (b *Bus) unsubscribeAll(sender chan<- *Message, name []string) {
	if name != nil && len(name) > 0 {
		c, ok := b.children[name[0]]
		if !ok {
			return
		}

		c.unsubscribeAll(sender, name[1:])

		return
	}

	delete(b.subscribed, sender)

	for _, c := range b.children {
		c.unsubscribeAll(sender, nil)
	}
}

// Unsubscribes a given receiver from a given specific name and all child names. A nil name is equivalent to an empty name (which unsubscribes from all names in the entire Bus.)
func (b *Bus) UnsubscribeAll(receiver <-chan *Message, name []string) {
	b.unsubscribeAll(senders[receiver], name)
}

func (b *Bus) publish(name []string, message *Message) {
	for k, _ := range b.subscribed {
		go func(k chan<- *Message) {
			k <- message
		}(k)
	}

	if len(name) == 0 {
		return
	}

	c, ok := b.children[name[0]]
	if !ok {
		return
	}

	c.publish(name[1:], message)
}

// Publishes a message to the Bus.
func (b *Bus) Publish(message *Message) {
	b.publish(message.Name, message)
}

// Creates a new Bus.
func New() *Bus {
	return &Bus{
		subscribed: make(map[chan<- *Message]struct{}),
		children:   make(map[string]*Bus),
		childsem:   make(chan struct{}, 1),
	}
}

// The default Bus. Any application that has a "primary" Bus or only uses one Bus may use this Bus for that purpose.
var Default = New()

// Subscribes to the default Bus. See Bus' Subscribe receiver function.
func Subscribe(receiver <-chan *Message, name []string) {
	Default.Subscribe(receiver, name)
}

// Unsubscribes from the default Bus. See Bus' Unsubscribe receiver function.
func Unsubscribe(receiver <-chan *Message, name []string) {
	Default.Unsubscribe(receiver, name)
}

// Unsubscribes from the default Bus. See Bus' UnsubscribeAll receiver function.
func UnsubscribeAll(receiver <-chan *Message, name []string) {
	Default.UnsubscribeAll(receiver, name)
}

// Publishes to the default Bus. See Bus' Publish receiver function.
func Publish(message *Message) {
	Default.Publish(message)
}
