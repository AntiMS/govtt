-- Initial version of the database

PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE user (
	id integer primary key autoincrement,
	username text not null unique,
	password text not null,
	token text
);
CREATE TABLE campaign (
	id integer primary key autoincrement,
	scene_id integer,
	name text not null,
	FOREIGN KEY(scene_id) REFERENCES scene(id)
);
CREATE TABLE campaign_user (
	id integer primary key autoincrement,
	user_id integer not null,
	campaign_id integer not null,
	display_name text not null,
	FOREIGN KEY(user_id) REFERENCES user(id),
	FOREIGN KEY(campaign_id) REFERENCES campaign(id)
);
CREATE TABLE scene (
	id integer primary key autoincrement,
	user_id integer not null,
	campaign_id integer not null,
	name text not null,
	FOREIGN KEY(user_id) REFERENCES user(id),
	FOREIGN KEY(campaign_id) REFERENCES campaign(id)
);
CREATE TABLE image (
	id integer primary key autoincrement,
	user_id integer not null,
	campaign_id integer not null,
	name text not null,
	archived bool not null default false,
	FOREIGN KEY(user_id) REFERENCES user(id),
	FOREIGN KEY(campaign_id) REFERENCES campaign(id)
);
CREATE TABLE object (
	id integer primary key autoincrement,
	kind text not null,
	user_id integer ,
	campaign_id integer,
	scene_id integer,
	name text not null,
	FOREIGN KEY(user_id) REFERENCES user(id),
	FOREIGN KEY(campaign_id) REFERENCES campaign(id),
	FOREIGN KEY(scene_id) REFERENCES scene(id)
);
CREATE TABLE object_token (
	object_id integer primary key not null,
	round bool not null,
	color text,
	x real not null,
	y real not null,
	width integer not null default 1,
	height integer not null default 1,
	image_id integer not null,
	image_x real not null,
	image_y real not null,
	image_size real not null,
	FOREIGN KEY(object_id) REFERENCES object(id),
	FOREIGN KEY(image_id) REFERENCES image(id)
);
CREATE TABLE object_image (
	object_id integer primary key not null,
	image_id integer,
	x real not null,
	y real not null,
	image_size real not null,
	locked bool not null,
	FOREIGN KEY(object_id) REFERENCES object(id),
	FOREIGN KEY(image_id) REFERENCES image(id)
);
CREATE TABLE object_map (
	object_id integer primary key not null,
	x int not null,
	y int not null,
	width int not null,
	height int not null,
	data blob not null,
	locked bool not null,
	FOREIGN KEY(object_id) REFERENCES object(id)
);
CREATE TABLE object_label (
	object_id integer primary key not null,
	text text,
	x real not null,
	y real not null,
	size real not null,
	foreground text,
	background text,
	FOREIGN KEY(object_id) REFERENCES object(id)
);
CREATE TABLE object_drawing (
	object_id integer primary key not null,
	color text not null,
	FOREIGN KEY(object_id) REFERENCES object(id)
);
CREATE TABLE object_drawing_point (
	id integer primary key not null,
	object_id int not null,
	"order" int not null,
	x real not null,
	y real not null,
	FOREIGN KEY(object_id) REFERENCES object_drawing(object_id)
);
CREATE TABLE object_rectangle (
	object_id integer primary key not null,
	min_x real not null,
	min_y real not null,
	max_x real not null,
	max_y real not null,
	color text not null,
	locked bool not null,
	FOREIGN KEY(object_id) REFERENCES object(id)
);
DELETE FROM sqlite_sequence;
CREATE UNIQUE INDEX idx__user__username ON user(username);
CREATE INDEX idx__campaign_user__user_id ON campaign_user(user_id);
CREATE INDEX idx__campaign_user__campaign_id ON campaign_user(campaign_id);
CREATE UNIQUE INDEX idx__campaign_user__user_id__campaign_id ON campaign_user(user_id, campaign_id);
CREATE INDEX idx__object__user_id__campaign_id ON object(user_id, campaign_id);
CREATE INDEX idx__object__scene_id ON object(scene_id);
COMMIT;
