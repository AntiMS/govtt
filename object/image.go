package object

import (
	"encoding/json"
	"errors"

	"gitlab.com/AntiMS/govtt/initdb"
)

// Kind-specific data structure for the "image"-kind object. An image simply draws a rasterized (PNG, JPG, GIF, etc) image on the table.
type Image struct {
	// The id of the image.
	ImageId int `json:"image_id"`

	// X coordinate of the center of the image.
	X float64 `json:"x"`

	// Y coordinate of the center of the image.
	Y float64 `json:"y"`

	// Determines the scale of the image. Units are in average of image height and image width per table unit. A square image with an image size of 1 will be displayed at one table unit by one table unit.
	ImageSize float64 `json:"image_size"`

	// If true, the image is "locked". Dragging it to change its position requires unlocking. (This is only enforced on the front-end, however.)
	Locked bool `json:"locked"`
}

func init() {
	kinds["image"] = Kind{
		Get: func(ids []int) (map[int]any, error) {
			params := ""
			idInterfaces := make([]any, 0)
			for _, i := range ids {
				if params == "" {
					params = "?"
				} else {
					params += ", ?"
				}
				idInterfaces = append(idInterfaces, i)
			}

			rows, err := initdb.DB.Query("SELECT object_id, image_id, x, y, image_size, locked FROM object_image WHERE object_id IN ("+params+");", idInterfaces...)
			if err != nil {
				return nil, err
			}

			out := make(map[int]any, 0)
			for rows.Next() {
				var id int
				image := Image{}
				err = rows.Scan(&id, &image.ImageId, &image.X, &image.Y, &image.ImageSize, &image.Locked)
				if err != nil {
					return nil, err
				}
				out[id] = image
			}

			rows.Close()

			return out, nil
		},
		Set: func(id int, obj any) error {
			k, ok := obj.(Image)
			if !ok {
				return errors.New("Object is not an image.")
			}

			_, err := initdb.DB.Exec("UPDATE object_image SET image_id = ?, x = ?, y = ?, image_size = ?, locked = ? WHERE object_id = ?;", k.ImageId, k.X, k.Y, k.ImageSize, k.Locked, id)
			if err != nil {
				return err
			}

			return nil
		},
		New: func(id int, obj any) error {
			k, ok := obj.(Image)
			if !ok {
				return errors.New("Object is not an image.")
			}

			_, err := initdb.DB.Exec("INSERT INTO object_image (object_id, image_id, x, y, image_size, locked) VALUES(?, ?, ?, ?, ?, ?);", id, k.ImageId, k.X, k.Y, k.ImageSize, k.Locked)
			if err != nil {
				return err
			}

			return nil
		},
		Del: func(id int) error {
			_, err := initdb.DB.Exec("DELETE from object_image WHERE object_id = ?;", id)
			if err != nil {
				return err
			}

			return nil
		},
		Parse: func(d []byte) (any, error) {
			out := Image{}

			err := json.Unmarshal(d, &out)
			if err != nil {
				return nil, err
			}

			return out, nil
		},
	}
}
