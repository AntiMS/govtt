package object

import (
	"encoding/json"
	"errors"

	"gitlab.com/AntiMS/govtt/initdb"
)

// Kind-specific data structure for the "rectangle"-kind object. A rectangle draws an opaque rectangle on the table. This is useful for obscuring things not desired to be seen.
type Rectangle struct {
	// The corner with minimum X and Y coordinates.
	Min Point `json:"min"`

	// The corner with maximum X and Y coordinates.
	Max Point `json:"max"`

	// The color of the rectangle in hexadecimal color format including the leading pound sign.
	Color string `json:"color"`

	// If true, the rectangle is "locked". Altering it requires unlocking. (This is only enforced on the front-end, however.)
	Locked bool `json:"locked"`
}

func init() {
	kinds["rectangle"] = Kind{
		Get: func(ids []int) (map[int]any, error) {
			params := ""
			idInterfaces := make([]any, 0)
			for _, i := range ids {
				if params == "" {
					params = "?"
				} else {
					params += ", ?"
				}
				idInterfaces = append(idInterfaces, i)
			}

			rows, err := initdb.DB.Query("SELECT object_id, min_x, min_y, max_x, max_y, color, locked FROM object_rectangle WHERE object_id IN ("+params+");", idInterfaces...)
			if err != nil {
				return nil, err
			}

			out := make(map[int]any, 0)
			for rows.Next() {
				var id int
				rectangle := Rectangle{}
				err = rows.Scan(&id, &rectangle.Min.X, &rectangle.Min.Y, &rectangle.Max.X, &rectangle.Max.Y, &rectangle.Color, &rectangle.Locked)
				if err != nil {
					return nil, err
				}
				out[id] = rectangle
			}

			rows.Close()

			return out, nil
		},
		Set: func(id int, obj any) error {
			k, ok := obj.(Rectangle)
			if !ok {
				return errors.New("Object is not a rectangle.")
			}

			_, err := initdb.DB.Exec("UPDATE object_rectangle SET min_x = ?, min_y = ?, max_x = ?, max_y = ?, color = ?, locked = ? WHERE object_id = ?;", k.Min.X, k.Min.Y, k.Max.X, k.Max.Y, k.Color, k.Locked, id)
			if err != nil {
				return err
			}

			return nil
		},
		New: func(id int, obj any) error {
			k, ok := obj.(Rectangle)
			if !ok {
				return errors.New("Object is not a rectangle.")
			}

			_, err := initdb.DB.Exec("INSERT INTO object_rectangle (object_id, min_x, min_y, max_x, max_y, color, locked) VALUES(?, ?, ?, ?, ?, ?, ?);", id, k.Min.X, k.Min.Y, k.Max.X, k.Max.Y, k.Color, k.Locked)
			if err != nil {
				return err
			}

			return nil
		},
		Del: func(id int) error {
			_, err := initdb.DB.Exec("DELETE from object_rectangle WHERE object_id = ?;", id)
			if err != nil {
				return err
			}

			return nil
		},
		Parse: func(d []byte) (any, error) {
			out := Rectangle{}

			err := json.Unmarshal(d, &out)
			if err != nil {
				return nil, err
			}

			return out, nil
		},
	}
}
