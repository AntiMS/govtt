package object

import (
	"encoding/json"
	"errors"

	"gitlab.com/AntiMS/govtt/initdb"
)

// Kind-specific data structure for the "label"-kind object. A label renders a bit of text on the table.
type Label struct {
	// The text to display on the label.
	Text string `json:"text"`

	// The X coordinate of the center of the label.
	X float64 `json:"x"`

	// The Y coordinate of the center of the label.
	Y float64 `json:"y"`

	// The size of the label. A size of 1 means the height of the label will be one table unit.
	Size float64 `json:"size"`

	// Foreground color in hexadecimal color notation, including the leading pound sign.
	Foreground string `json:"foreground"`

	// Background color in hexadecimal color notation, including the leading pound sign.
	Background string `json:"background"`
}

func init() {
	kinds["label"] = Kind{
		Get: func(ids []int) (map[int]any, error) {
			params := ""
			idInterfaces := make([]any, 0)
			for _, i := range ids {
				if params == "" {
					params = "?"
				} else {
					params += ", ?"
				}
				idInterfaces = append(idInterfaces, i)
			}

			rows, err := initdb.DB.Query("SELECT object_id, text, x, y, size, foreground, background FROM object_label WHERE object_id IN ("+params+");", idInterfaces...)
			if err != nil {
				return nil, err
			}

			out := make(map[int]any, 0)
			for rows.Next() {
				var id int
				label := Label{}
				err = rows.Scan(&id, &label.Text, &label.X, &label.Y, &label.Size, &label.Foreground, &label.Background)
				if err != nil {
					return nil, err
				}
				out[id] = label
			}

			rows.Close()

			return out, nil
		},
		Set: func(id int, obj any) error {
			k, ok := obj.(Label)
			if !ok {
				return errors.New("Object is not a label.")
			}

			_, err := initdb.DB.Exec("UPDATE object_label SET text = ?, x = ?, y = ?, size = ?, foreground = ?, background = ? WHERE object_id = ?;", k.Text, k.X, k.Y, k.Size, k.Foreground, k.Background, id)
			if err != nil {
				return err
			}

			return nil
		},
		New: func(id int, obj any) error {
			k, ok := obj.(Label)
			if !ok {
				return errors.New("Object is not a label.")
			}

			_, err := initdb.DB.Exec("INSERT INTO object_label (object_id, text, x, y, size, foreground, background) VALUES(?, ?, ?, ?, ?, ?, ?);", id, k.Text, k.X, k.Y, k.Size, k.Foreground, k.Background)
			if err != nil {
				return err
			}

			return nil
		},
		Del: func(id int) error {
			_, err := initdb.DB.Exec("DELETE from object_label WHERE object_id = ?;", id)
			if err != nil {
				return err
			}

			return nil
		},
		Parse: func(d []byte) (any, error) {
			out := Label{}

			err := json.Unmarshal(d, &out)
			if err != nil {
				return nil, err
			}

			return out, nil
		},
	}
}
