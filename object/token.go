package object

import (
	"encoding/json"
	"errors"

	"gitlab.com/AntiMS/govtt/initdb"
)

// Kind-specific data structure for the "token"-kind object. A token is an easily-movable object of integer dimensions based on an image. Useful for representing a creature.
type Token struct {
	// True for an eliptical token. False for a rectangular token.
	Round bool `json:"round"`

	// The color of the ring on the token as a hexadecimal color code including leading pound sign. Nil for no ring.
	Color *string `json:"color"`

	// The X coordinate of the center of the token.
	X float64 `json:"x"`

	// The Y coordinate of the center of the token.
	Y float64 `json:"y"`

	// The width of the token in table units as an integer.
	Width int `json:"width"`

	// The height of the token in table units as an integer.
	Height int `json:"height"`

	// The id of the image to display in the token.
	ImageId int `json:"image_id"`

	// The X location of the center of the token relative to the center of the image in the range 0.0-1.0.
	ImageX float64 `json:"image_x"`

	// The Y location of the center of the token relative to the center of the image in the range 0.0-1.0.
	ImageY float64 `json:"image_y"`

	// Determines the size of the image. Counterintuitively, an image size of 1 is the smallest possible scale. 2 is twice as large. 3 is three times as large as 1.
	ImageSize float64 `json:"image_size"`
}

func init() {
	kinds["token"] = Kind{
		Get: func(ids []int) (map[int]any, error) {
			params := ""
			idInterfaces := make([]any, 0)
			for _, i := range ids {
				if params == "" {
					params = "?"
				} else {
					params += ", ?"
				}
				idInterfaces = append(idInterfaces, i)
			}

			rows, err := initdb.DB.Query("SELECT object_id, round, color, x, y, width, height, image_id, image_x, image_y, image_size FROM object_token WHERE object_id IN ("+params+");", idInterfaces...)
			if err != nil {
				return nil, err
			}

			out := make(map[int]any, 0)
			for rows.Next() {
				var id int
				token := Token{}
				err = rows.Scan(&id, &token.Round, &token.Color, &token.X, &token.Y, &token.Width, &token.Height, &token.ImageId, &token.ImageX, &token.ImageY, &token.ImageSize)
				if err != nil {
					return nil, err
				}
				out[id] = token
			}

			rows.Close()

			return out, nil
		},
		Set: func(id int, obj any) error {
			k, ok := obj.(Token)
			if !ok {
				return errors.New("Saved object is not a token.")
			}

			_, err := initdb.DB.Exec("UPDATE object_token SET round = ?, color = ?, x = ?, y = ?, width = ?, height = ?, image_x = ?, image_y = ?, image_size = ? WHERE object_id = ?;", k.Round, k.Color, k.X, k.Y, k.Width, k.Height, k.ImageX, k.ImageY, k.ImageSize, id)
			if err != nil {
				return err
			}

			return nil
		},
		New: func(id int, obj any) error {
			k, ok := obj.(Token)
			if !ok {
				return errors.New("New object is not a token.")
			}

			_, err := initdb.DB.Exec("INSERT INTO object_token (object_id, round, color, x, y, width, height, image_id, image_x, image_y, image_size) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);", id, k.Round, k.Color, k.X, k.Y, k.Width, k.Height, k.ImageId, k.ImageX, k.ImageY, k.ImageSize, id)
			if err != nil {
				return err
			}

			return nil
		},
		Del: func(id int) error {
			_, err := initdb.DB.Exec("DELETE from object_token WHERE object_id = ?;", id)
			if err != nil {
				return err
			}

			return nil
		},
		Parse: func(d []byte) (any, error) {
			out := Token{}

			err := json.Unmarshal(d, &out)
			if err != nil {
				return nil, err
			}

			return out, nil
		},
	}
}
