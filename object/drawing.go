package object

import (
	"encoding/json"
	"errors"

	"gitlab.com/AntiMS/govtt/initdb"
)

// Kind-specific data structure for the "drawing"-kind object. A drawing is composed of a series of attached line segments. It allows quick composure of simple line drawings.
type Drawing struct {
	// The color of the drawing in hexadecimal color notation, including a leading pound sign.
	Color string `json:"color"`

	// Ordered slice of points indicating the sape of the drawing.
	Points []Point `json:"points"`
}

func init() {
	kinds["drawing"] = Kind{
		Get: func(ids []int) (map[int]any, error) {
			params := ""
			idInterfaces := make([]any, 0)
			for _, i := range ids {
				if params == "" {
					params = "?"
				} else {
					params += ", ?"
				}
				idInterfaces = append(idInterfaces, i)
			}

			rows, err := initdb.DB.Query("SELECT object_id, color FROM object_drawing WHERE object_id IN ("+params+");", idInterfaces...)
			if err != nil {
				return nil, err
			}

			out := make(map[int]any, 0)
			for rows.Next() {
				var id int
				drawing := Drawing{}
				err = rows.Scan(&id, &drawing.Color)
				if err != nil {
					return nil, err
				}
				out[id] = &drawing
			}

			rows.Close()

			rows, err = initdb.DB.Query("SELECT object_id, x, y FROM object_drawing_point WHERE object_id IN ("+params+") ORDER BY object_id ASC, \"order\" ASC;", idInterfaces...)
			if err != nil {
				return nil, err
			}

			for rows.Next() {
				var id int
				point := Point{}
				err = rows.Scan(&id, &point.X, &point.Y)
				if err != nil {
					return nil, err
				}
				d := out[id].(*Drawing)
				d.Points = append(d.Points, point)
			}

			rows.Close()

			return out, nil
		},
		Set: func(id int, obj any) error {
			k, ok := obj.(Drawing)
			if !ok {
				return errors.New("Saved object is not a drawing.")
			}

			_, err := initdb.DB.Exec("UPDATE object_drawing SET color = ? WHERE object_id = ?;", k.Color, id)
			if err != nil {
				return err
			}

			_, err = initdb.DB.Exec("DELETE FROM object_drawing_point WHERE object_id = ?;", id)
			if err != nil {
				return err
			}

			rows := ""
			args := make([]any, 0)
			for i, v := range k.Points {
				rows += "(?, ?, ?, ?),"
				args = append(args, id, i, v.X, v.Y)
			}
			rows = rows[:len(rows)-1] // Not unicode, so this'll work.

			_, err = initdb.DB.Exec("INSERT INTO object_drawing_point (object_id, \"order\", x, y) VALUES"+rows+";", args...)
			if err != nil {
				return err
			}

			return nil
		},
		New: func(id int, obj any) error {
			k, ok := obj.(Drawing)
			if !ok {
				return errors.New("New object is not a drawing.")
			}

			_, err := initdb.DB.Exec("INSERT INTO object_drawing (object_id, color) VALUES(?, ?);", id, k.Color)
			if err != nil {
				return err
			}

			rows := ""
			args := make([]any, 0)
			for i, v := range k.Points {
				rows += "(?, ?, ?, ?),"
				args = append(args, id, i, v.X, v.Y)
			}
			rows = rows[:len(rows)-1] // Not unicode, so this'll work.

			_, err = initdb.DB.Exec("INSERT INTO object_drawing_point (object_id, \"order\", x, y) VALUES"+rows+";", args...)
			if err != nil {
				return err
			}

			return nil
		},
		Del: func(id int) error {
			_, err := initdb.DB.Exec("DELETE from object_drawing WHERE object_id = ?;", id)
			if err != nil {
				return err
			}

			_, err = initdb.DB.Exec("DELETE FROM object_drawing_point WHERE object_id = ?;", id)
			if err != nil {
				return err
			}

			return nil
		},
		Parse: func(d []byte) (any, error) {
			out := Drawing{}

			err := json.Unmarshal(d, &out)
			if err != nil {
				return nil, err
			}

			return out, nil
		},
	}
}
