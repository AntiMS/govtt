package object

import (
	"encoding/json"
	"errors"

	"gitlab.com/AntiMS/govtt/initdb"
)

// Kind-specific data structure for the "map"-kind object. A map is a grid-based representation of a location from a top-down view. In a pinch, can be made to represent other things such as calendars.
type Map struct {
	// The X coordinate of the center of the map.
	X int `json:"x"`

	// The Y coordinate of the center of the map.
	Y int `json:"y"`

	// The width of the map.
	Width int `json:"width"`

	// The height of the map.
	Height int `json:"height"`

	// Raw map data. See the JavaScript file assets/js/objects/map.js for encoding details.
	Data []byte `json:"data"`

	// If true, the map is "locked". Altering it requires unlocking. (This is only enforced on the front-end, however.)
	Locked bool `json:"locked"`
}

func init() {
	kinds["map"] = Kind{
		Get: func(ids []int) (map[int]any, error) {
			params := ""
			idInterfaces := make([]any, 0)
			for _, i := range ids {
				if params == "" {
					params = "?"
				} else {
					params += ", ?"
				}
				idInterfaces = append(idInterfaces, i)
			}

			rows, err := initdb.DB.Query("SELECT object_id, x, y, width, height, data, locked FROM object_map WHERE object_id IN ("+params+");", idInterfaces...)
			if err != nil {
				return nil, err
			}

			out := make(map[int]any, 0)
			for rows.Next() {
				var id int
				m := Map{}
				err = rows.Scan(&id, &m.X, &m.Y, &m.Width, &m.Height, &m.Data, &m.Locked)
				if err != nil {
					return nil, err
				}
				out[id] = m
			}

			rows.Close()

			return out, nil
		},
		Set: func(id int, obj any) error {
			k, ok := obj.(Map)
			if !ok {
				return errors.New("Saved object is not a map.")
			}

			_, err := initdb.DB.Exec("UPDATE object_map SET x = ?, y = ?, width = ?, height = ?, data = ?, locked = ? WHERE object_id = ?;", k.X, k.Y, k.Width, k.Height, k.Data, k.Locked, id)
			if err != nil {
				return err
			}

			return nil
		},
		New: func(id int, obj any) error {
			k, ok := obj.(Map)
			if !ok {
				return errors.New("New object is not a map.")
			}

			_, err := initdb.DB.Exec("INSERT INTO object_map (object_id, x, y, width, height, data, locked) VALUES(?, ?, ?, ?, ?, ?, ?);", id, k.X, k.Y, k.Width, k.Height, k.Data, k.Locked)
			if err != nil {
				return err
			}

			return nil
		},
		Del: func(id int) error {
			_, err := initdb.DB.Exec("DELETE from object_map WHERE object_id = ?;", id)
			if err != nil {
				return err
			}

			return nil
		},
		Parse: func(d []byte) (any, error) {
			out := Map{}

			err := json.Unmarshal(d, &out)
			if err != nil {
				return nil, err
			}

			return out, nil
		},
	}
}
