/*
Objects are items which can be displayed/drawn on the game table. This package manages creation, storage, and bus notification of creation/changes to objects.

This package registers the following HTTP handlers.

HTTP Handlers
=============

/pocket/<campaign_id>
---------------------
Retrieves the current user's pocket items for the indicated campaign. The "pocket" is a place where objects may be "stored" not on the table for quick access. Note that currently only "token"-kind objects can be placed in the pocket.

Method: GET

Path parameters:

campaign_id
: The id of the Campaign for which to fetch the user's pocket.

Response JSON structure:

```
[

	{
		"id": <object id as integer>,
		"kind": <kind as string>,
		"name": <name as string>,
		"kind_data": <kind_data as an arbitrary, object-kind-specific data structure>
	},
	...

]
```

/object/save
------------
Updates an existing object in the database and notifies the bus about the change.

Method: POST

Request JSON structure:

```

	{
		"id": <object id as integer>,
		"kind": <kind as string>,
		"name": <name as string>,
		"kind_data": <kind_data as an arbitrary, object-kind-specific data structure>
	}

```

A successful response is a "204 No Content" response.

This call, when successful, also sends a message to the default bus of the form:

```
&bus.Message{Name: []string{"campaign", "<campaign_id>", "scene", "<scene_id>", "changeobject"}, Value: <object>}
```

Where "<object>" is an Object.

/object/newTable/<scene_id>
---------------------------
Creates a new object, places it on the table, and notifies the bus about the new object.

Method: POST

Path parameters:

scene_id
: The id of the Scene in which to create the new object.

Request JSON structure:

```

	{
		"id": null,
		"kind": <kind as string>,
		"name": <name as string>,
		"kind_data": <kind_data as an arbitrary, object-kind-specific data structure>
	}

```

A successful response is a "200 Ok" response.

Response JSON structure:

```
<object id as integer>
```

This call, when successful, also sends a message to the default bus of the form:

```
&bus.Message{Name: []string{"campaign", "<campaign_id>", "scene", "<scene_id>", "newobject"}, Value: <object>}
```

Where "<object>" is an Object.

/object/newPocket/<campaign_id>
-------------------------------
Creates a new object in the current user's pocket.

Method: POST

Path parameters:

campaign_id
: The id of the Campaign in which to create the new object.

Request JSON structure:

```

	{
		"id": null,
		"kind": <kind as string>,
		"name": <name as string>,
		"kind_data": <kind_data as an arbitrary, object-kind-specific data structure>
	}

```

A successful response is a "200 Ok" response.

Response JSON structure:

```
<object id as integer>
```

/object/delete/<object_id>
--------------------------
Deletes an object from the pocket or table and, if the deletion was from the table,  notifies the bus about the deletion.

Method: POST

Path parameters:

object_id
: The id of the Campaign in which to create the new object.

The request body is ignored. It may be empty.

A successful response is a "204 No Content" response.

This call, when it has successfully deleted an object off of the table, also sends a message to the default bus of the form:

```
&bus.Message{Name: []string{"campaign", "<campaign_id>", "scene", "<scene_id>", "delobject"}, Value: <object_id>}
```

Where "<object_id>" is the id of the object deleted.

If the object deleted was from the pocket, no bus message is sent.

Other Bus Interactions
======================
Upon new subscriptions to the bus (as indicated by the emission of any messages starting with []string{"bus", "newsubscription", "campaign"}), gets the scene id from the message name and sends the following messages in the order specified to the new subscribee via the channel in the new subscription message value:

* One message of the form: &bus.Message{Name: []string{"campaign", "<campaign_id>", "scene", "<scene_id>", "sceneinitstart"}, Value: <scene_id>}
* One message for each object in the scene: &bus.Message{Name: []string{"campaign", "<campaign_id>", "scene", "<scene_id>", "newobject"}, Value: <object>}
* One message of the form: &bus.Message{Name: []string{"campaign", "<campaign_id>", "scene", "<scene_id>", "sceneinitdone"}, Value: <scene_id>}

This allows the new subscribee to receive all object data for the scene to which they have just subscribed.
*/
package object

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"strings"
	"time"

	"gitlab.com/AntiMS/govtt/auth"
	"gitlab.com/AntiMS/govtt/bus"
	"gitlab.com/AntiMS/govtt/initdb"
)

/*
All objects have a particular "kind" which represent a "type" of object. Each kind has its own data structure.

Instances of this "Kind" type represent particular kinds. Functions stored in this struct provide kind-specific logic for storing/retrieving to/from the database as well as for parsing JSON into an kind-specific data.
*/
type Kind struct {
	// For a slice of object ids, return the kind-specific data for each referenced object.
	Get func(ids []int) (map[int]any, error)

	// Updates the kind-specific data for an existing object in the database.
	Set func(id int, obj any) error

	// Stores the kind-specific data for a new object to the database.
	New func(id int, obj any) error

	// Deletes the kind-specific data for an object from the database.
	Del func(id int) error

	// Converts a JSON string (actually []byte) into kind-specific data.
	Parse func(d []byte) (any, error)
}

var kinds map[string]Kind = make(map[string]Kind, 0)

/*
A JSON-serializeable object representing an object -- an object which can be drawn on the table.
*/
type Object struct {
	Id       *int   `json:"id"`
	Kind     string `json:"kind"`
	Name     string `json:"name"`
	KindData any    `json:"kind_data"`
}

func getSceneData(sceneId int) ([]Object, error) {
	query := "SELECT id, kind, name FROM object WHERE scene_id = ?;"
	rows, err := initdb.DB.Query(query, sceneId)
	if err != nil {
		return nil, err
	}

	out := make([]Object, 0)
	typeIds := make(map[string][]int, 0)
	idMap := make(map[int]int, 0)
	for rows.Next() {
		obj := Object{}
		err = rows.Scan(&obj.Id, &obj.Kind, &obj.Name)
		if err != nil {
			return nil, err
		}
		l, ok := typeIds[obj.Kind]
		if !ok {
			l = make([]int, 0)
			typeIds[obj.Kind] = l
		}
		typeIds[obj.Kind] = append(l, *obj.Id)
		idMap[*obj.Id] = len(out)
		out = append(out, obj)
	}

	rows.Close()

	for kind, ids := range typeIds {
		kindDatas, err := kinds[kind].Get(ids)
		if err != nil {
			return nil, err
		}

		for id, kindData := range kindDatas {
			out[idMap[id]].KindData = kindData
		}
	}

	return out, nil
}

func getPocketData(campaignId int, u auth.User) ([]Object, error) {
	query := "SELECT id, kind, name FROM object WHERE campaign_id = ? AND user_id = ?;"
	rows, err := initdb.DB.Query(query, campaignId, u.Id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	out := make([]Object, 0)
	typeIds := make(map[string][]int, 0)
	idMap := make(map[int]int, 0)
	for rows.Next() {
		obj := Object{}
		err = rows.Scan(&obj.Id, &obj.Kind, &obj.Name)
		if err != nil {
			return nil, err
		}
		l, ok := typeIds[obj.Kind]
		if !ok {
			l = make([]int, 0)
			typeIds[obj.Kind] = l
		}
		typeIds[obj.Kind] = append(l, *obj.Id)
		idMap[*obj.Id] = len(out)
		out = append(out, obj)
	}

	for kind, ids := range typeIds {
		kindDatas, err := kinds[kind].Get(ids)
		if err != nil {
			return nil, err
		}

		for id, kindData := range kindDatas {
			out[idMap[id]].KindData = kindData
		}
	}

	return out, nil
}

func getPocket(w http.ResponseWriter, r *http.Request, u auth.User) {
	if r.Method != "GET" {
		http.Error(w, "Please use GET.", http.StatusMethodNotAllowed)
		return
	}

	split := strings.Split(r.URL.Path, "/")
	if len(split) < 3 {
		http.Error(w, "Invalid request path; required campaign id.", http.StatusBadRequest)
		return
	}

	campaignIdStr := split[2]
	campaignId, err := strconv.Atoi(campaignIdStr)
	if err != nil {
		http.Error(w, "Failed to parse campaign id. "+err.Error(), http.StatusBadRequest)
		return
	}

	pocketData, err := getPocketData(campaignId, u)
	if err != nil {
		http.Error(w, "Failed to retrieve pocket data. "+err.Error(), http.StatusBadRequest)
		return
	}

	jsonPocketData, err := json.Marshal(pocketData)
	if err != nil {
		http.Error(w, "Failed to marshal response JSON. "+err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Cache-Control", "no-store max-age=0")
	http.ServeContent(w, r, "pocket_data.json", time.Time{}, bytes.NewReader(jsonPocketData))
}

func parse(r *http.Request) (Object, error) {
	body, err := io.ReadAll(r.Body)
	if err != nil {
		return Object{}, err
	}

	out := Object{}

	err = json.Unmarshal(body, &out)
	if err != nil {
		return Object{}, err
	}

	tmp, err := json.Marshal(out.KindData)
	if err != nil {
		return Object{}, err
	}

	out.KindData, err = kinds[out.Kind].Parse(tmp)
	if err != nil {
		return Object{}, err
	}

	return out, nil
}

func save(obj Object, u auth.User) error {
	query := `
		SELECT id FROM object WHERE id = ? AND (user_id = ? OR scene_id IN (
			SELECT scene_id FROM campaign WHERE id IN (
				SELECT campaign_id FROM campaign_user WHERE user_id = ?
			)
			UNION
			SELECT id FROM scene WHERE user_id = ?
		));`
	rows, err := initdb.DB.Query(query, obj.Id, u.Id, u.Id, u.Id)
	if err != nil {
		return err
	}

	if !rows.Next() {
		rows.Close()
		return errors.New("Unauthorized modification of object.")
	}

	rows.Close()

	rows, err = initdb.DB.Query("SELECT id, campaign_id FROM scene WHERE id = (SELECT scene_id FROM object WHERE id = ?);", obj.Id)
	if err != nil {
		return err
	}

	var campaignId int
	var sceneId int
	if rows.Next() {
		err := rows.Scan(&sceneId, &campaignId)
		if err != nil {
			rows.Close()
			return err
		}
	}

	rows.Close()

	// Nothing but name in the object table is mutable.
	_, err = initdb.DB.Exec("UPDATE object SET name = ? WHERE id = ?;", obj.Name, obj.Id)
	if err != nil {
		return err
	}

	kinds[obj.Kind].Set(*obj.Id, obj.KindData)

	if campaignId > 0 {
		bus.Publish(&bus.Message{Name: []string{"campaign", strconv.Itoa(campaignId), "scene", strconv.Itoa(sceneId), "changeobject"}, Value: obj})
	}

	return nil
}

func saveObject(w http.ResponseWriter, r *http.Request, u auth.User) {
	if r.Method != "POST" {
		http.Error(w, "Please use POST.", http.StatusMethodNotAllowed)
		return
	}

	req, err := parse(r)
	if err != nil {
		http.Error(w, "Failed to parse object. "+err.Error(), http.StatusBadRequest)
		return
	}

	err = save(req, u)
	if err != nil {
		http.Error(w, "Failed to save object. "+err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func newTable(obj Object, sceneId int, u auth.User) (int, error) {
	obj.Id = nil

	query := `
		SELECT campaign_id FROM scene WHERE (user_id = ? OR id IN (
			SELECT scene_id FROM campaign WHERE id IN (
				SELECT campaign_id FROM campaign_user WHERE user_id = ?
			)
		)) AND id = ?;`
	rows, err := initdb.DB.Query(query, u.Id, u.Id, sceneId)
	if err != nil {
		return 0, err
	}

	if !rows.Next() {
		rows.Close()
		return 0, errors.New("Unauthorized creation of table object.")
	}

	var campaignId int
	err = rows.Scan(&campaignId)
	if err != nil {
		rows.Close()
		return 0, err
	}

	rows.Close()

	res, err := initdb.DB.Exec("INSERT INTO object (kind, scene_id, name) VALUES(?, ?, ?);", obj.Kind, sceneId, obj.Name)
	if err != nil {
		return 0, err
	}

	id64, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}

	id := int(id64)
	obj.Id = &id

	err = kinds[obj.Kind].New(id, obj.KindData)
	if err != nil {
		return 0, err
	}

	bus.Publish(&bus.Message{Name: []string{"campaign", strconv.Itoa(campaignId), "scene", strconv.Itoa(sceneId), "newobject"}, Value: obj})

	return id, nil
}

func newTableObject(w http.ResponseWriter, r *http.Request, u auth.User) {
	if r.Method != "POST" {
		http.Error(w, "Please use POST.", http.StatusMethodNotAllowed)
		return
	}

	split := strings.Split(r.URL.Path, "/")
	if len(split) < 4 {
		http.Error(w, "Invalid request path; required scene id.", http.StatusBadRequest)
		return
	}

	sceneIdStr := split[3]
	sceneId, err := strconv.Atoi(sceneIdStr)
	if err != nil {
		http.Error(w, "Failed to parse scene id. "+err.Error(), http.StatusBadRequest)
		return
	}

	req, err := parse(r)
	if err != nil {
		http.Error(w, "Failed to parse object. "+err.Error(), http.StatusBadRequest)
		return
	}

	id, err := newTable(req, sceneId, u)
	if err != nil {
		http.Error(w, "Failed to save object. "+err.Error(), http.StatusInternalServerError)
		return
	}

	jsonId, err := json.Marshal(id)
	if err != nil {
		http.Error(w, "Failed to marshal response JSON. "+err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Cache-Control", "no-store max-age=0")
	http.ServeContent(w, r, "new_table_object.json", time.Time{}, bytes.NewReader(jsonId))
}

func newPocket(obj Object, campaignId int, u auth.User) (int, error) {
	obj.Id = nil

	query := `
		SELECT campaign_id FROM campaign_user WHERE campaign_id = ? AND user_id = ?;`
	rows, err := initdb.DB.Query(query, campaignId, u.Id)
	if err != nil {
		return 0, err
	}

	if !rows.Next() {
		rows.Close()
		return 0, errors.New("Unauthorized creation of pocket object.")
	}

	rows.Close()

	res, err := initdb.DB.Exec("INSERT INTO object (kind, user_id, campaign_id, name) VALUES(?, ?, ?, ?);", obj.Kind, u.Id, campaignId, obj.Name)
	if err != nil {
		return 0, err
	}

	id64, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}

	id := int(id64)
	obj.Id = &id

	err = kinds[obj.Kind].New(id, obj.KindData)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func newPocketObject(w http.ResponseWriter, r *http.Request, u auth.User) {
	if r.Method != "POST" {
		http.Error(w, "Please use POST.", http.StatusMethodNotAllowed)
		return
	}

	split := strings.Split(r.URL.Path, "/")
	if len(split) < 4 {
		http.Error(w, "Invalid request path; required campaign id.", http.StatusBadRequest)
		return
	}

	campaignIdStr := split[3]
	campaignId, err := strconv.Atoi(campaignIdStr)
	if err != nil {
		http.Error(w, "Failed to parse campaign id. "+err.Error(), http.StatusBadRequest)
		return
	}

	req, err := parse(r)
	if err != nil {
		http.Error(w, "Failed to parse object. "+err.Error(), http.StatusBadRequest)
		return
	}

	id, err := newPocket(req, campaignId, u)
	if err != nil {
		http.Error(w, "Failed to save object. "+err.Error(), http.StatusInternalServerError)
		return
	}

	jsonId, err := json.Marshal(id)
	if err != nil {
		http.Error(w, "Failed to marshal response JSON. "+err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Cache-Control", "no-store max-age=0")
	http.ServeContent(w, r, "new_pocket_object.json", time.Time{}, bytes.NewReader(jsonId))
}

func deleteObj(id int, u auth.User) error {
	query := `
		SELECT kind, scene_id FROM object WHERE id = ? AND (user_id = ? OR scene_id IN (
			SELECT scene_id FROM campaign WHERE id IN (
				SELECT campaign_id FROM campaign_user WHERE user_id = ?
			)
			UNION
			SELECT id FROM scene WHERE user_id = ?
		));`
	rows, err := initdb.DB.Query(query, id, u.Id, u.Id, u.Id)
	if err != nil {
		return err
	}

	if !rows.Next() {
		rows.Close()
		return errors.New("Unauthorized deletion of object.")
	}

	var kind string
	var sceneId *int
	err = rows.Scan(&kind, &sceneId)
	if err != nil {
		rows.Close()
		return err
	}

	rows.Close()

	_, err = initdb.DB.Exec("DELETE FROM object WHERE id = ?;", id)
	if err != nil {
		return err
	}

	kinds[kind].Del(id)

	if sceneId != nil && *sceneId > 0 {
		rows, err = initdb.DB.Query("SELECT campaign_id FROM scene WHERE id = ?;", sceneId)
		if err != nil {
			return err
		}

		if !rows.Next() {
			rows.Close()
			return errors.New("Could not get campaign id.")
		}

		var campaignId int
		err = rows.Scan(&campaignId)
		if err != nil {
			rows.Close()
			return err
		}

		rows.Close()

		bus.Publish(&bus.Message{Name: []string{"campaign", strconv.Itoa(campaignId), "scene", strconv.Itoa(*sceneId), "delobject"}, Value: id})
	}

	return nil
}

func deleteObject(w http.ResponseWriter, r *http.Request, u auth.User) {
	if r.Method != "POST" {
		http.Error(w, "Please use POST.", http.StatusMethodNotAllowed)
		return
	}

	split := strings.Split(r.URL.Path, "/")
	if len(split) < 4 {
		http.Error(w, "Invalid request path; required object id.", http.StatusBadRequest)
		return
	}

	objIdStr := split[3]
	objId, err := strconv.Atoi(objIdStr)
	if err != nil {
		http.Error(w, "Failed to parse object id. "+err.Error(), http.StatusBadRequest)
		return
	}

	err = deleteObj(objId, u)
	if err != nil {
		http.Error(w, "Failed to delete object. "+err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func init() {
	http.HandleFunc("/pocket/", auth.DecorateHandlerFunc(getPocket))
	http.HandleFunc("/object/save", auth.DecorateHandlerFunc(saveObject))
	http.HandleFunc("/object/newTable/", auth.DecorateHandlerFunc(newTableObject))
	http.HandleFunc("/object/newPocket/", auth.DecorateHandlerFunc(newPocketObject))
	http.HandleFunc("/object/delete/", auth.DecorateHandlerFunc(deleteObject))

	receiver := bus.NewReceiver()
	bus.Subscribe(receiver, []string{"bus", "newsubscription", "campaign"})
	go func() {
		for m := range receiver {
			if len(m.Name) != 6 || m.Name[4] != "scene" {
				continue
			}

			campaignId, err := strconv.Atoi(m.Name[3])
			if err != nil {
				continue
			}

			sceneId, err := strconv.Atoi(m.Name[5])
			if err != nil {
				continue
			}

			c, ok := m.Value.(chan<- *bus.Message)
			if !ok {
				continue
			}

			sceneData, err := getSceneData(sceneId)
			if err != nil {
				continue
			}

			func() {
				defer func() {
					if r := recover(); r != nil {
						fmt.Println("Recovered in f", r)
					}
				}()

				c <- &bus.Message{Name: []string{"campaign", strconv.Itoa(campaignId), "scene", strconv.Itoa(sceneId), "sceneinitstart"}, Value: sceneId}

				for _, o := range sceneData {
					c <- &bus.Message{Name: []string{"campaign", strconv.Itoa(campaignId), "scene", strconv.Itoa(sceneId), "newobject"}, Value: o}
				}

				c <- &bus.Message{Name: []string{"campaign", strconv.Itoa(campaignId), "scene", strconv.Itoa(sceneId), "sceneinitdone"}, Value: sceneId}
			}()
		}
	}()
}
