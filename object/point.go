package object

// A JSON serializable point with X and Y coordinates as float64.
type Point struct {
	X float64 `json:"x"`
	Y float64 `json:"y"`
}
