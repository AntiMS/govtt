module gitlab.com/AntiMS/govtt

go 1.19

require github.com/mattn/go-sqlite3 v1.14.16

require (
	github.com/tdewolff/minify/v2 v2.12.4 // indirect
	github.com/tdewolff/parse/v2 v2.6.5 // indirect
)
