package session

import (
	"crypto/rand"
	"encoding/base64"
	"errors"
)

var sessions map[string]*Session = make(map[string]*Session)

// Provides access to the state of a single client's session state.
type Session struct {
	SessionId string
	values    map[string]any
}

// Given a session value name, gets and returns the value. The returned boolean is true if this was successful and false if no such name could be found.
func (s *Session) Get(name string) (any, bool) {
	out, ok := s.values[name]
	return out, ok
}

// Stores a value to the session under the given name, overwriting the existing value if one already exists under the same name.
func (s *Session) Set(name string, value any) {
	s.values[name] = value
}

// Deletes a value by name from the session. Silently nullipotent if no value exists by that name.
func (s *Session) Del(name string) {
	delete(s.values, name)
}

func create() (*Session, error) {
	enc := base64.RawStdEncoding

	sessIdBytes := make([]byte, 32)
	n, err := rand.Read(sessIdBytes)
	if err != nil {
		return nil, err
	}
	if n < len(sessIdBytes) {
		return nil, errors.New("Could not obtain sufficient entropy.")
	}

	out := &Session{SessionId: enc.EncodeToString(sessIdBytes), values: make(map[string]any)}

	sessions[out.SessionId] = out

	return out, nil
}

func bySessionId(sessionId string) (*Session, bool) {
	out, ok := sessions[sessionId]
	return out, ok
}
