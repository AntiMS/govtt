/*
Manages server-side state stored related to a single user communicating with the server.
*/
package session

import (
	"net/http"
	"time"
)

func forRequest(w http.ResponseWriter, r *http.Request) (*Session, error) {
	var out *Session

	cookie, err := r.Cookie("SESSION")
	if err == nil {
		out, ok := bySessionId(cookie.Value)
		if ok {
			return out, nil
		}
	}

	if err != nil && err != http.ErrNoCookie {
		return nil, err
	}

	out, err = create()
	if err != nil {
		return nil, err
	}
	http.SetCookie(w, &http.Cookie{
		Name:     "SESSION",
		Value:    out.SessionId,
		Path:     "/",
		Expires:  time.Date(2200, time.Month(1), 1, 1, 0, 0, 0, time.UTC),
		Secure:   r.TLS != nil,
		HttpOnly: true,
		SameSite: http.SameSiteStrictMode,
	})

	return out, nil
}

/*
Returns an HTTP handler function suitable for registering via http.HandleFunc.

The single arguments to this function DecorateHandlerFunc is a function which takes an http.ResponseWriter, *http.Request (like a handler function) and a *Session. Called when the returned function is called.
*/
func DecorateHandlerFunc(f func(w http.ResponseWriter, r *http.Request, s *Session)) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		s, err := forRequest(w, r)
		if err != nil {
			http.Error(w, "Failed to retrieve session. "+err.Error(), http.StatusInternalServerError)
			return
		}
		f(w, r, s)
	}
}
