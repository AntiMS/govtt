/*
Manages TTRPG "campaigns" -- objects under which TTRPG-related resources may be grouped for organization purposes. Campaigns have one or more Scenes which are individual locations which can be shown on the virtual gaming table.

Campaigns and Scenes are persistent objects. Campaigns have a display name and a default scene and may be owned by one or more users. Scenes have only a name, are associated with a single campaign, and are owned by a single user. Scenes may be used to further group resources.

HTTP Handlers
=============
This package registers several HTTP handlers detailed below:

/campaigns
----------
Returns all Campaigns owned by the current user.

Method: GET

Takes no path parameters.

Response JSON structure:

```
[

	{
		"id": <campaign id as an integer>,
		"scene_id": <default scene id as an integer>,
		"name": <campaign display name as a string>
	},
	...

]
```

/campaign/changeDisplayName/<campaign_id>
-----------------------------------------
Sets the display name for the requesting user to that passed in the POST body.

Method: POST

Path parameters:

- campaign_id -- The id of the Campaign for which to set the user's display name.

The POST body should be just the new display name.

A successful response is a "204 No Content" response.

/scenes/<campaign_id>
---------------------
Returns all Scenes in a specified Campaign.

Method: GET

Path parameters:

- campaign_id -- The id of the Campaign for which to retrieve Scenes.

Response JSON structure:

```
[

	{
		"id": <scene id as an integer>,
		"name": <scene display name as a string>
	},
	...

]
```

/scenes/new/<campaign_id>
-------------------------
Creates a new Scene under a given Campaign.

Method: POST

Path parameters:

- campaign_id -- The id of the Campaign under which to create a new Scene.

Response JSON structure:

	<id as an int>

The returned id is the id of the newly-created scene.

/scenes/rename/<scene_id>
-------------------------
Renames a scene.

Method: POST

Path parameters:

- scene_id -- The id of the Scene to rename.

The POST body should just be the new name.

A successful response is a "204 No Content" response.

/scenes/set/<campaign_id>/<scene_id>
------------------------------------
Sets the default scene for a campaign.

Method: POST

Path parameters:

- campaign_id -- The id of the Campaign for which to set the default Scene.
- scene_id -- The id of the Scene to set as the default scene.

A successful response is a "204 No Content" response.

This call, when successful, also sends a message to the default bus of the form:

	&bus.Message{Name: []string{"campaign", "<campaign_id>", "campaignevent", "sceneset"}, Value: <scene_id>}

/campaignUsers/<campaign_id>
----------------------------
Returns usernames and display names of all users in a specified Campaign.

Method: GET

Path parameters:

- campaign_id -- The id of the Campaign for which to retrieve users.

Response JSON structure:

```
[

	{
		"username": <username as a string>,
		"display_name": <display name as a string>
	},
	...

]
```

Bus Handler
===========
This package also depends on the bus package. Every time there is a new subscription to a name of the form `[]string{"bus", "newsubscription", "campaign", "<campaign_id>", "campaignevent"}` (where `<campaign_id>` is the id of a campaign as a base-10-serialized integer), this package sends to the bus the following messages:

- `&bus.Message{Name: []string{"campaign", "<campaign_id>", "campaignevent", "newimage"}, Value: <image_id>}` -- Sent once for every image in the campaign. `<campaign_id>` is the id of the campaign as a base-10-serialized integer and `<image_id>` is the id of an image.
- `&bus.Message{Name: []string{"campaign", "<campaign_id>", "campaignevent", "campaigninitdone"}, Value: <campaign_id>}` -- Sent once. `<campaign_id>` is the id of the campaign as a base-10-serialized integer.
- `&bus.Message{Name: []string{"campaign", "<campaign_id>", "campaignevent", "sceneset"}, Value: <scene_id>}` -- Sent once. `<campaign_id>` is the id of the campaign as a base-10-serialized integer and <scene_id> is the id of the campaign's default scene as an integer.
*/
package campaign

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"strconv"
	"strings"
	"time"

	"gitlab.com/AntiMS/govtt/auth"
	"gitlab.com/AntiMS/govtt/bus"
	"gitlab.com/AntiMS/govtt/image"
	"gitlab.com/AntiMS/govtt/initdb"
)

// Represents a TTRPG campaign.
type Campaign struct {
	Id      int    `json:"id"`
	SceneId *int   `json:"scene_id"`
	Name    string `json:"name"`
}

// Represents a Scene -- a location to be displayed.
type Scene struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

// Represents a user's relationship to a campaign.
type CampaignUser struct {
	Username    string `json:"username"`
	DisplayName string `json:"display_name"`
}

// Creates a new scene in the indicated campaign. Returns its id.
func NewScene(userId, campaignId int, name string) (int, error) {
	res, err := initdb.DB.Exec("INSERT INTO scene (user_id, campaign_id, name) VALUES(?, ?, ?);", userId, campaignId, name)
	if err != nil {
		return 0, err
	}

	id64, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}

	return int(id64), nil
}

// Sets a scene as the current scene on a campaign. Does not cause any bus messages to be sent.
func SetScene(campaignId, sceneId int) error {
	_, err := initdb.DB.Exec("UPDATE campaign SET scene_id = ? WHERE id = ?;", sceneId, campaignId)
	return err
}

// Adds a user to a campaign
func Grant(campaignId, userId int, displayName string) error {
	_, err := initdb.DB.Exec("INSERT INTO campaign_user (user_id, campaign_id, display_name) VALUES (?, ?, ?);", userId, campaignId, displayName)
	if err != nil {
		return err
	}

	return nil
}

// Removes a user from a campaign
func Revoke(campaignId, userId int) error {
	res, err := initdb.DB.Exec("DELETE FROM campaign_user WHERE user_id = ? AND campaign_id = ?;", userId, campaignId)
	if err != nil {
		return err
	}

	affected, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if affected == 0 {
		return errors.New("User was not added to campaign.")
	}

	return nil
}

// Creates a new campaign (with accompanying scene named sceneName) in the database, associates the given user with it (using the given displayName), and returns its id.
func New(userId int, name, displayName, sceneName string) (int, error) {
	res, err := initdb.DB.Exec("INSERT INTO campaign (name) VALUES (?);", name)
	if err != nil {
		return 0, err
	}

	id64, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}
	id := int(id64)

	sceneId, err := NewScene(userId, id, sceneName)
	if err != nil {
		return 0, err
	}

	err = SetScene(id, sceneId)
	if err != nil {
		return 0, err
	}

	err = Grant(id, userId, displayName)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func byId(campaignId int) (Campaign, error) {
	rows, err := initdb.DB.Query("SELECT id, scene_id, name FROM campaign WHERE id = ?;", campaignId)
	if err != nil {
		return Campaign{}, err
	}

	if !rows.Next() {
		return Campaign{}, errors.New("No such campaign.")
	}

	out := Campaign{}
	if rows.Scan(&out.Id, &out.SceneId, &out.Name) != nil {
		return Campaign{}, errors.New("Error scanning row.")
	}

	rows.Close()

	return out, nil
}

// Gets all campaigns owned by a particular user.
func CampaignsByUserId(userId int) ([]Campaign, error) {
	rows, err := initdb.DB.Query("SELECT id, scene_id, name FROM campaign WHERE id IN (SELECT campaign_id FROM campaign_user WHERE user_id = ?);", userId)
	if err != nil {
		return nil, err
	}

	out := make([]Campaign, 0)
	for rows.Next() {
		campaign := Campaign{}
		err = rows.Scan(&campaign.Id, &campaign.SceneId, &campaign.Name)
		if err != nil {
			return nil, err
		}
		out = append(out, campaign)
	}

	rows.Close()

	return out, nil
}

func listCampaigns(w http.ResponseWriter, r *http.Request, u auth.User) {
	if r.Method != "GET" {
		http.Error(w, "Please use GET.", http.StatusMethodNotAllowed)
		return
	}

	campaigns, err := CampaignsByUserId(u.Id)
	if err != nil {
		http.Error(w, "Failed to get user's campaigns. "+err.Error(), http.StatusInternalServerError)
		return
	}

	jsonCampaigns, err := json.Marshal(campaigns)
	if err != nil {
		http.Error(w, "Failed to marshal response JSON. "+err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Cache-Control", "no-store max-age=0")
	http.ServeContent(w, r, "campaigns.json", time.Time{}, bytes.NewReader(jsonCampaigns))
}

func changeDisplayName(w http.ResponseWriter, r *http.Request, u auth.User) {
	if r.Method != "POST" {
		http.Error(w, "Please use POST.", http.StatusMethodNotAllowed)
		return
	}

	split := strings.Split(r.URL.Path, "/")
	if len(split) < 4 {
		http.Error(w, "Invalid request path url; required campaign id.", http.StatusBadRequest)
		return
	}

	campaignIdStr := split[3]
	campaignId, err := strconv.Atoi(campaignIdStr)
	if err != nil {
		http.Error(w, "Invalid request. Could not parse campaign id. "+err.Error(), http.StatusBadRequest)
		return
	}

	newDisplayName, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "Failed to read body. "+err.Error(), http.StatusInternalServerError)
		return
	}
	if len(newDisplayName) == 0 {
		http.Error(w, "Name must not be empty.", http.StatusBadRequest)
		return
	}

	res, err := initdb.DB.Exec("UPDATE campaign_user SET display_name = ? WHERE user_id = ? AND campaign_id = ?;", newDisplayName, u.Id, campaignId)
	if err != nil {
		http.Error(w, "Failed to update display name. "+err.Error(), http.StatusInternalServerError)
		return
	}

	rowsAffected, err := res.RowsAffected()
	if err != nil {
		http.Error(w, "Failed to update display name. "+err.Error(), http.StatusInternalServerError)
		return
	}
	if rowsAffected <= 0 {
		http.Error(w, "Failed to update display name. No such row found.", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func listScenes(w http.ResponseWriter, r *http.Request, u auth.User) {
	if r.Method != "GET" {
		http.Error(w, "Please use GET.", http.StatusMethodNotAllowed)
		return
	}

	split := strings.Split(r.URL.Path, "/")
	if len(split) < 3 {
		http.Error(w, "Invalid request path; required campaign id.", http.StatusBadRequest)
		return
	}

	campaignIdStr := split[2]
	campaignId, err := strconv.Atoi(campaignIdStr)
	if err != nil {
		http.Error(w, "Failed to parse campaign id. "+err.Error(), http.StatusBadRequest)
		return
	}

	query := "SELECT id, name FROM scene WHERE campaign_id = ? AND user_id = ? ORDER BY name;"
	rows, err := initdb.DB.Query(query, campaignId, u.Id)
	if err != nil {
		http.Error(w, "Error querying database. "+err.Error(), http.StatusInternalServerError)
		return
	}

	out := make([]Scene, 0)
	for rows.Next() {
		var scene Scene
		err = rows.Scan(&scene.Id, &scene.Name)
		if err != nil {
			http.Error(w, "Error reading database. "+err.Error(), http.StatusInternalServerError)
			return
		}

		out = append(out, scene)
	}

	rows.Close()

	outBytes, err := json.Marshal(out)
	if err != nil {
		http.Error(w, "Error marshalling response JSON. "+err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Cache-Control", "no-store max-age=0")
	http.ServeContent(w, r, "scenes.json", time.Time{}, bytes.NewReader(outBytes))
}

func setScene(w http.ResponseWriter, r *http.Request, u auth.User) {
	if r.Method != "POST" {
		http.Error(w, "Please use POST.", http.StatusMethodNotAllowed)
		return
	}

	split := strings.Split(r.URL.Path, "/")
	if len(split) < 5 {
		http.Error(w, "Invalid request path; required campaign id and scene id.", http.StatusBadRequest)
		return
	}

	campaignIdStr := split[3]
	campaignId, err := strconv.Atoi(campaignIdStr)
	if err != nil {
		http.Error(w, "Failed to parse campaign id. "+err.Error(), http.StatusBadRequest)
		return
	}

	sceneIdStr := split[4]
	sceneId, err := strconv.Atoi(sceneIdStr)
	if err != nil {
		http.Error(w, "Failed to parse scene id. "+err.Error(), http.StatusBadRequest)
		return
	}

	query := "SELECT campaign_id FROM campaign_user WHERE campaign_id = ? AND user_id = ?;"
	rows, err := initdb.DB.Query(query, campaignId, u.Id)
	if err != nil {
		http.Error(w, "Error querying database. "+err.Error(), http.StatusInternalServerError)
		return
	}
	if !rows.Next() {
		http.Error(w, "Unauthorized.", http.StatusUnauthorized)
		return
	}
	rows.Close()

	query = "SELECT id FROM scene WHERE id = ? AND campaign_id = ? AND user_id = ?;"
	rows, err = initdb.DB.Query(query, sceneId, campaignId, u.Id)
	if err != nil {
		http.Error(w, "Error querying database. "+err.Error(), http.StatusInternalServerError)
		return
	}
	if !rows.Next() {
		http.Error(w, "Unauthorized.", http.StatusUnauthorized)
		return
	}
	rows.Close()

	err = SetScene(campaignId, sceneId)

	bus.Publish(&bus.Message{Name: []string{"campaign", strconv.Itoa(campaignId), "campaignevent", "sceneset"}, Value: sceneId})

	w.WriteHeader(http.StatusNoContent)
}

func renameScene(w http.ResponseWriter, r *http.Request, u auth.User) {
	if r.Method != "POST" {
		http.Error(w, "Please use POST.", http.StatusMethodNotAllowed)
		return
	}

	split := strings.Split(r.URL.Path, "/")
	if len(split) < 4 {
		http.Error(w, "Invalid request path; required scene id.", http.StatusBadRequest)
		return
	}

	sceneIdStr := split[3]
	sceneId, err := strconv.Atoi(sceneIdStr)
	if err != nil {
		http.Error(w, "Failed to parse scene id. "+err.Error(), http.StatusBadRequest)
		return
	}

	name, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "Failed to read request body. "+err.Error(), http.StatusInternalServerError)
		return
	}
	if len(name) == 0 {
		http.Error(w, "Name must not be empty.", http.StatusBadRequest)
		return
	}

	res, err := initdb.DB.Exec("UPDATE scene SET name = ? WHERE id = ? AND user_id = ?;", name, sceneId, u.Id)
	if err != nil {
		http.Error(w, "Failed to update scene name. "+err.Error(), http.StatusInternalServerError)
		return
	}
	rowsAffected, err := res.RowsAffected()
	if err != nil {
		http.Error(w, "Failed to retrieve number of rows updated. "+err.Error(), http.StatusInternalServerError)
		return
	}
	if rowsAffected <= 0 {
		http.Error(w, "Scene not found or unauthorized.", http.StatusUnauthorized)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func newScene(w http.ResponseWriter, r *http.Request, u auth.User) {
	if r.Method != "POST" {
		http.Error(w, "Please use POST.", http.StatusMethodNotAllowed)
		return
	}

	split := strings.Split(r.URL.Path, "/")
	if len(split) < 4 {
		http.Error(w, "Invalid request path; required campaign id.", http.StatusBadRequest)
		return
	}

	campaignIdStr := split[3]
	campaignId, err := strconv.Atoi(campaignIdStr)
	if err != nil {
		http.Error(w, "Failed to parse campaign id.", http.StatusBadRequest)
		return
	}

	query := "SELECT user_id FROM campaign_user WHERE campaign_id = ? AND user_id = ?;"
	rows, err := initdb.DB.Query(query, campaignId, u.Id)
	if err != nil {
		http.Error(w, "Error querying database. "+err.Error(), http.StatusInternalServerError)
		return
	}
	if !rows.Next() {
		http.Error(w, "Unauthorized.", http.StatusUnauthorized)
		return
	}
	rows.Close()

	name, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "Failed to read request body. "+err.Error(), http.StatusInternalServerError)
		return
	}
	if len(name) == 0 {
		http.Error(w, "Name must not be empty.", http.StatusBadRequest)
		return
	}

	id, err := NewScene(u.Id, campaignId, string(name))
	if err != nil {
		http.Error(w, "Failed to create scene. "+err.Error(), http.StatusInternalServerError)
		return
	}

	jsonId, err := json.Marshal(id)
	if err != nil {
		http.Error(w, "Failed to marshal response JSON. "+err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Cache-Control", "no-store max-age=0")
	http.ServeContent(w, r, "new_scene.json", time.Time{}, bytes.NewReader(jsonId))
}

func campaignUsers(w http.ResponseWriter, r *http.Request, u auth.User) {
	if r.Method != "GET" {
		http.Error(w, "Please use GET.", http.StatusMethodNotAllowed)
		return
	}

	split := strings.Split(r.URL.Path, "/")
	if len(split) < 3 {
		http.Error(w, "Invalid request path; required campaign id.", http.StatusBadRequest)
		return
	}

	campaignIdStr := split[2]
	campaignId, err := strconv.Atoi(campaignIdStr)
	if err != nil {
		http.Error(w, "Failed to parse campaign id. "+err.Error(), http.StatusBadRequest)
		return
	}

	query := "SELECT user.username, campaign_user.display_name FROM user INNER JOIN campaign_user ON user.id = campaign_user.user_id WHERE campaign_user.campaign_id = ? AND ? IN (SELECT user_id FROM campaign_user WHERE campaign_id = ?);"
	rows, err := initdb.DB.Query(query, campaignId, u.Id, campaignId)
	if err != nil {
		http.Error(w, "Error querying database. "+err.Error(), http.StatusInternalServerError)
		return
	}

	out := make([]*CampaignUser, 0)
	for rows.Next() {
		var username string
		var displayName string
		err = rows.Scan(&username, &displayName)
		if err != nil {
			http.Error(w, "Error reading database. "+err.Error(), http.StatusInternalServerError)
			return
		}

		out = append(out, &CampaignUser{Username: username, DisplayName: displayName})
	}

	rows.Close()

	outBytes, err := json.Marshal(out)
	if err != nil {
		http.Error(w, "Error marshalling response JSON."+err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Cache-Control", "no-store max-age=0")
	http.ServeContent(w, r, "usernames.json", time.Time{}, bytes.NewReader(outBytes))
}

func init() {
	http.HandleFunc("/campaigns", auth.DecorateHandlerFunc(listCampaigns))
	http.HandleFunc("/campaign/changeDisplayName/", auth.DecorateHandlerFunc(changeDisplayName))
	http.HandleFunc("/scenes/", auth.DecorateHandlerFunc(listScenes))
	http.HandleFunc("/scenes/new/", auth.DecorateHandlerFunc(newScene))
	http.HandleFunc("/scenes/rename/", auth.DecorateHandlerFunc(renameScene))
	http.HandleFunc("/scenes/set/", auth.DecorateHandlerFunc(setScene))
	http.HandleFunc("/campaignUsers/", auth.DecorateHandlerFunc(campaignUsers))

	receiver := bus.NewReceiver()
	bus.Subscribe(receiver, []string{"bus", "newsubscription", "campaign"})
	go func() {
		for m := range receiver {
			if len(m.Name) != 5 || m.Name[4] != "campaignevent" {
				continue
			}

			campaignId, err := strconv.Atoi(m.Name[3])
			if err != nil {
				continue
			}

			c, ok := m.Value.(chan<- *bus.Message)
			if !ok {
				continue
			}

			maps, err := image.GetImages(campaignId)
			if err != nil {
				panic(err)
				continue
			}

			campaign, err := byId(campaignId)
			if err != nil {
				continue
			}

			for _, i := range maps {
				c <- &bus.Message{Name: []string{"campaign", strconv.Itoa(campaignId), "campaignevent", "newimage"}, Value: i}
			}

			c <- &bus.Message{Name: []string{"campaign", strconv.Itoa(campaignId), "campaignevent", "campaigninitdone"}, Value: campaignId}
			c <- &bus.Message{Name: []string{"campaign", strconv.Itoa(campaignId), "campaignevent", "sceneset"}, Value: campaign.SceneId}
		}
	}()
}
