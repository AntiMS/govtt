//go:build ignore

/*
A standalone command which adds an existing user to an existing campaign.

Takes two arguments: a username, campaign id, and display name for the user.

The last of these is optional and default to the user's username.
*/
package main

import (
	"fmt"
	"os"
	"strconv"

	"gitlab.com/AntiMS/govtt/auth"
	"gitlab.com/AntiMS/govtt/campaign"
	"gitlab.com/AntiMS/govtt/initdb"
	_ "gitlab.com/AntiMS/govtt/migrate"
)

func main() {
	if len(os.Args) < 3 || len(os.Args) > 4 {
		fmt.Fprintln(os.Stderr, "Please provide a username, a campaign id, and optionally a display name.")
		os.Exit(1)
	}

	username := os.Args[1]
	campaignIdStr := os.Args[2]
	var displayName string
	if len(os.Args) >= 4 {
		displayName = os.Args[3]
	} else {
		displayName = username
	}

	campaignId, err := strconv.Atoi(campaignIdStr)
	if err != nil {
		panic(err)
	}

	initdb.Init()

	user, err := auth.UserByUsername(username)
	if err != nil {
		panic(err)
	}

	err = campaign.Grant(campaignId, user.Id, displayName)
	if err != nil {
		panic(err)
	}
}
