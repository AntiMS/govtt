//go:build ignore

/*
A standalone command which creates a new campaign.

Takes three arguments: a username, a campaign name, a display name to use when granting the user access to the campaign, and a scene name for the initial scene.

The last two of these is optional and defaults to the username and "Default" respectively.
*/
package main

import (
	"fmt"
	"os"

	"gitlab.com/AntiMS/govtt/auth"
	"gitlab.com/AntiMS/govtt/campaign"
	"gitlab.com/AntiMS/govtt/initdb"
	_ "gitlab.com/AntiMS/govtt/migrate"
)

func main() {
	if len(os.Args) < 3 || len(os.Args) > 5 {
		fmt.Fprintln(os.Stderr, "Please provide a username, campaign name, and optionally display name and scene name as arguments.")
		os.Exit(1)
	}

	username := os.Args[1]
	campaignName := os.Args[2]
	var displayName string
	if len(os.Args) == 4 {
		displayName = os.Args[3]
	} else {
		displayName = username
	}
	var sceneName string
	if len(os.Args) == 5 {
		sceneName = os.Args[4]
	} else {
		sceneName = "Default"
	}

	initdb.Init()

	user, err := auth.UserByUsername(username)
	if err != nil {
		panic(err)
	}

	id, err := campaign.New(user.Id, campaignName, displayName, sceneName)
	if err != nil {
		panic(err)
	}

	fmt.Printf("New campaign of id %d created.\n", id)
}
