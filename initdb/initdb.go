/*
Initializes the database connection on initialization. Specifically an SQLite3 database connection.
*/
package initdb

import (
	"database/sql"
	"os"

	_ "github.com/mattn/go-sqlite3"
)

// A database connection. Not initialized until this package's `Init()` (not to be confused with `init()`) function is called. `Init()` should be called from the application's `main()` function.
var DB *sql.DB

var onConnect []func() = make([]func(), 0)

func OnConnect(f func()) {
	if onConnect == nil {
		panic("Database OnConnect called after database initialization.")
	}

	onConnect = append(onConnect, f)
}

// Creates the database connection and stores it to the package variable `DB`
func Init() {
	if onConnect == nil {
		return
	}

	driverName := os.Getenv("GOVTT_DB_BACKEND")
	if len(driverName) == 0 {
		driverName = "sqlite3"
	}

	dataSourceName := map[string]func() string{
		"sqlite3": func() string {
			fn := os.Getenv("GOVTT_DB_SQLITE3_FILENAME")
			if len(fn) == 0 {
				fn = "db.db"
			}
			return fn + "?_busy_timeout=2000"
		},
	}[driverName]()

	db, err := sql.Open(driverName, dataSourceName)
	if err != nil {
		panic(err)
	}

	oc := onConnect
	onConnect = nil
	DB = db

	for _, h := range oc {
		h()
	}
}
