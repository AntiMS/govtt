//go:build ignore

/*
This command generates a Go source file which registers an HTTP handler which serves CSS.

The CSS is served from the path "/media/css/style.css". It is built from the files in the directory "assets/css".

The CSS will be minified unless the environment variable "MINIFY" is defined and has the value "false".
*/
package main

import (
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"

	"github.com/tdewolff/minify/v2"
	"github.com/tdewolff/minify/v2/css"

	"gitlab.com/AntiMS/govtt/gen/mkstatic"
)

var cssDir string

var minifier *minify.M

var doMinify bool = true

func init() {
	workingDir, err := os.Getwd() // Unfortunate that everything's based off of this...
	if err != nil {
		panic(err)
	}

	cssDir = filepath.Join(filepath.Dir(filepath.Dir(workingDir)), "assets", "css")

	minifyFlag := os.Getenv("MINIFY")
	if minifyFlag == "false" {
		doMinify = false
	} else if minifyFlag != "" && minifyFlag != "true" {
		panic(errors.New("Unrecognized MINIFY value: " + fmt.Sprintf("#%v", minifyFlag)))
	}

	if doMinify {
		minifier = minify.New()
		minifier.AddFunc("text/css", css.Minify)
	}
}

type openingReader struct {
	Path       string
	readCloser io.ReadCloser
}

func (r *openingReader) Read(p []byte) (n int, err error) {
	if r.readCloser == nil {
		readCloser, err := os.Open(r.Path)
		if err != nil {
			panic(err)
		}
		r.readCloser = readCloser
	}

	out, outErr := r.readCloser.Read(p)

	if outErr != nil {
		r.readCloser.Close()
	}

	return out, outErr
}

func main() {
	cssReaders := make([]io.Reader, 0)
	err := filepath.Walk(cssDir, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}

		cssReaders = append(cssReaders, &openingReader{Path: path})

		return nil
	})
	if err != nil {
		panic(err)
	}

	reader := io.MultiReader(cssReaders...)

	if doMinify {
		reader = minifier.Reader("text/css", reader)
	}

	err = mkstatic.OutputCodeFromReader(reader, "css", "/media/css/style.css", "text/css", "media_css_style.go")
	if err != nil {
		panic(err)
	}
}
