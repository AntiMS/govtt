//go:build ignore

/*
This command generates a Go source file which registers an HTTP handler which serves JavaScript.

The JavaScript is served from the path "/media/js/script.js". It is built from the files in the directory "assets/js".

If the environment variable "EXTRA_JS" is defined, it is interpreted as a directory path. All files in the referenced directory will be appended to the end of the JavaScript.

The JavaScript will be minified unless the environment variable "MINIFY" is defined and has the value "false".
*/
package main

import (
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	"github.com/tdewolff/minify/v2"
	"github.com/tdewolff/minify/v2/js"

	"gitlab.com/AntiMS/govtt/gen/catjs"
	"gitlab.com/AntiMS/govtt/gen/mkstatic"
)

var doMinify bool = true

var minifier *minify.M

var jsDir string

var extraJsFiles []string

func init() {
	workingDir, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	baseDir := filepath.Dir(filepath.Dir(workingDir))
	jsDir = filepath.Join(baseDir, "assets", "js")

	extraJsFlag := os.Getenv("EXTRA_JS")
	if extraJsFlag != "" {
		split := strings.Split(extraJsFlag, ":")
		extraJsFiles = make([]string, 0, len(split))
		for _, entry := range split {
			if !filepath.IsAbs(entry) {
				entry = filepath.Join(baseDir, entry)
			}
			finfo, err := os.Stat(entry)
			if err != nil {
				panic(err)
			}
			if finfo.IsDir() {
				err := filepath.Walk(entry, func(path string, info os.FileInfo, err error) error {
					if info.IsDir() {
						return nil
					}

					if filepath.Ext(path) != ".js" {
						return nil
					}

					extraJsFiles = append(extraJsFiles, path)

					return nil
				})
				if err != nil {
					panic(err)
				}
			} else {
				extraJsFiles = append(extraJsFiles, entry)
			}
		}
	}

	minifyFlag := os.Getenv("MINIFY")
	if minifyFlag == "false" {
		doMinify = false
	} else if minifyFlag != "" && minifyFlag != "true" {
		panic(errors.New("Unrecognized MINIFY value: " + fmt.Sprintf("#%v", minifyFlag)))
	}

	if doMinify {
		minifier = minify.New()
		minifier.AddFunc("text/javascript", js.Minify)
	}
}

func main() {
	jsFiles := make([]string, 0)
	err := filepath.Walk(jsDir, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}

		if filepath.Ext(path) != ".js" {
			return nil
		}

		shortName := strings.TrimSuffix(path, ".js")[len(jsDir)+1:]

		if !strings.HasPrefix(shortName, "tests/") {
			jsFiles = append(jsFiles, shortName)
		}

		return nil
	})
	if err != nil {
		panic(err)
	}

	reader, err := catjs.CatJs(jsDir, jsFiles, map[string]io.Reader{}, extraJsFiles, nil)
	if err != nil {
		panic(err)
	}

	if doMinify {
		reader = minifier.Reader("text/javascript", reader)
	}

	err = mkstatic.OutputCodeFromReader(reader, "js", "/media/js/script.js", "text/javascript", "media_script.go")
	if err != nil {
		panic(err)
	}
}
