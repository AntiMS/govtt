/*
Initializes the application and starts the HTTP/HTTPS server.
*/
package main

import (
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	_ "gitlab.com/AntiMS/govtt/auth"
	_ "gitlab.com/AntiMS/govtt/bus"
	_ "gitlab.com/AntiMS/govtt/campaign"
	_ "gitlab.com/AntiMS/govtt/channel"
	_ "gitlab.com/AntiMS/govtt/image"
	_ "gitlab.com/AntiMS/govtt/message"
	_ "gitlab.com/AntiMS/govtt/migrate"
	_ "gitlab.com/AntiMS/govtt/object"
	_ "gitlab.com/AntiMS/govtt/session"
	_ "gitlab.com/AntiMS/govtt/static/css"
	_ "gitlab.com/AntiMS/govtt/static/html"
	_ "gitlab.com/AntiMS/govtt/static/js"

	"gitlab.com/AntiMS/govtt/csrf"
	"gitlab.com/AntiMS/govtt/initdb"
)

var httpPort int
var httpsPort int

var chainFile = filepath.Join("cert", "fullchain.pem")
var keyFile = filepath.Join("cert", "privkey.pem")

func init() {
	envHttpsPortStr := os.Getenv("HTTPS_PORT")
	envHttpsPort, err := strconv.Atoi(envHttpsPortStr)
	if err != nil {
		envHttpsPort = 443
	}
	httpPortStr := os.Getenv("HTTP_PORT")
	httpPort, err = strconv.Atoi(httpPortStr)
	if err != nil {
		httpPort = 80
	}

	info, err := os.Stat(chainFile)
	if err != nil || info.IsDir() {
		print(err.Error())
		print("\n")
		fmt.Println("Could not load fullchain. Disabling HTTPS.")
		return
	}
	info, err = os.Stat(keyFile)
	if err != nil || info.IsDir() {
		print(err.Error())
		print("\n")
		fmt.Println("Could not load privkey. Disabling HTTPS.")
		return
	}

	httpsPort = envHttpsPort
}

func main() {
	rand.Seed(time.Now().UTC().UnixNano())

	initdb.Init()

	handler := csrf.WrapHandler(http.DefaultServeMux)

	if httpsPort != 0 {
		fmt.Printf("Listening on ports %d and %d.\n", httpPort, httpsPort)
		go func() {
			log.Fatal(http.ListenAndServe(":"+strconv.Itoa(httpPort), http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				redirTo := "https://" + strings.Split(r.Host, ":")[0]
				if httpsPort != 443 {
					redirTo += ":" + strconv.Itoa(httpsPort)
				}
				redirTo += r.URL.RawPath
				http.Redirect(w, r, redirTo, http.StatusMovedPermanently)
			})))
		}()
		log.Fatal(http.ListenAndServeTLS(":"+strconv.Itoa(httpsPort), chainFile, keyFile, handler))
	} else {
		fmt.Printf("Listening on port %d.\n", httpPort)
		log.Fatal(http.ListenAndServe(":"+strconv.Itoa(httpPort), handler))
	}
}
