package csrf

import (
	"fmt"
	"net/http"

	"gitlab.com/AntiMS/govtt/session"
)

type csrfHandler struct {
	Wrapped http.Handler
}

func (c *csrfHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Authorization") != "" {
		c.Wrapped.ServeHTTP(w, r)
		return
	}

	session.DecorateHandlerFunc(func(w http.ResponseWriter, r *http.Request, s *session.Session) {
		var token string
		tokeni, ok := s.Get("csrf")
		if ok {
			token, ok = tokeni.(string)
			if !ok {
				http.Error(w, fmt.Sprintf("Token %#v from session is not of expected type.", tokeni), http.StatusInternalServerError)
				return
			}
		} else {
			token, err := generateToken()
			if err != nil {
				http.Error(w, "Failed to generate a token. "+err.Error(), http.StatusInternalServerError)
				return
			}
			s.Set("csrf", token)
		}

		w.Header().Set("X-CSRF-Token", token)

		if r.Method != "GET" && r.Method != "HEAD" && r.Method != "OPTIONS" && r.Method != "TRACE" {
			passedToken := r.Header.Get("X-CSRF-Token")
			if passedToken == "" {
				http.Error(w, "No CSRF token in request.", http.StatusForbidden)
				return
			}
			if passedToken != token {
				http.Error(w, "Passed CSRF token is incorrect.", http.StatusForbidden)
				return
			}
		}

		c.Wrapped.ServeHTTP(w, r)
	})(w, r)
}

// Wraps the given http.Handler, returning another http.Handler which provides CSRF protection.
func WrapHandler(h http.Handler) http.Handler {
	return &csrfHandler{Wrapped: h}
}
