/*
A CSRF-protection package implementing the synchronizer token pattern.

To protect all requests passing through http.DefaultServeMux, start your server as follows:

```
log.Fatal(http.ListenAndServe(":" + strPort, csrf.WrapHandler(http.DefaultServeMux)))
```
*/
package csrf

import (
	"crypto/rand"
	"encoding/base64"
	"errors"
)

func generateToken() (string, error) {
	enc := base64.RawStdEncoding

	tokenBytes := make([]byte, 32)
	n, err := rand.Read(tokenBytes)
	if err != nil {
		return "", err
	}
	if n < len(tokenBytes) {
		return "", errors.New("Could not obtain sufficient entropy.")
	}

	return enc.EncodeToString(tokenBytes), nil
}
