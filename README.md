GoVTT
=====
GoVTT is a web-based virtual tabletop designed for playing tabletop roleplaying games with others remotely. GoVTT uses Sqlite3 for persistent storage.

GoVTT takes a minimalistic approach. It is entirely system agnostic with no system-specific facilities, but aims to one day be very hackable, allowing for extensions and integrations not anticipated by its authors.

GoVTT also doesn't grant any more or less privileges to any user. GoVTT doesn't really know which participant is the Game Master. GoVTT assumes that players are trusted not to cheat. With a quick JavaScript one-liner in the developer console, it would be entirely possible to see a scene created by the Game Master but not intended to be seen, or to remove elements intended to obscure other elements the Game Master didn't intend players to see yet.

GoVTT is currently relatively early in development. It's "beta-quality" software, but as of the time of writing, works quite decently for my regularly-meeting TTRPG group for which I GM.

GoVTT is untested on any operating system but GNU/Linux.

Features
--------
* Tokens for representing creatures on the table.
* Images may be placed on the table and scaled up or down as well as "locked" so they cannot accidentally be moved. This is useful for using premade maps.
* Simple rectangular-grid-based maps can be made in GoVTT directly.
* Rectangular areas may be obscured.
* Integrated public dice-rolling supporting arbitrary expressions such as "7d8*2+2d6-3" (roll seven eight-sided dice, double the result, add the sum of two six-sided die rolls and subtract three.)
* "Pocket" feature for quick access to premade tokens.
* Private direct messaging.
* Fetching of images from arbitrary urls as well as by uploading from the local computer.
* Quick menu and keyboard shortcuts. Press "." in GoVTT to see a list of available shortcuts.
* Multiple separate campaigns in one instance/installation of GoVTT.

Build Instructions
------------------
Building requires Go version 1.19 or later. All build, setup, run, and update instructions will be written for running GoVTT on GNU/Linux, but if you're using another operating system, it shouldn't be difficult to adapt them to your operating system.

GoVTT has few dependencies, but one of its dependencies is go-sqlite3 which requires cgo. The reason why this is notable is because this makes cross-compilation more difficult than simply setting your GOOS and GOARCH environment variables. Cross compilation is a problem yet to be solved.

Building starts with obtaining a copy of the GoVTT source code. This can be done by downloading a source archive [from Gitlab](https://gitlab.com/AntiMS/govtt) or clonining via git:

```
git clone https://gitlab.com/AntiMS/govtt.git
```

Either way you get it, open a terminal in the source code location. You'll then need to run a few `go generate` commands which produce a few source files necessary to build working GoVTT executables.

```
MINIFY='false' GOVTT_TITLE='My GoVTT Instance' go generate -x gitlab.com/AntiMS/govtt/static/html
MINIFY='false' go generate -x gitlab.com/AntiMS/govtt/static/css
MINIFY='false' EXTRA_JS='/path/to/extra/js/directory' go generate -x gitlab.com/AntiMS/govtt/static/js
go generate -x gitlab.com/AntiMS/govtt/migrate
```

One or more of these commands may download dependencies.

Setting the `MINIFY` environment variable to 'false' leaves generated front-end code (HTML, CSS, or JavaScript) unminified for easier reading when debugging the client side. It's optional and defaults to 'true'.

The `GOVTT_TITLE` environment variable only affects what goes in the `<title></title>` of the page when visiting GoVTT from a browser. It's optional and defaults to 'GoVTT'.

The `EXTRA_JS` environment viable, also optional, points to a directory of JavaScript source files to be included (at the end) of the page JavaScript. This provides a way to extend GoVTT in some basic custom ways.

Finally, build GoVTT and supporting binaries.

```
go build -o govtt ./main
go build -o newuser ./auth/newuser.go
go build -o genpass ./auth/genpass.go
go build -o changepw ./auth/changepw.go
go build -o gentoken ./auth/gentoken.go
go build -o newcampaign ./campaign/newcampaign.go
go build -o grantcampaign ./campaign/grantcampaign.go
```

Setup And Run Instructions
--------------------------
To run GoVTT, you'll need copies of the necessary binaries:

* govtt
* newuser
* genpass
* changepw
* gentoken
* newcampaign
* grantcampaign

Before running GoVTT for the first time, you'll need to set up the (Sqlite3) database with a new user and campaign. The following commands can do this:

```
./newuser username password
./newcampaign username 'My Campaign' 'Game Master' 'Initial Scene'
```

The last two parameters of `newcampaign` are optional and default to the same value as the username and "Default" respectively.

These commands will create a new file named 'db.db' in the working directory. This is a Sqlit3 database which will store most of your campaign data, excluding static files uploaded to or fetched by GoVTT.

You can now run GoVTT.

```
HTTP_PORT=8080 HTTPS_PORT=8443 ./govtt
```

HTTPS is only enabled if the files `cert/fullchain.pem` and `cert/privkey.pem` (relative to the working directory) exist. Enabling HTTPS with a trusted trusted certificate is strongly recommended for the security of your GoVTT instance and your users. You can get a certificate trusted by all major browsers for free from [Let's Encrypt](https://letsencrypt.org).

If HTTPS is enabled (because the full chain and private key files exist), the HTTP port will function only to redirect users to the HTTPS port.

The `HTTP_PORT` and `HTTPS_PORT` environment variables specify on what ports to listen for HTTP and HTTPS connections respectively. Both environment variables are optional and default to 80 and 443 respectively.

Port numbers 1024 and above can be used freely, but ports below are restricted by most commonly-used operating systems. On GNU/Linux, to get around this restriction, allowing for listening on ports below 1024 as a non-root user, one can use the following command as root:

```
setcap 'cap_net_bind_service=+ep' govtt
```

This command will set a special property on the executable which henceforth allows it to listen on low ports even when invoked by non-root users.

Once GoVTT is running, point your browser to the proper location. If running GoVTT on your local machine on port 443, you can connect to it by using the url `https://localhost/`. You should be able to log in with the username and password you set above.

You can add more users to GoVTT with the following commands:

```
./newuser username password
./grantcampaign username 1 'Quimbly'
```

When you share a user's password with them, it's recommended to present it as a temporary password which the user should change at their earliest opportunity.

Uploading any images to GoVTT using the image upload feature will create the directory "uploads/images" relative to the working directory. Images will be stored in that directory.

To update GoVTT, simply replace the binaries with the newer versions and, if necessary, set the option on the new `govtt` binary which allows it to listen on lower ports.

Executing the new version of GoVTT (that is, executing any of the binaries) will automatically make any necessary changes to the Sqlite3 database necessary for the new version of GoVTT to work properly.

Legal Stuff
-----------
Copyright 2023 AntiMS

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received  a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
