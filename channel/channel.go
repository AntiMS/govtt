/*
Pushes events related to the current campaign, scene, and user to the client using Server-Sent Events. Receives events to be sent to the client from a bus.Bus.

Upon opening an SSE channel, subscribes to messages of the following forms:

```
[]string{"campaign", <campaign_id>, "campaignevent"}
[]string{"campaign", <campaign_id>, "user", <user_id>}
```

Messages of the following form can also be subscribed to using the "/channelScene/" handler (see below):

```
[]string{"campaign", <campaign_id>, "scene", <scene_id>}
```

Also, when first subscribed, sends the following message to the client where "<channel_key>" is a string identifier of the current channel:

```
{"name": "channelkey", "data": <channel_key>}
```

The channel key should be saved for later use in a call to the "/channelScene/" handler.

Post-processes messages from the bus before sending them to the client. All messages from busses for which a matching post-processor is not registered are post-processed such that the object sent to the client has a "name" value which is the final element of the "name" of the message from the bus and a "data" value which is the same as the "value" value from the bus message. (i.e. a `&bus.Message{Name: []string{"a", "b", "c"}, struct{A int}{A: 1}}` would be sent to the client as `{"name": "a", "data": {"A": 1}}`.)

One default post-processor is registered which alters all bus messages with the a name of the form `[]string{"campaign", <any>, "campaignevent", "newimage"}` to return a message with a name "newimage" and data which is the same object which is the "value" from the bus message with one additional key, "mine", which is "true" for images belonging to the current user and "false" for those not belonging to the current user.

HTTP Handlers
=============
This package registers several HTTP handlers detailed below:

/channel/<campaign_id>
----------------------
The Server-Sent Events handler. Only allows opening the channel if the user has permissions on the campaign.

Method: GET

Path parameters:

- campaign_id -- The id of the Campaign for which to open a channel.

/channelScene/<channel_key>/<scene_id>
--------------------------------------
Subscribes a specified channel to a specified scene. Only allows subscribing if the user was the one who opened the channel.

Method: POST

Path parameters:

- channel_key -- The channel key returned when first opening the channel for which to subscribe to a scene.
- scene_id -- The id of the scene for which to subscribe to events.

A successful response is a "204 No Content" response.
*/
package channel

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/AntiMS/govtt/auth"
	"gitlab.com/AntiMS/govtt/bus"
	"gitlab.com/AntiMS/govtt/initdb"
)

type channelData struct {
	Receiver   <-chan *bus.Message
	CampaignId int
	UserId     int
}

var channelDatas map[string]channelData = make(map[string]channelData)

var postprocessors map[string]func(n []string, v any, userId int) any = make(map[string]func(n []string, v any, userId int) any)

func postprocess(n []string, v any, userId int) any {
	keyList := make([]string, len(n))

	for i, nn := range n {
		var isnum bool = true
		for _, c := range nn {
			isnum = isnum && c >= '0' && c <= '9'
			if !isnum {
				break
			}
		}
		if isnum {
			keyList[i] = "*"
		} else {
			keyList[i] = nn
		}
	}
	key := strings.Join(keyList, ".")

	p, ok := postprocessors[key]
	if !ok {
		return &msg{Name: n[len(n)-1], Data: v}
	}

	return p(n, v, userId)
}

func generateKey() (string, error) {
	enc := base64.RawURLEncoding

	tokenBytes := make([]byte, 32)
	n, err := rand.Read(tokenBytes)
	if err != nil {
		return "", err
	}
	if n < len(tokenBytes) {
		return "", errors.New("Could not obtain sufficient entropy.")
	}

	return enc.EncodeToString(tokenBytes), nil
}

func writeMessage(w http.ResponseWriter, f http.Flusher, m any) {
	j, err := json.Marshal(m)
	if err != nil {
		j, err = json.Marshal(map[string]string{"type": "error", "message": err.Error()})
		if err != nil {
			panic(err)
		}
	}

	w.Write([]byte("data: "))
	w.Write(j)
	w.Write([]byte("\n\n"))

	if f != nil {
		f.Flush()
	}
}

func channel(w http.ResponseWriter, r *http.Request, u auth.User) {
	split := strings.Split(r.URL.Path, "/")
	if len(split) < 3 {
		http.Error(w, "Invalid request.", http.StatusBadRequest)
		return
	}

	campaignIdStr := split[2]
	campaignId, err := strconv.Atoi(campaignIdStr)
	if err != nil {
		http.Error(w, "Invalid request.", http.StatusBadRequest)
		return
	}

	flusher, ok := w.(http.Flusher)
	if !ok {
		log.Println("ResponseWriter does not satisfy http.Flusher")
	}

	query := "SELECT scene_id FROM campaign WHERE id = (SELECT campaign_id FROM campaign_user WHERE campaign_id = ? AND user_id = ?);"
	rows, err := initdb.DB.Query(query, campaignId, u.Id)
	if err != nil {
		http.Error(w, "Error querying database. "+err.Error(), http.StatusInternalServerError)
		return
	}

	defer rows.Close()

	if !rows.Next() {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	var sceneId int
	err = rows.Scan(&sceneId)
	if err != nil {
		http.Error(w, "Error scanning", http.StatusUnauthorized)
		return
	}

	rows.Close()

	key, err := generateKey()
	if err != nil {
		http.Error(w, "Error scanning", http.StatusUnauthorized)
		return
	}

	receiver := bus.NewReceiver()

	bus.Subscribe(receiver, []string{"campaign", strconv.Itoa(campaignId), "campaignevent"})
	bus.Subscribe(receiver, []string{"campaign", strconv.Itoa(campaignId), "user", strconv.Itoa(u.Id)})

	channelDatas[key] = channelData{Receiver: receiver, CampaignId: campaignId, UserId: u.Id}

	w.Header().Set("Content-Type", "text/event-stream")
	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("Connection", "keep-alive")
	w.Header().Set("X-Accel-Buffering", "no")
	w.Header().Set("Transfer-Encoding", "identity")
	w.WriteHeader(http.StatusOK)
	if flusher != nil {
		flusher.Flush()
	}

	writeMessage(w, flusher, &msg{Name: "channelkey", Data: key})

	var cont bool = true
	for cont {
		select {
		case m := <-receiver:
			if m == nil {
				cont = false
				break
			}
			writeMessage(w, flusher, postprocess(m.Name, m.Value, u.Id))
		case <-r.Context().Done():
			cont = false
		}
	}

	bus.UnsubscribeAll(receiver, nil)
	bus.CloseReceiver(receiver)
}

func subscribeScene(w http.ResponseWriter, r *http.Request, u auth.User) {
	if r.Method != "POST" {
		http.Error(w, "Please use POST.", http.StatusMethodNotAllowed)
		return
	}

	split := strings.Split(r.URL.Path, "/")
	if len(split) < 4 {
		http.Error(w, "Invalid request.", http.StatusBadRequest)
		return
	}

	key := split[2]
	sceneIdStr := split[3]
	sceneId, err := strconv.Atoi(sceneIdStr)
	if err != nil {
		http.Error(w, "Invalid request.", http.StatusBadRequest)
		return
	}

	channelData, ok := channelDatas[key]
	if !ok {
		http.Error(w, "No such channel", http.StatusUnauthorized)
		return
	}

	if channelData.UserId != u.Id {
		http.Error(w, "Wrong user", http.StatusUnauthorized)
		return
	}

	query := "SELECT id FROM scene WHERE campaign_id = ? AND id = ?;"
	rows, err := initdb.DB.Query(query, channelData.CampaignId, sceneId)
	if err != nil {
		http.Error(w, "Error querying database. "+err.Error(), http.StatusInternalServerError)
		return
	}

	defer rows.Close()

	if !rows.Next() {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	bus.UnsubscribeAll(channelData.Receiver, []string{"campaign", strconv.Itoa(channelData.CampaignId), "scene"})

	bus.Subscribe(channelData.Receiver, []string{"campaign", strconv.Itoa(channelData.CampaignId), "scene", strconv.Itoa(sceneId)})
}

func init() {
	http.HandleFunc("/channel/", auth.DecorateHandlerFunc(channel))
	http.HandleFunc("/channelScene/", auth.DecorateHandlerFunc(subscribeScene))
}
