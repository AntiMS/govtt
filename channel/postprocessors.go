/*
Channel post-processors transform bus messages to be delivered to the client before sending on the channel.

Channel post-processors are registered by being added to the `postprocessors` map. Keys are strings. These strings represent bus message names. The method for turning a message name into one of these key strings is as follows:

- For any items in the name which consist of only digits, replace the item with a single asterisk.
- Join all items into a single string, delimited by periods.

When the channel system receives a message from the bus, if the message name matches the key, the postprocessor function (the value in the postprocessors map) will be used instead of the default transformation.

The postprocessor function takes a name ([]string), value (any), and the userId (int) of the user who will receive the message. Returns an any which will be serialized using the Go standard library "json" package.

A note. The return value is the whole object to be serialized. If it is wished that the object have only a "name" and "data" key, the object the postprocessor function returns must include those.

Exactly one postprocessor is registered.

This postprocessor receives an image registered ("campaign.*.campaignevent.newimage") or image changed ("campaign.*.campaignevent.imagechanged") message and adds to the value a "mine" key which is true only if the receiving user is the owner of the image referenced.
*/
package channel

type msg struct {
	Name string `json:"name"`
	Data any    `json:"data"`
}

func imagePostProcessor(n []string, v any, userId int) any {
	m := v.(map[string]any)
	return &msg{Name: n[len(n)-1], Data: map[string]any{"id": m["id"], "name": m["name"], "mine": m["user_id"] == userId, "archived": m["archived"]}}
}

func init() {
	postprocessors["campaign.*.campaignevent.newimage"] = imagePostProcessor
	postprocessors["campaign.*.campaignevent.imagechanged"] = imagePostProcessor
}
