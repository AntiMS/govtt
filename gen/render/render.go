package render

import (
	"bytes"
	"fmt"
	"html/template"
	"io"
	"os"
	"path/filepath"
)

var cachedTemplates *template.Template = nil

// Given a Go-template-syntax filename location and value, renders the indicated template with the given dot value, returning an io.Reader of the rendered output (or any error encountered while attempting to render.)
func Render(templateName string, dot any) (io.Reader, error) {
	if cachedTemplates == nil {
		workingDir, err := os.Getwd()
		if err != nil {
			return nil, err
		}

		templatesDir := filepath.Join(filepath.Dir(filepath.Dir(workingDir)), "assets", "templates")

		templateFiles := make([]string, 0)
		err = filepath.Walk(templatesDir, func(path string, info os.FileInfo, err error) error {
			if info.IsDir() {
				return nil
			}

			templateFiles = append(templateFiles, path)

			return nil
		})
		if err != nil {
			return nil, err
		}

		cachedTemplates, err = template.ParseFiles(templateFiles...)
		if err != nil {
			return nil, err
		}

		// Add extra functions.
		cachedTemplates.Funcs(template.FuncMap{
			"html": func(v any) template.HTML {
				return template.HTML(fmt.Sprint(v))
			},
		})
	}

	buf := new(bytes.Buffer)

	err := cachedTemplates.ExecuteTemplate(buf, templateName, dot)
	if err != nil {
		return nil, err
	}

	return buf, nil
}
