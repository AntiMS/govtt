/*
Logic for concatenating JavaScript files in an order which guarantees all dependencies are defined before their dependents reference them.

Each JavaScript file must begin with a specific header comment of the following form:

```
/**
  * Optional comment. May be any number of lines and followed by a line with no text as in this example or not.
  *
  * Dependencies
  * ============
  * file_one
  * file_two
  * file_three
  * /
```

(Note: the last line of the above code example includes a space between the asterisk and forward slash. This space was necessary to include this comment in a Go file, but should be omitted in the JavaScript file.)

The output of this logic labels where the content of one of the original files ends and the next starts. Each label contains a file path relative to a base directory excluding extension (or an indication the content came from a non-file source).

This package (currently) only exposes a single function named "CatJs". See its comment for more information.
*/
package catjs

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
)

func resetReader(reader io.Reader) {
	seeker, ok := reader.(io.Seeker)
	if ok {
		seeker.Seek(0, io.SeekStart)
	}
	closer, ok := reader.(io.Closer)
	if ok {
		closer.Close()
	}
}

func fileByShortName(base, shortName string) string {
	return filepath.Join(base, shortName+".js")
}

func getReader(base, shortName string, preset map[string]io.Reader) (io.Reader, error) {
	if preset != nil {
		out, ok := preset[shortName]
		if ok {
			return out, nil
		}
	}

	out, err := os.Open(fileByShortName(base, shortName))
	if err != nil {
		return nil, err
	}

	return out, nil
}

func getDependencies(base, shortName string, preset map[string]io.Reader) ([]string, error) {
	reader, err := getReader(base, shortName, preset)
	if err != nil {
		return nil, err
	}

	got := make(map[string]bool)
	got["/**"] = false
	got[" * Dependencies"] = false
	got[" * ============"] = false
	got[" */"] = false

	out := make([]string, 0)

	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		l := scanner.Text()

		res, ok := got[l]
		if ok && !res {
			got[l] = true
			continue
		}

		got3 := got["/**"] && got[" * Dependencies"] && got[" * ============"]
		if got3 && !got[" */"] {
			if !strings.HasPrefix(l, " * ") {
				return nil, errors.New(fmt.Sprintf("Malformed dependency %#v in %s", l, shortName))
			}
			dep := l[3:]
			dep = strings.Split(dep, "#")[0]
			dep = strings.TrimSpace(dep)
			out = append(out, dep)
		} else if got3 && got[" */"] {
			break
		}
	}

	resetReader(reader)

	return out, nil
}

func getUnmet(met, files []string, preset map[string]io.Reader) []string {
	unmet := make([]string, 0)
	for _, f := range files {
		inOut := false
		for _, o := range met {
			inOut = o == f
			if inOut {
				break
			}
		}
		if !inOut {
			unmet = append(unmet, f)
		}
	}
	for f, _ := range preset {
		inOut := false
		for _, o := range met {
			inOut = o == f
			if inOut {
				break
			}
		}
		if !inOut {
			unmet = append(unmet, f)
		}
	}
	return unmet
}

func orderByDependency(base string, files []string, preset map[string]io.Reader) ([]string, error) {
	nd := make([]string, len(files))
	copy(nd, files)
	files = nd

	fileInfos := make(map[string]*struct {
		deps      []string
		satisfied bool
	})
	out := make([]string, 0)

	handleFile := func(file string) error {
		fileInfo, ok := fileInfos[file]
		if !ok {
			deps, err := getDependencies(base, file, preset)
			if err != nil {
				return err
			}
			fileInfo = &struct {
				deps      []string
				satisfied bool
			}{deps: deps, satisfied: false}
			fileInfos[file] = fileInfo
		} else if fileInfo.satisfied {
			return nil
		}

		var satisfied = true
		for _, d := range fileInfo.deps {
			fi, ok := fileInfos[d]
			if !ok || !fi.satisfied {
				satisfied = false
			}
		}

		if satisfied {
			out = append(out, file)
			fileInfo.satisfied = true
		}

		return nil
	}

	for len(out) < len(files)+len(preset) {
		prevLen := len(out)

		for k, _ := range preset {
			if err := handleFile(k); err != nil {
				return nil, err
			}
		}

		for _, file := range files {
			if err := handleFile(file); err != nil {
				return nil, err
			}
		}

		if len(out) == prevLen {
			fmt.Fprintln(os.Stderr, "Unmet javascript dependencies (colors: \x1b[31munmet\x1b[0m, \x1b[32mmet\x1b[0m):")
			unmet := getUnmet(out, files, preset)
			for _, f := range unmet {
				fmt.Fprintln(os.Stderr, f)
				deps, err := getDependencies(base, f, preset)
				if err != nil {
					fmt.Fprintln(os.Stderr, "Failed to print dependencies.")
					continue
				}
				for _, d := range deps {
					var met bool = true
					for _, f2 := range unmet {
						if d == f2 {
							met = false
							break
						}
					}
					var color string
					if met {
						color = "32"
					} else {
						color = "31"
					}
					fmt.Fprintln(os.Stderr, "  \x1b["+color+"m"+d+"\x1b[0m")
				}
			}
			panic(errors.New("Unmet javascript dependencies"))
		}
	}

	return out, nil
}

type openingReader struct {
	Base   string
	Name   string
	Preset map[string]io.Reader
	reader io.Reader
}

func (r *openingReader) Read(p []byte) (n int, err error) {
	if r.reader == nil {
		reader, err := getReader(r.Base, r.Name, r.Preset)
		if err != nil {
			panic(err)
		}
		r.reader = io.MultiReader(bytes.NewReader([]byte("\n\n// File: "+r.Name+".js\n")), reader)
	}

	out, outErr := r.reader.Read(p)

	if outErr != nil {
		resetReader(r.reader)
	}

	return out, outErr
}

/*
Given the following parameters, returns an io.Reader from which can be read to get JavaScript source code which is the concatenation of many JavaScript files.

Parameters
==========
base
: A base directory in which to look for JavaScript files to concatenate.

files
: An array of file paths (not including the .js extension) relative to base.

preset
: A map of io.Readers of JavaScript source code. May be specified instead of or in addition to filenames. These files are sorted into the output by dependency order.

extraJsFiles
: Absolute paths of files the content of which to append at the end of the output. This file content appears at the end and is not sorted by dependency or expected to have a dependency comment.

extraJsReaders
: io.Readers to append at the end of the output. This content appears at the end and is not sorted by dependency or expected to have a dependency comment.
*/
func CatJs(base string, files []string, preset map[string]io.Reader, extraJsFiles []string, extraJsReaders []io.Reader) (io.Reader, error) {
	files, err := orderByDependency(base, files, preset)
	if err != nil {
		return nil, err
	}

	readers := make([]io.Reader, 0, len(files)+len(extraJsFiles)+len(extraJsReaders))

	for _, file := range files {
		readers = append(readers, &openingReader{Base: base, Name: file, Preset: preset})
	}

	for _, file := range extraJsFiles {
		readers = append(readers, io.MultiReader(bytes.NewReader([]byte("\n\n// Extra file: "+file+"\n")), &openingReader{Name: strings.TrimSuffix(file, ".js")}))
	}

	for _, reader := range extraJsReaders {
		readers = append(readers, io.MultiReader(bytes.NewReader([]byte("\n\n// Unnamed File\n")), reader))
	}

	return io.MultiReader(readers...), nil
}
