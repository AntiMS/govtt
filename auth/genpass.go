//go:build ignore

/*
A standalone command which generates a password hash for a user and prints the hash.

Takes two arguments: a username and password.
*/
package main

import (
	"fmt"
	"os"

	"gitlab.com/AntiMS/govtt/auth"
	"gitlab.com/AntiMS/govtt/initdb"
	_ "gitlab.com/AntiMS/govtt/migrate"
)

func main() {
	initdb.Init()

	hash, err := auth.GeneratePasswordHash(os.Args[1], os.Args[2])
	if err != nil {
		panic(err)
	}
	fmt.Println(hash)
}
