//go:build ignore

/*
A standalone command which changes a user's password to that given.

Takes two arguments: a username and password.
*/
package main

import (
	"fmt"
	"os"

	"gitlab.com/AntiMS/govtt/auth"
	"gitlab.com/AntiMS/govtt/initdb"
	_ "gitlab.com/AntiMS/govtt/migrate"
)

func main() {
	if len(os.Args) != 3 {
		fmt.Fprintln(os.Stderr, "Please provide a username and password as arguments.")
		os.Exit(1)
	}

	username := os.Args[1]
	pass := os.Args[2]

	initdb.Init()

	user, err := auth.UserByUsername(username)
	if err != nil {
		panic(err)
	}

	err = auth.SetPassword(user, pass)
	if err != nil {
		panic(err)
	}
}
