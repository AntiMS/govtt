package auth

import (
	"bytes"
	"crypto/rand"
	"crypto/sha512"
	"encoding/base64"
	"errors"
	"strings"
)

func init() {
	name := "saltedbase64sha512"

	matchers[name] = func(_, pass, hash string) (bool, error) {
		split := strings.Split(hash, "$")
		if len(split) != 3 {
			return false, errors.New("Invalid password hash; fewer or more than 3 '$'-delimited fields")
		}

		salt := split[1]
		hash = split[2]

		enc := base64.RawStdEncoding

		saltBytes, err := enc.DecodeString(salt)
		if err != nil {
			return false, errors.New("Invalid password hash; could not base64-decode salt")
		}
		hashBytes, err := enc.DecodeString(hash)
		if err != nil {
			return false, errors.New("Invalid password hash; could not base64-decode hash")
		}

		hasher := sha512.New()
		hasher.Write([]byte(pass))
		hasher.Write(saltBytes)
		state := hasher.Sum(nil)

		return bytes.Equal(state, hashBytes), nil
	}

	hashers[name] = func(_, pass string) (string, error) {
		salt := make([]byte, 32)
		n, err := rand.Read(salt)
		if err != nil {
			return "", err
		}
		if n < len(salt) {
			return "", errors.New("Error generating salt")
		}

		hasher := sha512.New()
		hasher.Write([]byte(pass))
		hasher.Write(salt)
		state := hasher.Sum(nil)

		enc := base64.RawStdEncoding

		return name + "$" + enc.EncodeToString(salt) + "$" + enc.EncodeToString(state), nil
	}

	defaultHasher = name
}
