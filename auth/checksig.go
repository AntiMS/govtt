//go:build ignore

/*
A standalone command which checks the correctness of a user's authentication signature.

Takes as arguments: a username and signature.
*/
package main

import (
	"fmt"
	"os"

	"gitlab.com/AntiMS/govtt/auth"
	"gitlab.com/AntiMS/govtt/initdb"
	_ "gitlab.com/AntiMS/govtt/migrate"
)

func main() {
	if len(os.Args) != 3 {
		fmt.Fprintln(os.Stderr, "Please provide a username and signature.")
		os.Exit(1)
	}

	username := os.Args[1]
	sig := os.Args[2]

	initdb.Init()

	user, err := auth.CheckSignatureWithVerbosity(username, sig, true)
	if err != nil {
		panic(err)
	}

	fmt.Printf("Successfully got user: %#v\n", user)
}
