//go:build ignore

/*
A standalone command which creates a new user in the database.

Takes two arguments: a username and a password.
*/
package main

import (
	"fmt"
	"os"

	"gitlab.com/AntiMS/govtt/auth"
	"gitlab.com/AntiMS/govtt/initdb"
	_ "gitlab.com/AntiMS/govtt/migrate"
)

func main() {
	if len(os.Args) != 3 {
		fmt.Fprintln(os.Stderr, "Please provide a username and password as arguments.")
		os.Exit(1)
	}

	username := os.Args[1]
	pass := os.Args[2]

	initdb.Init()

	err := auth.NewUser(username, pass)
	if err != nil {
		panic(err)
	}
}
