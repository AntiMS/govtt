package auth

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"gitlab.com/AntiMS/govtt/session"
)

func login(w http.ResponseWriter, r *http.Request, s *session.Session) {
	if r.Method != "POST" {
		http.Error(w, "Bad method.", http.StatusBadRequest)
		return
	}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "Failed to read body. "+err.Error(), http.StatusInternalServerError)
		return
	}

	req := make(map[string]string)
	if err = json.Unmarshal(body, &req); err != nil {
		http.Error(w, "Failed to parse body. "+err.Error(), http.StatusBadRequest)
		return
	}

	username := req["username"]
	password := req["password"]

	user, err := checkPassword(username, password)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	s.Set("user", user)
	w.WriteHeader(http.StatusNoContent)
}

func loginBySignature(w http.ResponseWriter, r *http.Request, s *session.Session) {
	if r.Method != "POST" {
		http.Error(w, "Bad method.", http.StatusBadRequest)
		return
	}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "Failed to read body. "+err.Error(), http.StatusInternalServerError)
		return
	}

	req := make(map[string]string)
	if err = json.Unmarshal(body, &req); err != nil {
		http.Error(w, "Failed to parse body. "+err.Error(), http.StatusBadRequest)
		return
	}

	username := req["username"]
	signature := req["signature"]

	user, err := CheckSignature(username, signature)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	s.Set("user", user)
	w.WriteHeader(http.StatusNoContent)
}

func logout(w http.ResponseWriter, r *http.Request, s *session.Session) {
	if r.Method != "GET" {
		http.Error(w, "Bad method.", http.StatusBadRequest)
		return
	}

	s.Del("user")
	w.WriteHeader(http.StatusNoContent)
}

func getUsername(w http.ResponseWriter, r *http.Request, s *session.Session) {
	if r.Method != "GET" {
		http.Error(w, "Bad method.", http.StatusBadRequest)
		return
	}

	useri, ok := s.Get("user")
	if !ok {
		http.Error(w, "Not authenticated", http.StatusForbidden)
		return
	}
	user, ok := useri.(User)
	if !ok {
		http.Error(w, fmt.Sprintf("User %#v not of expected type.", useri), http.StatusInternalServerError)
		return
	}
	http.ServeContent(w, r, "name", time.Time{}, bytes.NewReader([]byte(user.Username)))
}

func changePass(w http.ResponseWriter, r *http.Request, s *session.Session) {
	if r.Method != "POST" {
		http.Error(w, "Bad method.", http.StatusBadRequest)
		return
	}

	useri, ok := s.Get("user")
	if !ok {
		http.Error(w, "Not authenticated", http.StatusForbidden)
		return
	}
	user, ok := useri.(User)
	if !ok {
		http.Error(w, fmt.Sprintf("User %#v not of expected type.", useri), http.StatusInternalServerError)
		return
	}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "Failed to read body. "+err.Error(), http.StatusInternalServerError)
		return
	}

	req := make(map[string]string)
	if err = json.Unmarshal(body, &req); err != nil {
		http.Error(w, "Failed to parse body.", http.StatusBadRequest)
		return
	}

	oldPassword := req["old_password"]
	newPassword := req["new_password"]

	_, err = checkPassword(user.Username, oldPassword)
	if err != nil {
		http.Error(w, "Failed to check password. "+err.Error(), http.StatusBadRequest)
		return
	}

	SetPassword(user, newPassword)

	w.WriteHeader(http.StatusNoContent)
}

func init() {
	http.HandleFunc("/login", session.DecorateHandlerFunc(login))
	http.HandleFunc("/loginBySignature", session.DecorateHandlerFunc(loginBySignature))
	http.HandleFunc("/logout", session.DecorateHandlerFunc(logout))
	http.HandleFunc("/user", session.DecorateHandlerFunc(getUsername))
	http.HandleFunc("/changePw", session.DecorateHandlerFunc(changePass))
}

// Returns a new handler func (to be passed with http.HandleFunc(...)) which calls the argument (which is a func) with the http.ResponseWriter, *http.Request, and the User currently in the session.
// Or responds to the request with a forbidden status if there is no user in the session.
func DecorateHandlerFunc(f func(w http.ResponseWriter, r *http.Request, u User)) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		var err string
		var done bool

		func(w http.ResponseWriter, r *http.Request) {
			h := r.Header.Get("Authorization")
			if h == "" {
				return
			}

			split := strings.SplitN(h, " ", 2)
			if len(split) < 2 {
				err = "Invalid authorization header"
				return
			}

			if split[0] != "Signature" {
				err = "Invalid authorization header type"
				return
			}

			split = strings.SplitN(split[1], ":", 2)
			if len(split) < 2 {
				err = "Invalid authorization header signature"
				return
			}

			u, e := CheckSignature(split[0], split[1])
			if e != nil {
				err = e.Error()
				return
			}

			f(w, r, u)

			done = true
		}(w, r)

		if err != "" {
			http.Error(w, err, http.StatusForbidden)
			return
		}

		if done {
			return
		}

		session.DecorateHandlerFunc(func(w http.ResponseWriter, r *http.Request, s *session.Session) {
			useri, ok := s.Get("user")
			if !ok {
				http.Error(w, "No user in session.", http.StatusForbidden)
				return
			}
			user, ok := useri.(User)
			if !ok {
				http.Error(w, fmt.Sprintf("User %#v not of expected type.", useri), http.StatusInternalServerError)
				return
			}
			f(w, r, user)
		})(w, r)
	}
}
