//go:build ignore

/*
A standalone command which generates and sets an authentication token for a user in the database.

Takes one argument: a username.
*/
package main

import (
	"fmt"
	"os"

	"gitlab.com/AntiMS/govtt/auth"
	"gitlab.com/AntiMS/govtt/initdb"
	_ "gitlab.com/AntiMS/govtt/migrate"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Fprintln(os.Stderr, "Please provide a username as an argument.")
		os.Exit(1)
	}

	username := os.Args[1]

	initdb.Init()

	user, err := auth.UserByUsername(username)
	if err != nil {
		panic(err)
	}

	token, err := auth.SetToken(user)
	if err != nil {
		panic(err)
	}

	fmt.Println(token)
}
