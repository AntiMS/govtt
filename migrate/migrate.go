// Applies database connections on database connect.
package migrate

import (
	"fmt"
	"os"

	"gitlab.com/AntiMS/govtt/initdb"
)

func createTable() {
	fmt.Fprintln(os.Stderr, "Did not find migration table. Creating.")

	// The "collection" column in this table is reserved for future use by plugins.
	_, err := initdb.DB.Exec(`
		CREATE TABLE migration (
			id integer primary key autoincrement,
			collection text,
			name text not null,
			hash blob not null
		);
	`)
	if err != nil {
		panic(err)
	}
}

func cmpByteSlice(a, b []byte) bool {
	if len(a) != len(b) {
		return false
	}

	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

func applyMigrations() {
	rows, err := initdb.DB.Query("SELECT name, hash FROM migration;")
	if err != nil {
		panic(err)
	}

	i := 0
	var name string
	var hash []byte
	for rows.Next() {
		err = rows.Scan(&name, &hash)
		if err != nil {
			panic(err)
		}

		if !cmpByteSlice(hash, migrations[i].s) {
			panic(fmt.Sprintf("Migration %#v hash does not match.", name))
		}

		i++
	}

	rows.Close()

	for ; i < len(migrations); i++ {
		fmt.Fprintf(os.Stderr, "Applying migration %#v.\n", migrations[i].n)

		_, err = initdb.DB.Exec(migrations[i].c)
		if err != nil {
			panic(fmt.Sprintf("Error applying migration %#v. Error was %#v.", migrations[i].n, err.Error()))
		}

		_, err := initdb.DB.Exec("INSERT INTO migration (name, hash) VALUES(?, ?);", migrations[i].n, migrations[i].s)
		if err != nil {
			panic(fmt.Sprintf("Error marking migration %#v applied. Error was %#v.", migrations[i].n, err.Error()))
		}
	}
}

func migrate() {
	rows, err := initdb.DB.Query("SELECT name FROM sqlite_master WHERE type = 'table' AND name = 'migration';")
	if err != nil {
		panic(err)
	}
	tableExists := rows.Next()
	rows.Close()

	if !tableExists {
		createTable()
	}

	applyMigrations()
}

// Does database migration on database connect
func init() {
	initdb.OnConnect(migrate)
}
