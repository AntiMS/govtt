package message

import (
	"errors"
	"fmt"
	"math"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"

	"gitlab.com/AntiMS/govtt/auth"
	"gitlab.com/AntiMS/govtt/initdb"
)

const (
	LOWEST_PRIORITY = iota
	PLUS
	MINUS
	TIMES
	DIVIDE
	DIE
	OPENPAREN
	CLOSEPAREN
	COMMA
	NUMBER
	BEST
	WORST
	HIGHEST_PRIORITY
)

var tokenKindNames map[uint8]string = map[uint8]string{
	COMMA:      "COMMA",
	PLUS:       "PLUS",
	MINUS:      "MINUS",
	TIMES:      "TIMES",
	DIVIDE:     "DIVIDE",
	DIE:        "DIE",
	OPENPAREN:  "OPENPAREN",
	CLOSEPAREN: "CLOSEPAREN",
	NUMBER:     "NUMBER",
	BEST:       "BEST",
	WORST:      "WORST",
}

func tokenKindName(t token) string {
	if t == nil {
		return "NIL_TOKEN"
	}
	n, ok := tokenKindNames[t.Kind()]
	if !ok {
		return fmt.Sprintf("UNRECOGNIZED_TOKEN:%d", t.Kind())
	}
	return n
}

type token interface {
	Kind() uint8
	Data() any
}
type noData uint8

func (n noData) Kind() uint8 { return uint8(n) }
func (n noData) Data() any   { return nil }

type withData struct {
	kind uint8
	data any
}

func (w withData) Kind() uint8 { return w.kind }
func (w withData) Data() any   { return w.data }

func tokenize(expr string) ([]token, error) {
	scts := map[rune]uint8{
		'+': PLUS,
		'-': MINUS,
		'*': TIMES,
		'/': DIVIDE,
		'(': OPENPAREN,
		')': CLOSEPAREN,
		',': COMMA,
		'b': BEST,
		'w': WORST,
		'd': DIE,
	}
	out := make([]token, 0)
	var n *int
	for _, c := range expr {
		sct, ok := scts[c]
		if ok {
			if n != nil {
				out = append(out, withData{kind: NUMBER, data: *n})
				n = nil
			}
			out = append(out, noData(sct))
			continue
		}
		if c > '9' || c < '0' {
			return nil, errors.New(fmt.Sprintf("Unexpected charcter '%c' in dice expression", c))
		}
		if n == nil {
			t := 0
			n = &t
		}
		*n = *n*10 + int(c-'0')
	}
	if n != nil {
		out = append(out, withData{kind: NUMBER, data: *n})
		n = nil
	}
	return out, nil
}

type expression interface {
	Value() int
	Expansion() string
}
type number int

func (n number) Value() int        { return int(n) }
func (n number) Expansion() string { return strconv.Itoa(int(n)) }

type operator struct {
	Expr1, Expr2 expression
	Op           rune
}

func (o *operator) Value() int {
	e1 := o.Expr1.Value()
	e2 := o.Expr2.Value()
	switch o.Op {
	case '+':
		return e1 + e2
	case '-':
		return e1 - e2
	case '*':
		return e1 * e2
	case '/':
		// By default, rounding rounds toward zero.
		// Better would be toward negative infinity.
		// But this isn't likely to ever be an issue.
		return e1 / e2
	default:
		panic(fmt.Sprintf("Unrecognized operator '%c'", o.Op))
	}
}
func (o *operator) Expansion() string {
	return fmt.Sprintf("%s %c %s", o.Expr1.Expansion(), o.Op, o.Expr2.Expansion())
}

type function struct {
	Fn   string
	Args []expression
}

func (f *function) Value() int {
	getMost := func(initial int, cmp func(v, ov int) bool) func(args ...expression) int {
		return func(args ...expression) int {
			out := initial
			for _, a := range args {
				v := a.Value()
				if cmp(v, out) {
					out = v
				}
			}
			return out
		}
	}
	return map[string]func(args ...expression) int{
		"b": getMost(math.MinInt, func(v, ov int) bool { return v > ov }),
		"w": getMost(math.MaxInt, func(v, ov int) bool { return v < ov }),
	}[f.Fn](f.Args...)
}
func (f *function) Expansion() string {
	exps := make([]string, 0, len(f.Args))
	for _, a := range f.Args {
		exps = append(exps, a.Expansion())
	}
	return fmt.Sprintf("%s(%s)", f.Fn, strings.Join(exps, ", "))
}

type die struct {
	Count, Sides int
	vals         []int
}

func (d *die) roll() {
	if d.vals == nil {
		d.vals = make([]int, 0, d.Count)
		for i := int(0); i < d.Count; i++ {
			d.vals = append(d.vals, int(rand.Intn(d.Sides)+1))
		}
	}
}
func (d *die) Value() int {
	d.roll()
	var out int = 0
	for _, v := range d.vals {
		out += v
	}
	return out
}
func (d *die) Expansion() string {
	d.roll()
	out := make([]string, 0, d.Count)
	for _, v := range d.vals {
		out = append(out, strconv.Itoa(v))
	}
	return strings.Join(out, "+")
}

type parenthetical struct {
	Expr expression
}

func (p *parenthetical) Value() int {
	return p.Expr.Value()
}
func (p *parenthetical) Expansion() string {
	return fmt.Sprintf("(%s)", p.Expr.Expansion())
}

func parse(tokens []token) (expression, error) {
	var peek func() token
	var pop func() token
	var prefix func() (expression, error)
	var infix func(left expression) (expression, error)
	var doParse func(precedence uint8) (expression, error)

	peek = func() token {
		if len(tokens) == 0 {
			return nil
		}
		return tokens[0]
	}
	pop = func() token {
		if len(tokens) == 0 {
			return nil
		}
		out := tokens[0]
		tokens = tokens[1:]
		return out
	}

	prefix = func() (expression, error) {
		token := peek()
		if token == nil {
			return nil, errors.New(fmt.Sprintf("Expected more than zero tokens"))
		}

		var funcname string
		switch token.Kind() {
		case BEST:
			funcname = "b"
			fallthrough
		case WORST:
			if funcname == "" {
				funcname = "w"
			}
			pop()
			if peek() == nil || peek().Kind() != OPENPAREN {
				return nil, errors.New(fmt.Sprintf("Unexpected %s after function name", tokenKindName(peek())))
			}
			args := make([]expression, 0)
			for peek().Kind() == COMMA || len(args) == 0 {
				pop()
				arg, err := doParse(LOWEST_PRIORITY)
				if err != nil {
					return nil, err
				}
				args = append(args, arg)
			}
			if peek() == nil || peek().Kind() != CLOSEPAREN {
				return nil, errors.New("Missing close parenthesis at end of function call")
			}
			if len(args) == 0 {
				return nil, errors.New("Function calls with zero arguments are illegal")
			}
			pop()
			return &function{Fn: funcname, Args: args}, nil
		case NUMBER:
			num := pop().Data().(int)
			return number(num), nil
		case OPENPAREN:
			pop()
			out, err := doParse(LOWEST_PRIORITY)
			if err != nil {
				return nil, err
			}
			if peek() == nil || peek().Kind() != CLOSEPAREN {
				return nil, errors.New("Missing close parenthesis at end of parenthetical expression")
			}
			pop()
			_, ok := out.(*parenthetical)
			if ok {
				return out, nil
			}
			return &parenthetical{Expr: out}, nil
		case PLUS, MINUS, TIMES, DIVIDE, DIE, COMMA:
			return nil, errors.New(fmt.Sprintf("Unexpected %s", tokenKindName(peek())))
		default:
			return nil, errors.New(fmt.Sprintf("Illegal token %s when parsing expression", tokenKindName(peek())))
		}
	}
	infix = func(left expression) (expression, error) {
		var op rune = '_'
		switch peek().Kind() {
		case PLUS:
			op = '+'
			fallthrough
		case MINUS:
			if op == '_' {
				op = '-'
			}
			fallthrough
		case TIMES:
			if op == '_' {
				op = '*'
			}
			fallthrough
		case DIVIDE:
			if op == '_' {
				op = '/'
			}
			k := peek().Kind()
			pop()
			right, err := doParse(k)
			if err != nil {
				return nil, err
			}
			return &operator{Expr1: left, Expr2: right, Op: op}, nil
		case DIE:
			pop()
			count, ok := left.(number)
			if !ok {
				return nil, errors.New("Die count is not a number")
			}
			right, err := prefix()
			if err != nil {
				return nil, err
			}
			sides, ok := right.(number)
			if !ok {
				return nil, errors.New("Die sides not a number")
			}
			return &die{Count: int(count), Sides: int(sides)}, nil
		default:
			return nil, errors.New(fmt.Sprintf("Illegal token %s when parsing expression", tokenKindName(peek())))
		}
	}
	doParse = func(precedence uint8) (expression, error) {
		e, err := prefix()
		if err != nil {
			return nil, err
		}
		if len(tokens) == 0 {
			return e, nil
		}
		switch peek().Kind() {
		case OPENPAREN, NUMBER, BEST, WORST, HIGHEST_PRIORITY:
			return nil, errors.New(fmt.Sprintf("Unexpected token %s after prefix expression", tokenKindName(peek())))
		}
		for peek() != nil && peek().Kind() != OPENPAREN && peek().Kind() != CLOSEPAREN && peek().Kind() != COMMA && precedence < peek().Kind() {
			e, err = infix(e)
			if err != nil {
				return nil, err
			}
		}
		return e, nil
	}
	out, err := doParse(LOWEST_PRIORITY)
	if err != nil {
		return nil, err
	}
	if peek() != nil {
		return nil, errors.New("Unexpected tokens following expression")
	}
	return out, nil
}

type diceRoll struct {
	Timestamp   int64  `json:"timestamp"`
	Username    string `json:"username"`
	DisplayName string `json:"display_name"`
	Expr        string `json:"expr"`
	Expansion   string `json:"expansion"`
	Total       int    `json:"total"`
}

func init() {
	http.HandleFunc("/rolldice/", sendMessage(func(path string, parsed any, u auth.User) ([]string, any, error) {
		split := strings.Split(path, "/")
		if len(split) != 3 {
			return nil, nil, invalidRequest
		}

		campaignIdStr := split[2]
		campaignId, err := strconv.Atoi(campaignIdStr)
		if err != nil {
			return nil, nil, invalidRequest
		}

		expr, ok := parsed.(string)
		if !ok {
			return nil, nil, invalidRequest
		}

		if len(expr) <= 0 {
			return nil, nil, errors.New("Dice expression must not be empty")
		}

		query := "SELECT display_name FROM campaign_user WHERE campaign_id = ? AND user_id = ?;"
		rows, err := initdb.DB.Query(query, campaignId, u.Id)
		if err != nil {
			return nil, nil, errors.New("Internal server error")
		}

		var displayName string
		if !rows.Next() {
			return nil, nil, errors.New("Unauthorized")
		}

		err = rows.Scan(&displayName)
		if err != nil {
			return nil, nil, errors.New("Internal server error")
		}

		rows.Close()

		expr = strings.ReplaceAll(expr, " ", "")

		ts, err := tokenize(expr)
		if err != nil {
			return nil, nil, err
		}

		e, err := parse(ts)
		if err != nil {
			return nil, nil, err
		}

		name := []string{"campaign", strconv.Itoa(campaignId), "campaignevent", "diceroll"}
		out := diceRoll{
			Timestamp:   time.Now().UnixNano() / 1000000,
			Username:    u.Username,
			DisplayName: displayName,
			Expr:        expr,
			Expansion:   e.Expansion(),
			Total:       e.Value(),
		}

		return name, out, nil
	}))
}
