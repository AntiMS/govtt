package message

import (
	"errors"
	"net/http"
	"strconv"
	"strings"
	"time"

	"gitlab.com/AntiMS/govtt/auth"
	"gitlab.com/AntiMS/govtt/initdb"
)

type directMessage struct {
	Timestamp   int64  `json:"timestamp"`
	Username    string `json:"username"`
	DisplayName string `json:"display_name"`
	Text        string `json:"text"`
}

func init() {
	http.HandleFunc("/directmessage/", sendMessage(func(path string, parsed any, u auth.User) ([]string, any, error) {
		split := strings.Split(path, "/")
		if len(split) != 4 {
			return nil, nil, invalidRequest
		}

		campaignIdStr := split[2]
		campaignId, err := strconv.Atoi(campaignIdStr)
		if err != nil {
			return nil, nil, invalidRequest
		}

		toUsername := split[3]

		text, ok := parsed.(string)
		if !ok {
			return nil, nil, invalidRequest
		}

		query := "SELECT display_name FROM campaign_user WHERE campaign_id = ? AND user_id = ?;"
		rows, err := initdb.DB.Query(query, campaignId, u.Id)
		if err != nil {
			return nil, nil, errors.New("Internal server error.")
		}

		var displayName string
		if !rows.Next() {
			return nil, nil, errors.New("Unauthorized.")
		}

		err = rows.Scan(&displayName)
		if err != nil {
			return nil, nil, errors.New("Internal server error.")
		}

		rows.Close()

		var toUserId int
		query = "SELECT id FROM user WHERE username = ? AND id IN (SELECT user_id FROM campaign_user WHERE campaign_id = ?);"
		rows, err = initdb.DB.Query(query, toUsername, campaignId)
		if err != nil {
			return nil, nil, errors.New("Internal server error.")
		}
		if !rows.Next() {
			return nil, nil, errors.New("Unauthorized.")
		}
		err = rows.Scan(&toUserId)
		if err != nil {
			return nil, nil, err
		}
		rows.Close()

		name := []string{"campaign", strconv.Itoa(campaignId), "user", strconv.Itoa(toUserId), "directmessage"}
		out := directMessage{
			Timestamp:   time.Now().UnixNano() / 1000000,
			Username:    u.Username,
			DisplayName: displayName,
			Text:        text,
		}

		return name, out, nil
	}))
}
