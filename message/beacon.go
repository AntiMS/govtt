package message

import (
	"errors"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/AntiMS/govtt/auth"
	"gitlab.com/AntiMS/govtt/initdb"
)

func init() {
	http.HandleFunc("/beacon/", sendMessage(func(path string, parsed any, u auth.User) ([]string, any, error) {
		split := strings.Split(path, "/")
		if len(split) != 4 {
			return nil, nil, invalidRequest
		}

		campaignIdStr := split[2]
		campaignId, err := strconv.Atoi(campaignIdStr)
		if err != nil {
			return nil, nil, invalidRequest
		}

		sceneIdStr := split[3]
		sceneId, err := strconv.Atoi(sceneIdStr)
		if err != nil {
			return nil, nil, invalidRequest
		}

		m, ok := parsed.(map[string]any)
		if !ok {
			return nil, nil, invalidRequest
		}

		if len(m) != 2 {
			return nil, nil, invalidRequest
		}

		_, ok = m["x"].(float64)
		if !ok {
			return nil, nil, invalidRequest
		}
		_, ok = m["y"].(float64)
		if !ok {
			return nil, nil, invalidRequest
		}

		query := "SELECT id FROM scene WHERE (campaign_id = ? AND campaign_id IN (SELECT campaign_id FROM campaign_user WHERE user_id = ?) AND id = ?) OR user_id = ?;"
		rows, err := initdb.DB.Query(query, campaignId, u.Id, sceneId, u.Id)
		if err != nil {
			return nil, nil, errors.New("Internal server error.")
		}
		if !rows.Next() {
			return nil, nil, errors.New("Unauthorized.")
		}
		rows.Close()

		return []string{"campaign", strconv.Itoa(campaignId), "scene", strconv.Itoa(sceneId), "beacon"}, m, nil
	}))
}
