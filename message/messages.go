/*
This package holds infrastructure for several "messages" -- non-persistent values which go to the default bus.

Message Types
=============

Beacon
------
Beacons are simple indicators which highlight a location on the table.

Message name example:

```
[]string{"campaign", "123", "scene", "123", "beacon"}
```

Message value example JSON:

```
{"x": 123, "y": 123}
```

Dice
----
A dice roll.

Message name example:

```
[]string{"campaign", "123", "campaignevent", "diceroll"}
```

Message value example JSON:

```

	{
		"timestamp": 1234567890,
		"username": "antims",
		"display_name": "AntiMS",
		"expr": "1d20+3",
		"expansion": "16+3",
		"total": 19
	}

```

Direct Message
--------------

Message name example:

```
[]string{"campaign", "123", "user", "123", "directmessage"}
```

Message value example JSON:

```

	{
		"timestamp": 1234567890,
		"username": "antims",
		"display_name": "AntiMS",
		"text": "Roll me a Wis save in secret, please."
	}

```

HTTP Handlers
=============
This package registers several HTTP handlers related to the various message types.

/beacon/<campaign_id>/<scene_id>
--------------------------------
Places a "beacon" (a "location indicactor") on the table by which one player may direct other players' attention to a specific location.

Method: POST

Path parameters:

- campaign_id -- The id of the campaign containing the referenced scene.
- scene_id -- The id of the scene on which to place the beacon.

Request JSON structure:

```
{"x": <x coordinate>, "y": <y coordinate>}
```

Both coordinates are floating point numbers.

A successful response is a "204 No Content" response.

This call, when successful, also sends a message to the default bus of the form:

```
&bus.Message{Name: []string{"campaign", "<campaign_id>", "scene", "<scene_id>", "beacon"}, Value: map[string]any{"x": <x_coordinate>, "y": <y_coordinate>}}
```

/rolldice/<campaign_id>
-----------------------
Rolls (virtual) dice and publicly shares the dice roll result with all players currently on the associated campaign.

Method: POST

Path parameters:

- campaign_id -- The id of the campaign to which to associate this dice roll.

Request JSON structure:

```
"<dice_exression>"
```

A valid dice expression is of the following form (specified in EBNF):

```
digit excluding zero = "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" ;
digit = "0" | digit excluding zero ;
number = digit excluding zero, { digit }
subexpression = ( number, [ "d", number ] ) | ( [ number ], "d", number )
dice expression = subexpression, { ( "+" | "-" ), subexpression }
```

Some example expressions:

- 1d20 -- A single d20 roll
- d20 -- Equivalent to 1d20
- d20+d6+4 -- Sum the result of a d20 roll, the result of a d6 roll, and the number 4
- 2d20+d6-2 -- Add together the result of a roll of two d20s and one d6 and subtract 2 from the result
- 2d20-d6-d4-2 -- Subtract the result of a d6 roll, the result of a d4 roll, and the number 2 from the sum of the results of two d20 rolls
- 1+2+3+4+5 -- Sum the numbers 1, 2, 3, 4, and 5
- d7 -- A single d7 roll
- d100 -- A single d100 roll

The following are not valid dice expressions (currently):

- d20 + 2 -- Spaces included in the expression are not allowed
- 02d20 -- Numbers cannot start with zeroes
- d04 -- Numbers cannot start with zeroes
- -d20 -- Dice expressions cannot start with a hyphen
- d4+1.5 -- Decimals are not allowed in dice expressions
- d6/2 -- Division is not supported
- d20*2 -- Multiplication is not supported

A successful response is a "204 No Content" response.

This call, when successful, also sends a message to the default bus of the form:

```

	&bus.Message{
		Name: []string{"campaign", <campaign_id>, "campaignevent", "diceroll"},
		Value: map[string]any{
			"timestamp": <millisecond_timestamp>,
			"user": {"username": "<username>", "id": <user_id>},
			"expr": "<expression>",
			"expansion": "<expansion>",
			"total": <total>
		}
	}

```

"<expression>" is the expression originally passed as a string. "<expansion>" is a breakdown of all of the individual rolls as a string. An example expansion for the expression "2d20+3d4+2" might be "(16+7) + (2+1+3) + 2". <total> is the total as an integer.

/directmessage/<campaign_id>/<to_username>
------------------------------------------
Sends a direct message to a given user on the given campaign.

Method: POST

Path parameters:

- campaign_id -- The id of the campaign on which to send the direct message.

The request body should just be the message to send.

A successful response is a "204 No Content" response.

This call, when successful, also sends a message to the default bus of the form:

```

	&bus.Message{
		Name: []string{"campaign", "<campaign_id>", "user", "<user_id>", "directmessage"},
		Value: map[string]interface}{
			"timestamp": <millisecond_timestamp>,
			"from": {"username": "<username>", "id": <user_id>},
			"text": "<message_text>",
		}
	}

```
*/
package message

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"

	"gitlab.com/AntiMS/govtt/auth"
	"gitlab.com/AntiMS/govtt/bus"
)

var invalidRequest error = errors.New("Invalid request.")

func sendMessage(f func(path string, parsed any, u auth.User) ([]string, any, error)) func(w http.ResponseWriter, r *http.Request) {
	return auth.DecorateHandlerFunc(func(w http.ResponseWriter, r *http.Request, u auth.User) {
		if r.Method != "POST" {
			http.Error(w, "Please use POST.", http.StatusMethodNotAllowed)
			return
		}

		body, err := io.ReadAll(r.Body)
		if err != nil {
			http.Error(w, "Could not read request body. "+err.Error(), http.StatusInternalServerError)
			return
		}

		var parsed any
		contentType, ok := r.Header["Content-Type"]
		if ok {
			if len(contentType) > 1 {
				http.Error(w, "Multiple \"Content-Type\" headers provided.", http.StatusInternalServerError)
				return
			}
			if len(contentType) == 1 && contentType[0] == "application/json" {
				err = json.Unmarshal(body, &parsed)
				if err != nil {
					http.Error(w, "Failed to parse JSON request body. "+err.Error(), http.StatusInternalServerError)
					return
				}
			}
		}
		if parsed == nil {
			parsed = string(body)
		}

		name, obj, err := f(r.URL.Path, parsed, u)
		if err != nil {
			http.Error(w, "Error getting message object. "+err.Error(), http.StatusInternalServerError)
			return
		}

		bus.Publish(&bus.Message{Name: name, Value: obj})

		w.WriteHeader(http.StatusNoContent)
	})
}
