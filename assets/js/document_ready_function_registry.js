/**
 * Function registry to be called on document ready.
 *
 * Dependencies
 * ============
 * function_registry
 */
var document_ready_function_registry = function_registry.create();
