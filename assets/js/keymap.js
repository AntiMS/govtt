/**
 * Just a map of character (or semantic name) to key code.
 */
var keymap = (function() {
	var out = {},
		keys = {
			"ENTER": 13,
			"ESCAPE": 27,
			"SPACE": 32,
			"LEFT": 37,
			"UP": 38,
			"RIGHT": 39,
			"DOWN": 40,
			"+": 61,
			"a": 65,
			"b": 66,
			"c": 67,
			"d": 68,
			"e": 69,
			"f": 70,
			"g": 71,
			"h": 72,
			"i": 73,
			"j": 74,
			"k": 75,
			"l": 76,
			"m": 77,
			"n": 78,
			"o": 79,
			"p": 80,
			"q": 81,
			"r": 82,
			"s": 83,
			"t": 84,
			"u": 85,
			"v": 86,
			"w": 87,
			"x": 88,
			"y": 89,
			"z": 90,
			"-": 173,
			".": 190
		},
		reverse = {},
		k;

	for (k in keys) {
		reverse[keys[k]] = k;
	}

	out.keycode = function(char) {
		return keys[char];
	};

	out.char = function(keycode) {
		return reverse[keycode];
	};

	return out;
}());
