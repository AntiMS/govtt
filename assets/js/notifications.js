/**
 * Handles displaying notifications.
 *
 * Dependencies
 * ============
 * campaigns
 * channel
 * modal
 * server
 * hash_parse
 */
var notifications = (function() {
	var notifications_div,
		out = {};

	// From https://stackoverflow.com/questions/17415579/how-to-iso-8601-format-a-date-with-timezone-offset-in-javascript
	Date.prototype.toIsoString = function() {
		var tzo = -this.getTimezoneOffset(),
			dif = tzo >= 0 ? '+' : '-',
			pad = function(num) {
				var norm = Math.floor(Math.abs(num));
				return (norm < 10 ? '0' : '') + norm;
			};
		return this.getFullYear() +
			'-' + pad(this.getMonth() + 1) +
			'-' + pad(this.getDate()) +
			'T' + pad(this.getHours()) +
			':' + pad(this.getMinutes()) +
			':' + pad(this.getSeconds()) +
			dif + pad(tzo / 60) +
			':' + pad(tzo % 60);
	};

	out.direct_message = function(username, display_name) {
		modal.modal(function(elem) {
			var child;

			elem.classList.add("directmessage");

			child = document.createElement("label");
			child.for = "text";
			child.appendChild(document.createTextNode("Message to " + display_name));
			elem.appendChild(child);

			child = document.createElement("textarea");
			child.name = "text";
			child.id = "directmessage_text";
			elem.appendChild(child);

			child = document.createElement("button");
			child.appendChild(document.createTextNode("cancel"));
			child.addEventListener("click", function(e) {
				e.preventDefault();

				modal.hide();
			});
			elem.appendChild(child);

			child = document.createElement("button");
			child.appendChild(document.createTextNode("send"));
			child.addEventListener("click", function(e) {
				e.preventDefault();

				server.direct_message(document.getElementById("directmessage_text").value, campaigns.campaign.id, username, modal.hide);
			});
			elem.appendChild(child);
		});
	};

	function display(text, timestamp, buttons) {
		var child;

		if (buttons === undefined) {
			buttons = [];
		}

		if (notifications_div === undefined) {
			notifications_div = document.getElementById("notifications");
		}

		child = document.createElement("div");
		child.classList.add("notification");
		child.appendChild(document.createElement("div"));
		child.firstChild.classList.add("notification_wrapper");

		child.firstChild.appendChild(document.createElement("a"));
		child.firstChild.firstChild.classList.add("notification_dismiss");
		child.firstChild.firstChild.appendChild(document.createTextNode("X"));
		child.firstChild.firstChild.addEventListener("click", function(e) {
			this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
		});

		child.firstChild.appendChild(document.createTextNode(" "));

		child.firstChild.appendChild(document.createElement("a"));
		child.firstChild.lastChild.classList.add("notification_text");
		child.firstChild.lastChild.appendChild(document.createTextNode(text));
		child.firstChild.lastChild.addEventListener("click", function(e) {
			modal.modal(function(elem) {
				var i,
					child;

				elem.classList.add("notification");

				child = document.createElement("p");
				child.appendChild(document.createTextNode(new Date(timestamp).toIsoString()));
				elem.appendChild(child);

				child = document.createElement("pre");
				child.appendChild(document.createTextNode(text));
				elem.appendChild(child);

				child = document.createElement("button");
				child.appendChild(document.createTextNode("close"));
				child.addEventListener("click", modal.hide);
				elem.appendChild(child);

				for (i = 0 ; i < buttons.length ; i++) {
					child = document.createElement("button");
					child.appendChild(document.createTextNode(buttons[i].name));
					child.addEventListener("click", buttons[i].onclick);
					elem.appendChild(child);
				}
			});
		});

		notifications_div.appendChild(child);

		setTimeout(function() {
			if (child.parentNode !== null) {
				child.parentNode.removeChild(child);
			}
		}, 20000);
	}

	channel.register_handler("diceroll", function(name, data) {
		var text = data.display_name + " rolled " + data.expr + " = ";
		if (data.total.toString() !== data.expansion) {
			text += data.expansion + " = ";
		}
		display(text + data.total, data.timestamp);
	});

	if (hash_parse.get_parameter("no_dms") === undefined) {
		channel.register_handler("directmessage", function(name, data) {
			display(data.display_name + " sent you a direct message:\n\n" + data.text, data.timestamp, [
				{"name": "reply", "onclick": function(e) {
					e.preventDefault();

					out.direct_message(data.username, data.display_name);
				}}
			]);
		});
	}

	return out;
}());
