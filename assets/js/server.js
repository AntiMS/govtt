/**
 * All Ajax requests
 */
var server = (function() {
	var csrf_token = null;

	function call_server(method, path, body_obj, callback, error_callback) {
		var xhr = new XMLHttpRequest(),
			body;

		xhr.open(method, path, true);
		xhr.onload = function() {
			var msg,
				ct;

			if (xhr.readyState === 4) {
				ct = xhr.getResponseHeader("X-CSRF-Token");
				if (ct !== null && ct !== undefined) {
					csrf_token = ct;
				}

				if (xhr.status === 204) {
					if (callback !== null && callback !== undefined) {
						callback();
					}
				} else if (xhr.status === 200) {
					body = xhr.responseText;
					if (xhr.getResponseHeader("Content-Type") === "application/json") {
						body = JSON.parse(body);
					}
					if (callback !== null && callback !== undefined) {
						callback(body);
					}
				} else if (error_callback !== null && error_callback !== undefined) {
					error_callback(xhr.responseText);
				}
			}
		};
		xhr.onerror = function() {
			var ct;

			ct = xhr.getResponseHeader("X-CSRF-Token");
			if (ct !== null && ct !== undefined) {
				csrf_token = ct;
			}

			if (error_callback !== null && error_callback !== undefined) {
				error_callback(xhr.responseText);
			}
		};

		if (csrf_token !== null) {
			xhr.setRequestHeader("X-CSRF-Token", csrf_token);
		}

		if (body_obj === null || body_obj === undefined) {
			body = null;
		} else if (typeof body_obj === 'string' || body_obj instanceof String) {
			body = body_obj;
		} else {
			body = JSON.stringify(body_obj);
			xhr.setRequestHeader("Content-Type", "application/json");
		}
		xhr.send(body);
	}

	return {
		"login": function(username, password, callback, error_callback) {
			call_server("POST", "/login", {"username": username, "password": password}, callback, error_callback);
		},

		"login_by_signature": function(username, signature, callback, error_callback) {
			call_server("POST", "/loginBySignature", {"username": username, "signature": signature}, callback, error_callback);
		},

		"logout": function(callback, error_callback) {
			call_server("GET", "/logout", null, callback, error_callback);
		},

		"user": function(callback, error_callback) {
			call_server("GET", "/user", null, callback, error_callback);
		},

		"change_password": function(old_password, new_password, callback, error_callback) {
			call_server("POST", "/changePw", {"old_password": old_password, "new_password": new_password}, callback, error_callback);
		},

		"campaign_users": function(campaign_id, callback, error_callback) {
			call_server("GET", "/campaignUsers/" + campaign_id, null, callback, error_callback);
		},

		"campaigns": function(callback, error_callback) {
			call_server("GET", "/campaigns", null, callback, error_callback);
		},

		"change_display_name": function(campaign_id, new_display_name, callback, error_callback) {
			call_server("POST", "/campaign/changeDisplayName/" + campaign_id, new_display_name, callback, error_callback);
		},

		"scenes": function(campaign_id, callback, error_callback) {
			call_server("GET", "/scenes/" + campaign_id, null, callback, error_callback);
		},

		"new_scene": function(campaign_id, name, callback, error_callback) {
			call_server("POST", "/scenes/new/" + campaign_id, name, callback, error_callback);
		},

		"rename_scene": function(scene_id, name, callback, error_callback) {
			call_server("POST", "/scenes/rename/" + scene_id, name, callback, error_callback);
		},

		"set_scene": function(campaign_id, scene_id, callback, error_callback) {
			call_server("POST", "/scenes/set/" + campaign_id + "/" + scene_id, "", callback, error_callback);
		},

		"subscribe_scene": function(key, scene_id, callback, error_callback) {
			call_server("POST", "/channelScene/" + key + "/" + scene_id, "", callback, error_callback);
		},

		"pocket": function(campaign_id, callback, error_callback) {
			call_server("GET", "/pocket/" + campaign_id, null, callback, error_callback);
		},

		"fetch_image": function(campaign_id, url, name, callback, error_callback) {
			call_server("POST", "/images/fetch/" + campaign_id, {"url": url, "name": name}, callback, error_callback);
		},

		"rename_image": function(image_id, name, callback, error_callback) {
			call_server("POST", "/images/rename/" + image_id, name, callback, error_callback);
		},

		"archive_image": function(image_id, callback, error_callback) {
			call_server("POST", "/images/archive/" + image_id, null, callback, error_callback);
		},

		"unarchive_image": function(image_id, callback, error_callback) {
			call_server("POST", "/images/unarchive/" + image_id, null, callback, error_callback);
		},

		"save_object": function(obj, callback, error_callback) {
			call_server("POST", "/object/save", obj, callback, error_callback);
		},

		"new_table_object": function(obj, scene_id, callback, error_callback) {
			call_server("POST", "/object/newTable/" + scene_id, obj, callback, error_callback);
		},

		"new_pocket_object": function(obj, campaign_id, callback, error_callback) {
			call_server("POST", "/object/newPocket/" + campaign_id, obj, callback, error_callback);
		},

		"delete_object": function(obj_id, callback, error_callback) {
			call_server("POST", "/object/delete/" + obj_id, null, callback, error_callback);
		},

		"beacon": function(p, campaign_id, scene_id, callback, error_callback) {
			call_server("POST", "/beacon/" + campaign_id + "/" + scene_id, p, callback, error_callback);
		},

		"rolldice": function(expr, campaign_id, callback, error_callback) {
			call_server("POST", "/rolldice/" + campaign_id, expr, callback, error_callback);
		},

		"direct_message": function(text, campaign_id, username, callback, error_callback) {
			call_server("POST", "/directmessage/" + campaign_id + "/" + username, text, callback, error_callback);
		},

		"temporary_image_fix": function(campaign_id, callback, error_callback) {
			call_server("POST", "/temporaryImageFix/" + campaign_id, null, callback, error_callback);
		}
	};
})();
