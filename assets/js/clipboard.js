/**
 * Implements a "clipbard" for saving things cut/copied from the table for later pasting.
 *
 * Dependencies
 * ============
 * client_objects/clipboard_select
 * quick_menu
 * objects
 * table
 */
(function() {
	var clipboard = null; // When not null, a list of objects.

	function move_by(obj, offset) {
		var def = object_definitions.definition(obj);
		if (def.move_by !== undefined) {
			def.move_by(obj, offset);
		} else {
			if (obj.kind_data.x === undefined || obj.kind_data.y === undefined) {
				return false;
			}
			obj.kind_data.x += offset.x;
			obj.kind_data.y += offset.y;
		}
		return true;
	}

	(function() {
		var select = null;

		function get_op(f) {
			function op() {
				table.mouse_override({
					"cursor": "crosshair",
					"drag": function(p, dragstart) {
						if (select === null) {
							select = clipboard_select.create(dragstart);
						}
						select.update(p);
					},
					"drag_end": function() {
						var objs = [],
							bounds,
							offset,
							i,
							b,
							d;
						if (select === null) {
							console.log("Warning: clipboard_select drag_end called when not selecting.");
							table.clear_mouse_override();
							return;
						}
						bounds = select.bounds();
						select.del();
						select = null;
						table.clear_mouse_override();
						table.draw();
						clipboard = [];
						table_objects.iterate(function(obj) {
							var def = object_definitions.definition(obj),
								orig = table_objects.copy(obj),
								bb;
							if (def.client_only === true) {
								return;
							}
							if (def.subobject_within !== undefined) {
								obj = def.subobject_within(obj, bounds);
								if (obj === null || obj === undefined) {
									return;
								}
								// The following shouldn't be necessary unless an object's subobject_within(...) returns a different type than the original object.
								def = object_definitions.definition(obj);
								if (def.client_only === true) {
									return;
								}
							}
							bb = def.bounding_box(obj);
							if (bb.min.x >= bounds.min.x && bb.max.x <= bounds.max.x && bb.min.y >= bounds.min.y && bb.max.y <= bounds.max.y) {
								clipboard.push(obj);
								objs.push(orig);
							}
						});
						if (clipboard.length === 0) {
							clipboard = null;
							return;
						}
						if (f !== undefined) {
							f(objs, bounds);
						}
						bounds = {
							"min": {"x": Number.MAX_VALUE, "y": Number.MAX_VALUE},
							"max": {"x": -Number.MAX_VALUE, "y": -Number.MAX_VALUE}
						};
						for (i = 0 ; i < clipboard.length ; i++) {
							d = object_definitions.definition(clipboard[i]);
							b = d.bounding_box(clipboard[i]);
							if (b.min.x < bounds.min.x) {
								bounds.min.x = b.min.x;
							}
							if (b.min.y < bounds.min.y) {
								bounds.min.y = b.min.y;
							}
							if (b.max.x > bounds.max.x) {
								bounds.max.x = b.max.x;
							}
							if (b.max.y > bounds.max.y) {
								bounds.max.y = b.max.y;
							}
						}
						offset = { // Overflow issues if bounds.min/max.x/y are close to Number.MAX_VALUE?
							"x": -Math.round((bounds.min.x + bounds.max.x) / 2),
							"y": -Math.round((bounds.min.y + bounds.max.y) / 2)
						};
						for (i = 0 ; i < clipboard.length ; i++) {
							if (!move_by(clipboard[i], offset)) {
								console.log("Error: copied object missing coordinates");
								clipboard.splice(i, 1);
								i--; // Bit of a hack here.
								continue;
							}
							delete clipboard[i].id;
						}
						table.clear_mouse_override();
					}
				});
			}
			return op;
		}

		quick_menu.register("copy", keymap.keycode("c"), get_op());
		quick_menu.register("cut", keymap.keycode("x"), get_op(function(objs, rect) {
			function doit() {
				var obj,
					def;
				if (objs.length === 0) {
					return;
				}
				obj = objs[0];
				def = object_definitions.definition(obj);
				objs = objs.slice(1);
				if (def.delete_within !== undefined) {
					def.delete_within(obj, rect, doit);
				} else {
					table_objects.del(obj, doit);
				}
			}
			doit();
		}));
	}());

	(function() {
		function get_dopaste(ocb, oids, ooffset) {
			var cb = ocb.slice(),
				ids = oids === null ? null : oids.slice(),
				offset = {"x": ooffset.x, "y": ooffset.y};
			function dopaste() {
				var obj;
				if (cb.length === 0) {
					return;
				}
				obj = table_objects.copy(cb[0]);
				move_by(obj, offset);
				if (ids !== null) {
					table_objects.del_local(ids[0]);
				}
				table_objects.create(obj, function() {
					cb = cb.slice(1);
					if (ids !== null) {
						ids = ids.slice(1);
					}
					dopaste();
				});
			}
			return dopaste;
		}

		(function() {
			var ids = null,
				offset = null;

			quick_menu.register("paste", keymap.keycode("v"), function() {
				if (clipboard === null) {
					return false;
				}
				table.mouse_override({
					"cursor": "crosshair",
					"drag": function(p, dragstart) {
						var i,
							obj;
						if (ids === null) {
							ids = [];
							for (i = 0 ; i < clipboard.length ; i++) {
								obj = table_objects.copy(clipboard[i]);
								obj.id = table_objects.new_id();
								ids.push(obj.id);
								table_objects.save_local(obj);
							}
						}
						offset = {"x": Math.round(p.x), "y": Math.round(p.y)};
						for (i = 0 ; i < clipboard.length ; i++) {
							obj = table_objects.copy(clipboard[i]);
							obj.id = ids[i];
							move_by(obj, offset);
							table_objects.save_local(obj);
						}
						table.draw();
					},
					"drag_end": function() {
						get_dopaste(clipboard, ids, offset)();
						ids = null;
						offset = null;
						table.clear_mouse_override();
					},
					"click": function(p) {
						get_dopaste(clipboard, null, {"x": Math.round(p.x), "y": Math.round(p.y)})();
						ids = null;
						offset = null;
						table.clear_mouse_override();
					},
					"clear": function() {
						var i;
						if (ids === null && offset === null) {
							return;
						}
						for (i = 0 ; i < clipboard.length ; i++) {
							table_objects.del_local(ids[i]);
						}
						ids = null;
						offset = null;
					}
				});
			});
		}());
	}());
}());
