/**
 * Handles an SSE channel.
 */
var channel = (function() {
	var out = {},
		handlers = {};

	// Register a function to be called when a message of a particular name is received on the channel. The function is called with the name of the event and the data in the message.
	out.register_handler = function(name, func) {
		if (handlers[name] === undefined) {
			handlers[name] = [];
		}
		handlers[name].push(func);
	};

	// Creates a new channel connection for a given campaign. Callback is called after the connection is established. Also exposes the "channel key" -- a unique identifier of this channel -- on the property out.key.
	out.connect = function(campaign_id, callback) {
		var es = new EventSource("/channel/" + campaigns.campaign.id);

		out.register_handler("channelkey", function(name, data) {
			out.key = data;

			if (callback !== null && callback !== undefined) {
				callback();
			}
		});

		es.addEventListener("message", function(e) {
			var i,
				m = JSON.parse(e.data);

			console.log(m);

			if (handlers[m.name] !== undefined) {
				for (i = 0 ; i < handlers[m.name].length ; i++) {
					handlers[m.name][i](m.name, m.data);
				}
			}
		});
	};

	return out;
}());
