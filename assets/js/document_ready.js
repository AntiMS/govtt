/**
 * Initialization.
 *
 * Dependencies
 * ============
 * document_ready_function_registry
 */
(function () {
	/**
	 * Called when all resources (excepting those loaded dynamically via JavaScript) on the page have loaded.
	 */
	function document_ready() {
		document_ready_function_registry.call_functions();
	}

	/**
	 * The following block of code causes the function document_ready to trigger when all resources on the page have loaded (other than those loaded dynamically.)
	 */
	function onload_ael() {
		document.removeEventListener("DOMContentLoaded", onload_ael, false);
		document_ready();
	}
	function onload_st() {
		try {
			document.documentElement.doScroll("left");
		} catch (error) {
			setTimeout(onload_st, 10);
			return;
		}
		document_ready();
	}
	if (document.addEventListener) {
		document.addEventListener("DOMContentLoaded", onload_ael);
	} else if (document.attachEvent) {
		onload_st();
	}
}());
