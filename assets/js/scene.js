/**
 * Handles scenes.
 *
 * Dependencies
 * ============
 * campaigns
 * server
 */
var scene = (function() {
	var out = {};

	out.iterate = function(callback) {
		server.scenes(campaigns.campaign.id, function(list) {
			var i;

			for (i = 0 ; i < list.length ; i++) {
				callback(list[i]);
			}
		});
	};

	return out;
}());
