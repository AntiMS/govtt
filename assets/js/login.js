/**
 * Login infrastructure.
 *
 * Dependencies
 * ============
 * document_ready_function_registry
 * function_registry
 * server
 * hash_parse
 */
var login = (function() {
	var out = {
		"logged_in": function_registry.create(),
		"login_checked": function_registry.create(),
		"username": null
	};

	document_ready_function_registry.register(function() {
		var login_elem = document.getElementById("login"),
			username_elem = document.getElementById("login_username"),
			password_elem = document.getElementById("login_password"),
			button_elem = document.getElementById("login_login"),
			error_elem = document.getElementById("login_error");

		function submit(username, auth, server_func, presubmit_check) {
			var err_msg;

			button_elem.disabled = true;
			username_elem.disabled = true;
			password_elem.disabled = true;

			function clear_error() {
				while (error_elem.firstChild) {
					error_elem.removeChild(error_elem.firstChild);
				}
			}

			clear_error();
			error_elem.appendChild(document.createTextNode("\u00A0"));

			function reenable() {
				button_elem.disabled = false;
				username_elem.disabled = false;
				password_elem.disabled = false;
			}

			function error(msg) {
				if (msg === "" || msg === null || msg === undefined) {
					msg = "Unknown error.";
				}
				clear_error();
				error_elem.appendChild(document.createTextNode(msg));
			}

			err_msg = presubmit_check(username, auth);
			if (err_msg !== null) {
				reenable();
				error(err_msg);
				server.logout();
				return;
			}

			server_func(username, auth, function() {
				out.username = username_elem.value;
				login_elem.style.display = "none";
				out.logged_in.call_functions();
				out.login_checked.call_functions();
				reenable();
			}, function(msg) {
				error(msg);
				reenable();
			});
		}

		function signature_submit() {
			var signature = hash_parse.get_parameter("auth_signature"),
				un;

			if (signature === undefined) {
				return false;
			}

			signature = signature.split(":");
			un = signature[0];
			signature = signature.slice(1).join(":");

			submit(un, signature, server.login_by_signature, function(username, auth) {
				if (username === "" || auth === "") {
					return "Invalid auth signature value.";
				}
				return null;
			});

			return true;
		}

		function password_submit() {
			submit(username_elem.value, password_elem.value, server.login, function(username, auth) {
				if (username === "" || auth === "") {
					return "Please enter a username and password.";
				}
				return null;
			});
		}

		function enter_submit(event) {
			if (event.keyCode === 13) {
				event.preventDefault();
				password_submit();
			}
		}

		button_elem.addEventListener("click", password_submit);
		username_elem.addEventListener("keyup", enter_submit);
		password_elem.addEventListener("keyup", enter_submit);

		server.user(function(msg) {
			if (!signature_submit()) {
				document.getElementById("login").style.display = "none";
				out.username = msg;
				out.logged_in.call_functions();
			}
			out.login_checked.call_functions();
		}, function() {
			signature_submit();
			out.login_checked.call_functions();
		});
	});

	return out;
}());
