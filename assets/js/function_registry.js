/**
 * Function registries are lists of functions callable as a unit.
 */
var function_registry = (function () {
	return {
		/**
		 * Creates and returns a function registry.
		 */
		"create": function () {
			/**
			 * The list of functions.
			 */
			var funcs = [],
				async_funcs = [];

			return {
				/**
				 * Registers a function.
				 *
				 * Parameters
				 * ==========
				 * func
				 *     The function to register.
				 */
				"register": function (func) {
					if (func !== null && func !== undefined) {
						funcs.push(func);
					}
				},

				/**
				 * Registers an asynchronous function.
				 *
				 * Parameters
				 * ==========
				 * func
				 *     The function to register.
				 */
				"register_async": function (func) {
					if (func !== null && func !== undefined) {
						async_funcs.push(func);
					}
				},

				/**
				 * Unregisters a function.  Fails silently if the given argument is not found in the registered functions.
				 *
				 * Parameters
				 * ==========
				 * func
				 *     The function to unregister.
				 */
				"unregister": function (func) {
					var i;
					for (i = 0; i < funcs.length; i += 1) {
						if (funcs[i] === func) {
							funcs.splice(i, 1);
						}
					}
					for (i = 0; i < async_funcs.length; i += 1) {
						if (async_funcs[i] === func) {
							async_funcs.splice(i, 1);
						}
					}
				},

				/**
				 * Unregisters all callbacks.
				 */
				"empty": function () {
					funcs = [];
					async_funcs = [];
				},

				/**
				 * Returns true iff the given function is registered. False otherwise.
				 *
				 * Parameters
				 * ==========
				 * func
				 *     The function to unregister.
				 */
				"is_registered": function (func) {
					var i;
					for (i = 0; i < funcs.length; i += 1) {
						if (funcs[i] === func) {
							return true;
						}
					}
					for (i = 0; i < async_funcs.length; i += 1) {
						if (async_funcs[i] === func) {
							return true;
						}
					}
					return false;
				},

				/**
				 * Calls all of the functions in the registry, both synchronous and asynchronous.
				 *
				 * Parameters
				 * ==========
				 * callback
				 *     A function to call when all functions registered have been called.
				 * other arguments
				 *     All other arguments are passed to each of the functions.
				 */
				"call_functions": function (callback) {
					var args = Array.prototype.slice.call(arguments, 1),
						i;

					for (i = 0; i < funcs.length; i += 1) {
						funcs[i].apply(undefined, args);
					}

					if (async_funcs.length <= 0) {
						if (callback !== null && callback !== undefined) {
							callback();
						}
						return;
					}

					function cb() {
						i--;
						if (i <= 0 && callback !== null && callback !== undefined) {
							callback();
						}
					}

					function dc(i) {
						var j = i;
						setTimeout(function() {
							async_funcs[j].apply(null, [cb].concat(args));
						}, 0);
					}

					for (i = 0 ; i < async_funcs.length ; i++) {
						dc(i);
					}
				}
			};
		}
	};
}());
