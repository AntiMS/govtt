/**
 * Handles rectangle objects.
 *
 * Dependencies
 * ============
 * objects
 * table
 * modal
 */
(function() {
	/*
	Structure
	=========
	{
		"id": 1234,
		"name": "Rectangle",
		"kind": "rectangle",
		"kind_data": {
			"min": {"x": 1.72, "y": -0.15},
			"max": {"x": 3.91, "y": 0.95},
			"color": "#ffff00",
			"locked": false
		}
	}
	*/

	var definition = {},
		dragloc,
		constants;

	constants = {
		"margin_factor": 0.10,
		"clone_offset": 0.25
	};

	definition.name = "rectangle";

	definition.z = function(obj) {
		return 5;
	};

	definition.cursor = function(obj) {
		return obj.kind_data.locked ? "grab" : "pointer";
	};

	definition.draw = function(obj, ctx) {
		var min = table.to_canvas_coords(obj.kind_data.min),
			max = table.to_canvas_coords(obj.kind_data.max),
			width = max.x - min.x,
			height = max.y - min.y;

		ctx.fillStyle = obj.kind_data.color;
		ctx.fillRect(min.x, min.y, max.x - min.x, max.y - min.y);

		if (!obj.kind_data.locked) {
			ctx.fillStyle = "#ffffff";
			ctx.globalAlpha = 0.1;
			ctx.fillRect(min.x, min.y, width, height * constants.margin_factor);
			ctx.fillRect(min.x, max.y - height * constants.margin_factor, width, height * constants.margin_factor);
			ctx.fillRect(min.x, min.y, width * constants.margin_factor, height);
			ctx.fillRect(max.x - width * constants.margin_factor, min.y, width * constants.margin_factor, height);
			ctx.globalAlpha = 1;
		}
	};

	definition.in = function(obj, point) {
		return point.x >= obj.kind_data.min.x && point.x <= obj.kind_data.max.x && point.y >= obj.kind_data.min.y && point.y <= obj.kind_data.max.y;
	};

	definition.bounding_box = function(obj) {
		return {"min": obj.kind_data.min, "max": obj.kind_data.max};
	};

	definition.drag = function(obj, coords, start) {
		var min_size = 0.125,
			gc,
			gr,
			draglocs,
			bb,
			px,
			py,
			i;

		if (obj.kind_data.locked) {
			return false;
		}

		if (dragloc === undefined) {
			gc = function(c, m) {
				return function(obj, coords) {
					var f = m ? "max" : "min",
						b = m ? obj.kind_data.min[c] + min_size : obj.kind_data.max[c] - min_size;
					obj.kind_data[f][c] = Math[f](coords[c], b);
				};
			};
			gr = function(xm, ym) {
				var l = [];
				if (xm !== null) {
					l.push(gc("x", xm));
				}
				if (ym !== null) {
					l.push(gc("y", ym));
				}
				return function(obj, coords) {
					var i;
					for (i = 0 ; i < l.length ; i++) {
						l[i](obj, coords);
					}
				};
			};
			draglocs = [
				{"name": "tl", "predicate": function(x, y) { return x <= constants.margin_factor && y <= constants.margin_factor; }, "apply": gr(false, false)},
				{"name": "tr", "predicate": function(x, y) { return x >= 1 - constants.margin_factor && y <= constants.margin_factor; }, "apply": gr(true, false)},
				{"name": "br", "predicate": function(x, y) { return x >= 1 - constants.margin_factor && y >= 1 - constants.margin_factor; }, "apply": gr(true, true)},
				{"name": "bl", "predicate": function(x, y) { return x <= constants.margin_factor && y >= 1 - constants.margin_factor; }, "apply": gr(false, true)},
				{"name": "t", "predicate": function(x, y) { return y <= constants.margin_factor; }, "apply": gr(null, false)},
				{"name": "r", "predicate": function(x, y) { return x >= 1 - constants.margin_factor; }, "apply": gr(true, null)},
				{"name": "b", "predicate": function(x, y) { return y >= 1 - constants.margin_factor; }, "apply": gr(null, true)},
				{"name": "l", "predicate": function(x, y) { return x <= constants.margin_factor; }, "apply": gr(false, null)},
				{"name": "c", "predicate": function(x, y) { return true; }, "apply": function() {
					var dims = {"x": obj.kind_data.max.x - obj.kind_data.min.x, "y": obj.kind_data.max.y - obj.kind_data.min.y},
						offset = {"x": coords.x - obj.kind_data.min.x, "y": coords.y - obj.kind_data.min.y};
					return function(obj, coords) {
						obj.kind_data.min.x = coords.x - offset.x;
						obj.kind_data.min.y = coords.y - offset.y;
						obj.kind_data.max.x = obj.kind_data.min.x + dims.x;
						obj.kind_data.max.y = obj.kind_data.min.y + dims.y;
					};
				}()}
			];
			px = (coords.x - obj.kind_data.min.x) / (obj.kind_data.max.x - obj.kind_data.min.x);
			py = (coords.y - obj.kind_data.min.y) / (obj.kind_data.max.y - obj.kind_data.min.y);
			for (i = 0 ; i < draglocs.length ; i++) {
				if (draglocs[i].predicate(px, py)) {
					dragloc = draglocs[i];
					break;
				}
			}
		}

		dragloc.apply(obj, coords);
		table_objects.save_local(obj);
	};

	definition.drag_end = function(obj, coords) {
		dragloc = undefined;
		if (!obj.kind_data.locked) {
			table_objects.save(obj.id);
		}
	};

	definition.click = function(obj, point, callback) {
		modal.modal(function(elem) {
			var child,
				colorbox,
				colors = [
					"#888888",
					"#ff0000",
					"#ff8800",
					"#ffff00",
					"#00ff00",
					"#0000ff",
					"#ff00ff",
					"#964b00",
					"#444444",
					"#880000",
					"#884400",
					"#888800",
					"#008800",
					"#000088",
					"#880088",
					"#000000",
                                        "#080a12"
				];

			elem.classList.add("rectangle");

			child = document.createElement("label");
			child.for = "color";
			child.appendChild(document.createTextNode("Color"));
			elem.appendChild(child);

			colorbox = document.createElement("div");
			colorbox.classList.add("rectangle_modal_colorbox");
			elem.appendChild(colorbox);

			function change_handler() {
				if (this.checked) {
					obj.kind_data.color = this.value === "" ? null : this.value;
				}
				table_objects.save(obj);
			}

			for (i = 0 ; i < colors.length ; i++) {
				child = document.createElement("input");
				child.type = "radio";
				child.name = "color";
				child.checked = (obj.kind_data.color === colors[i]);
				child.style.backgroundColor = colors[i];
				child.value = colors[i];
				child.id = "rectangle_modal_color_" + i;
				child.classList.add("rectangle_modal_color");
				child.addEventListener("change", change_handler);
				colorbox.appendChild(child);
			}

			child = document.createElement("label");
			child.appendChild(document.createTextNode("Locked"));
			child.for = "locked";
			elem.appendChild(child);

			child = document.createElement("input");
			child.type = "checkbox";
			child.name = "locked";
			child.checked = obj.kind_data.locked;
			child.addEventListener("change", function(e) {
				e.preventDefault();

				obj.kind_data.locked = this.checked;
				table_objects.save(obj);
			});
			elem.appendChild(child);

			child = document.createElement("button");
			child.name = "rectangle_modal_exit";
			child.appendChild(document.createTextNode("exit"));
			child.addEventListener("click", function(e) {
				e.preventDefault();

				modal.hide();
			});
			elem.appendChild(child);

			child = document.createElement("button");
			child.appendChild(document.createTextNode("clone"));
			child.addEventListener("click", function() {
				var t = table_objects.copy(obj);

				delete t.id;
				t.kind_data.min.x += constants.clone_offset;
				t.kind_data.min.y += constants.clone_offset;
				t.kind_data.max.x += constants.clone_offset;
				t.kind_data.max.y += constants.clone_offset;
				t.kind_data.locked = false;

				modal.clear();
				table_objects.create(t, function() {
					table.draw();
					modal.hide();
				});
			});
			elem.appendChild(child);

			child = document.createElement("button");
			child.name = "rectangle_modal_delete";
			child.appendChild(document.createTextNode("delete"));
			child.addEventListener("click", function(e) {
				e.preventDefault();

				table_objects.del(obj, table.draw);
				modal.hide();
			});
			elem.appendChild(child);
		});
	};

	definition.move_by = function(obj, pt) {
		obj.kind_data.min.x += pt.x;
		obj.kind_data.min.y += pt.y;
		obj.kind_data.max.x += pt.x;
		obj.kind_data.max.y += pt.y;
	};

	object_definitions.register(definition);
}());
