/**
 * Handles image objects.
 *
 * Dependencies
 * ============
 * objects
 * table
 * images
 * modal
 */
(function() {
	/*
	Structure
	=========
	{
		"id": 1234,
		"name": "Image",
		"kind": "image",
		"kind_data": {
			"image_id": 21,
			"image_size": 5.22093023255814,
			"locked": true,
			"x": -1.6300246811821726,
			"y": 2.9803052420909677
		}
	}
	*/

	var tmploc,
		definition = {};

	definition.name = "image";

	definition.z = function(obj) {
		return 1;
	};

	definition.draw = function(obj, ctx) {
		var img_obj = images.get(obj.kind_data.image_id),
			img = img_obj.image,
			point = table.to_canvas_coords(obj.kind_data),
			scale = table.zoom * obj.kind_data.image_size,
			avgdim,
			width,
			height,
			i;

		if (img_obj.loaded) {
			avgdim = img_obj.loaded ? (img.width + img.height) / 2 : 1;
			width = scale * (img_obj.loaded ? img.width / avgdim : 1);
			height = scale * (img_obj.loaded ? img.height / avgdim : 1);
			ctx.drawImage(img, 0, 0, img.width, img.height, point.x - width / 2, point.y - height / 2, width, height);
		} else {
			ctx.fillStyle = "#222288";
			ctx.strokeStyle = "#666666";
			ctx.lineWidth = scale / 9;
			ctx.fillRect(point.x - scale / 2, point.y - scale / 2, scale, scale);
			for (i = 1 ; i < 9 ; i += 2) {
				ctx.beginPath();
				ctx.moveTo(point.x - scale / 2 + (i + 0.5) * scale / 9, point.y - scale / 2);
				ctx.lineTo(point.x - scale / 2 + (i + 0.5) * scale / 9, point.y + scale / 2);
				ctx.stroke();
			}
		}
	};

	definition.in = function(obj, point) {
		var img_obj = images.get(obj.kind_data.image_id),
			img = img_obj.image,
			avgdim = img_obj.loaded ? (img.width + img.height) / 2 : 1,
			width = obj.kind_data.image_size * (img_obj.loaded ? img.width / avgdim : 1),
			height = obj.kind_data.image_size * (img_obj.loaded ? img.height / avgdim : 1);

		return point.x >= obj.kind_data.x - width / 2 && point.x <= obj.kind_data.x + width / 2 && point.y >= obj.kind_data.y - height / 2 && point.y <= obj.kind_data.y + height / 2;
	};

	definition.bounding_box = function(obj) {
		var img_obj = images.get(obj.kind_data.image_id),
			img = img_obj.image,
			avgdim = img_obj.loaded ? (img.width + img.height) / 2 : 1,
			width = obj.kind_data.image_size * (img_obj.loaded ? img.width / avgdim : 1),
			height = obj.kind_data.image_size * (img_obj.loaded ? img.height / avgdim : 1);

		return {"min": {
			"x": obj.kind_data.x - width / 2,
			"y": obj.kind_data.y - height / 2
		}, "max": {
			"x": obj.kind_data.x + width / 2,
			"y": obj.kind_data.y + height / 2
		}};
	};

	definition.drag = function(obj, coords, start) {
		if (obj.kind_data.locked) {
			return false;
		}

		if (tmploc === undefined) {
			tmploc = {"x": obj.kind_data.x, "y": obj.kind_data.y};
		}

		obj.kind_data.x = tmploc.x + coords.x - start.x;
		obj.kind_data.y = tmploc.y + coords.y - start.y;
		table_objects.save_local(obj);
	};

	definition.drag_end = function(obj, coords) {
		tmploc = undefined;
		if (!obj.kind_data.locked) {
			table_objects.save(obj.id);
		}
	};

	definition.click = function(obj, point, callback) {
		modal.modal(function(elem) {
			var child;

			elem.classList.add("image");

			child = document.createElement("label");
			child.appendChild(document.createTextNode("Zoom"));
			elem.appendChild(child);

			child = document.createElement("button");
			child.name = "image_modal_zoom_in";
			child.appendChild(document.createTextNode("+"));
			child.addEventListener("click", function(e) {
				e.preventDefault();

				obj.kind_data.image_size *= 1.1;
				obj.kind_data.image_size = Math.max(Math.min(obj.kind_data.image_size, 1000), 0.25);
				table_objects.save(obj);
				table.draw();
			});
			elem.appendChild(child);

			child = document.createElement("button");
			child.name = "image_modal_zoom_out";
			child.appendChild(document.createTextNode("-"));
			child.addEventListener("click", function(e) {
				e.preventDefault();

				obj.kind_data.image_size /= 1.1;
				obj.kind_data.image_size = Math.max(Math.min(obj.kind_data.image_size, 1000), 0.25);
				table_objects.save(obj);
				table.draw();
			});
			elem.appendChild(child);

			child = document.createElement("label");
			child.appendChild(document.createTextNode("Locked"));
			child.for = "locked";
			elem.appendChild(child);

			child = document.createElement("input");
			child.type = "checkbox";
			child.name = "locked";
			child.checked = obj.kind_data.locked;
			child.addEventListener("change", function(e) {
				e.preventDefault();

				obj.kind_data.locked = this.checked;
				table_objects.save(obj);
			});
			elem.appendChild(child);

			child = document.createElement("button");
			child.name = "image_modal_exit";
			child.appendChild(document.createTextNode("exit"));
			child.addEventListener("click", function(e) {
				e.preventDefault();

				modal.hide();
			});
			elem.appendChild(child);

			child = document.createElement("button");
			child.name = "image_modal_delete";
			child.appendChild(document.createTextNode("delete"));
			child.addEventListener("click", function(e) {
				e.preventDefault();

				table_objects.del(obj, table.draw);
				modal.hide();
			});
			elem.appendChild(child);
		});
	};

	object_definitions.register(definition);
}());
