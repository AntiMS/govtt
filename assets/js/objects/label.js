/**
 * Handles label objects.
 *
 * Dependencies
 * ============
 * objects
 * table
 * modal
 */
// May not actually need this return value.
(function() {
	/*
	Structure
	=========
	{
		"id": 1234,
		"name": "Text",
		"kind": "text",
		"kind_data": {
			"background": "#ff4444",
			"foreground": "#000000",
			"size": 0.5,
			"text": "API BABY!",
			"x": 1.5400199926238898,
			"y": 2.8772782955173266
		}
	}
	*/

	var tmploc,
		constants,
		ctx_cache,
		definition = {};

	constants = {
		"box_size_ratio": 1.05,
		"font": "Arial",
		"clone_offset": 0.25,
		"min_size": 0.03125,
		"max_size": 4
	};

	function draw(obj, ctx, point) {
		var text_size = table.zoom * obj.kind_data.size,
			text_width,
			box_width,
			box_height;

		if (ctx_cache === undefined) {
			ctx_cache = ctx;
		}

		ctx.font = text_size + "px " + constants.font;
		text_width = ctx.measureText(obj.kind_data.text).width;
		box_width = text_width * constants.box_size_ratio;
		box_height = text_size * constants.box_size_ratio;

		ctx.fillStyle = obj.kind_data.background;
		ctx.fillRect(point.x - box_width / 2, point.y - box_height / 2, box_width, box_height);

		ctx.fillStyle = obj.kind_data.foreground;
		ctx.textAlign = "center";
		ctx.textBaseline = "middle";
		ctx.fillText(obj.kind_data.text, point.x, point.y);
	}

	definition.name = "label";

	definition.z = function(obj) {
		return 4;
	};

	definition.draw = function(obj, ctx) {
		draw(obj, ctx, table.to_canvas_coords(obj.kind_data));
	};

	definition.in = function(obj, point) {
		var text_size = table.zoom * obj.kind_data.size,
			text_width,
			width,
			height;

		if (ctx_cache === undefined) {
			return false;
		}

		ctx_cache.font = text_size + "px " + constants.font;
		text_width = ctx_cache.measureText(obj.kind_data.text).width;
		width = text_width * constants.box_size_ratio / table.zoom;
		height = text_size * constants.box_size_ratio / table.zoom;

		return point.x >= obj.kind_data.x - width / 2 && point.x <= obj.kind_data.x + width / 2 && point.y >= obj.kind_data.y - height / 2 && point.y <= obj.kind_data.y + height / 2;
	};

	definition.bounding_box = function(obj) {
		var text_size = table.zoom * obj.kind_data.size,
			text_width,
			width,
			height = text_size * constants.box_size_ratio / table.zoom;

		if (ctx_cache === undefined) {
			width = height * 5;
		} else {
			ctx_cache.font = text_size + "px " + constants.font;
			text_width = ctx_cache.measureText(obj.kind_data.text).width;
			width = text_width * constants.box_size_ratio / table.zoom;
		}

		return {"min": {
			"x": obj.kind_data.x - width / 2,
			"y": obj.kind_data.y - height / 2
		}, "max": {
			"x": obj.kind_data.x + width / 2,
			"y": obj.kind_data.y + height / 2
		}};
	};

	definition.drag = function(obj, coords, start) {
		if (tmploc === undefined) {
			tmploc = {"x": obj.kind_data.x, "y": obj.kind_data.y};
		}

		obj.kind_data.x = tmploc.x + coords.x - start.x;
		obj.kind_data.y = tmploc.y + coords.y - start.y;
		table_objects.save_local(obj);
	};

	definition.drag_end = function(obj, coords) {
		tmploc = undefined;
		table_objects.save(obj.id);
	};

	definition.click = function(obj, point, callback) {
		modal.modal(function(elem) {
			var child,
				colors,
				colorbox;

			elem.classList.add("label");

			child = document.createElement("label");
			child.for = "text";
			child.appendChild(document.createTextNode("Text"));
			elem.appendChild(child);

			child = document.createElement("input");
			child.type = "text";
			child.name = "text";
			child.value = obj.kind_data.text;
			child.id = "label_modal_text";
			child.addEventListener("change", function() {
				obj.kind_data.text = this.value;
				table_objects.save(obj);
				table.draw();
			});
			elem.appendChild(child);

			child = document.createElement("label");
			child.appendChild(document.createTextNode("Zoom"));
			elem.appendChild(child);

			child = document.createElement("button");
			child.name = "label_modal_zoom_in";
			child.appendChild(document.createTextNode("+"));
			child.addEventListener("click", function(e) {
				e.preventDefault();

				obj.kind_data.size *= 1.1;
				obj.kind_data.size = Math.max(Math.min(obj.kind_data.size, constants.max_size), constants.min_size);
				table_objects.save(obj);
				table.draw();
			});
			elem.appendChild(child);

			child = document.createElement("button");
			child.name = "label_modal_zoom_out";
			child.appendChild(document.createTextNode("-"));
			child.addEventListener("click", function(e) {
				e.preventDefault();

				obj.kind_data.size /= 1.1;
				obj.kind_data.size = Math.max(Math.min(obj.kind_data.size, constants.max_size), constants.min_size);
				table_objects.save(obj);
				table.draw();
			});
			elem.appendChild(child);

			colors = [
				"#888888",
				"#ff0000",
				"#ff8800",
				"#ffff00",
				"#00ff00",
				"#0000ff",
				"#ff00ff",
				"#964b00",
				"#444444",
				"#880000",
				"#884400",
				"#888800",
				"#008800",
				"#000088",
				"#880088",
				"#000000"
			];

			child = document.createElement("label");
			child.for = "foreground";
			child.appendChild(document.createTextNode("Foreground"));
			elem.appendChild(child);

			colorbox = document.createElement("div");
			colorbox.classList.add("label_modal_foreground_box");
			elem.appendChild(colorbox);

			function fg_change_handler() {
				if (this.checked) {
					obj.kind_data.foreground = this.value;
				}

				table_objects.save(obj);
				table.draw();
			}

			for (i = 0 ; i < colors.length ; i++) {
				child = document.createElement("input");
				child.type = "radio";
				child.name = "foreground";
				child.checked = (obj.kind_data.foreground === colors[i]);
				child.style.backgroundColor = colors[i];
				child.value = colors[i];
				child.id = "label_modal_foreground_color_" + i;
				child.classList.add("label_modal_foreground_color");
				child.addEventListener("change", fg_change_handler);
				colorbox.appendChild(child);
			}

			colors = [
				"#cccccc",
				"#ff4444",
				"#ffcc44",
				"#ffff44",
				"#44ff44",
				"#4444ff",
				"#ff44ff",
				"#ba8f44",
				"#ffffff",
				"#cc4444",
				"#cc8844",
				"#cccc44",
				"#44cc44",
				"#4444cc",
				"#cc44cc",
				"#666666"
			];

			child = document.createElement("label");
			child.for = "background";
			child.appendChild(document.createTextNode("Background"));
			elem.appendChild(child);

			colorbox = document.createElement("div");
			colorbox.classList.add("label_modal_background_box");
			elem.appendChild(colorbox);

			function bg_change_handler() {
				if (this.checked) {
					obj.kind_data.background = this.value;
				}

				table_objects.save(obj);
				table.draw();
			}

			for (i = 0 ; i < colors.length ; i++) {
				child = document.createElement("input");
				child.type = "radio";
				child.name = "background";
				child.checked = (obj.kind_data.background === colors[i]);
				child.style.backgroundColor = colors[i];
				child.value = colors[i];
				child.id = "label_modal_background_color_" + i;
				child.classList.add("label_modal_background_color");
				child.addEventListener("change", bg_change_handler);
				colorbox.appendChild(child);
			}

			child = document.createElement("button");
			child.name = "label_modal_exit";
			child.appendChild(document.createTextNode("exit"));
			child.addEventListener("click", function(e) {
				e.preventDefault();

				modal.hide();
			});
			elem.appendChild(child);

			child = document.createElement("button");
			child.appendChild(document.createTextNode("clone"));
			child.addEventListener("click", function() {
				var t = table_objects.copy(obj);

				delete t.id;
				t.kind_data.x += constants.clone_offset;
				t.kind_data.y += constants.clone_offset;

				modal.clear();
				table_objects.create(t, function() {
					table.draw();
					modal.hide();
				});
			});
			elem.appendChild(child);

			child = document.createElement("button");
			child.name = "label_modal_delete";
			child.appendChild(document.createTextNode("delete"));
			child.addEventListener("click", function(e) {
				e.preventDefault();

				table_objects.del(obj, table.draw);
				modal.hide();
			});
			elem.appendChild(child);
		});
	};

	object_definitions.register(definition);
}());
