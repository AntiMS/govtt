/**
 * Handles token objects.
 *
 * Dependencies
 * ============
 * objects
 * table
 * images
 * modal
 */
var token = (function() {
	/*
	Structure
	=========
	{
		"id": 1234,
		"name": "Token",
		"kind": "token",
		"kind_data": {
			"color": null,
			"height": 4,
			"image_id": 3,
			"image_size": 5.160493827160494,
			"image_x": 0.51,
			"image_y": 0.6519138755980861,
			"round": true,
			"width": 3,
			"x": -6.1,
			"y": 3.7375
		}
	}
	*/

	var definition = {},
		out = {},
		constants;

	constants = {
		"border_ratio": 0.85
	};

	/*
	Draws this token on the given canvas. Zoom is the size of a single unit in pixels. Point is the canvas point on which to center the token in the form `{"x": <number>, "y": <number>}`.
	*/
	function draw(obj, ctx, zoom, point) {
		var width = obj.kind_data.width * zoom,
			height = obj.kind_data.height * zoom,
			max_dim = Math.max(obj.kind_data.width, obj.kind_data.height),
			img_obj = images.get(obj.kind_data.image_id),
			img,
			bounding_dim,
			image_width,
			image_height,
			image_x,
			image_y,
			i;

		function clip() {
			ctx.beginPath();
			if (obj.kind_data.round) {
				ctx.ellipse(point.x, point.y, width / 2, height / 2, 0, 0, Math.PI * 2);
			} else {
				ctx.rect(point.x - width / 2, point.y - height / 2, width, height);
			}
			ctx.closePath();
			ctx.clip();
		}

		ctx.save();

		clip();

		if (obj.kind_data.color !== null) {
			ctx.fillStyle = obj.kind_data.color;
			ctx.fillRect(point.x - width, point.y - height, width * 2, height * 2);

			width *= constants.border_ratio;
			height *= constants.border_ratio;

			clip();
		}

		if (img_obj.loaded) {
			img = img_obj.image;
			bounding_dim = img.width / img.height > obj.kind_data.width / obj.kind_data.height ? img.height : img.width;
			image_width = obj.kind_data.width * bounding_dim / (obj.kind_data.image_size * max_dim);
			image_height = obj.kind_data.height * bounding_dim / (obj.kind_data.image_size * max_dim);
			image_x = obj.kind_data.image_x * img.width;
			image_y = obj.kind_data.image_y * img.height;

			ctx.drawImage(img, image_x - image_width / 2, image_y - image_height / 2, image_width, image_height, point.x - width / 2, point.y - height / 2, width, height);
		} else {
			ctx.fillStyle = "#222288";
			ctx.strokeStyle = "#666666";
			ctx.lineWidth = width / 9;
			ctx.fillRect(point.x - width / 2, point.y - height / 2, width, height);
			for (i = 1 ; i < 9 ; i += 2) {
				ctx.beginPath();
				ctx.moveTo(point.x - width / 2 + (i + 0.5) * width / 9, point.y - height / 2);
				ctx.lineTo(point.x - width / 2 + (i + 0.5) * width / 9, point.y + height / 2);
				ctx.stroke();
			}
		}

		ctx.restore();
	}

	definition.name = "token";

	definition.z = function(obj) {
		return 2;
	};

	definition.draw = function(obj, ctx) {
		draw(obj, ctx, table.zoom, table.to_canvas_coords(obj.kind_data));
	};

	definition.in = function(obj, point) {
		if (obj.kind_data.round) {
			return ((point.x - obj.kind_data.x) / obj.kind_data.width) ** 2 + ((point.y - obj.kind_data.y) / obj.kind_data.height) ** 2 <= 0.25;
		}
		return point.x >= obj.kind_data.x - obj.kind_data.width / 2 && point.x <= obj.kind_data.x + obj.kind_data.width / 2 && point.y >= obj.kind_data.y - obj.kind_data.height / 2 && point.y <= obj.kind_data.y + obj.kind_data.height / 2;
	};

	definition.bounding_box = function(obj) {
		return {"min": {
			"x": obj.kind_data.x - obj.kind_data.width / 2,
			"y": obj.kind_data.y - obj.kind_data.height / 2
		}, "max": {
			"x": obj.kind_data.x + obj.kind_data.width / 2,
			"y": obj.kind_data.y + obj.kind_data.height / 2
		}};
	};

	definition.drag = function(obj, coords) {
		obj.kind_data.x = coords.x;
		obj.kind_data.y = coords.y;
		table_objects.save_local(obj);
	};

	definition.drag_end = function(obj, coords) {
		table_objects.save(obj.id);
	};

	definition.click = function(obj, point, callback) {
		out.edit_modal(obj, callback, {"x": obj.kind_data.x + 0.25, "y": obj.kind_data.y + 0.25}, table_objects.save, table_objects.del);
	};

	object_definitions.register(definition);

	out.edit_modal = function(obj, callback, clone_loc, save_func, del_func) {
		modal.modal(function(elem, onshow) {
			var canvas,
				child,
				ctx,
				i,
				colors,
				colorbox,
				mouseloc = null,
				imgloc = null;

			function get_zoom() {
				return Math.min(
					canvas.clientWidth * 0.95 / obj.kind_data.width,
					canvas.clientHeight * 0.95 / obj.kind_data.height
				);
			}

			function draw_preview() {
				canvas.width = canvas.clientWidth;
				canvas.height = canvas.clientHeight;

				draw(obj, ctx, get_zoom(), {"x": canvas.clientWidth / 2, "y": canvas.clientHeight / 2});
			}

			function mouse_coords(e) {
				var p = {};

				if (e.pageX && e.pageY) {
					p.x = e.pageX;
					p.y = e.pageY;
				} else {
					p.x = e.clientX;
					p.y = e.clientY;
				}

				return p;
			}

			function fix_image() {
				var img_obj = images.get(obj.kind_data.image_id),
					img = img_obj.image,
					bounding_dim = img.width / img.height > obj.kind_data.width / obj.kind_data.height ? img.height : img.width,
					token_max_dim = Math.max(obj.kind_data.width, obj.kind_data.height),
					min_size = 1,
					max_size = 15, // Pretty arbitrary.
					min_x,
					min_y;

				obj.kind_data.image_size = Math.max(min_size, Math.min(max_size, obj.kind_data.image_size));

				min_x = obj.kind_data.width * bounding_dim / (2 * img.width * token_max_dim * obj.kind_data.image_size);
				min_y = obj.kind_data.height * bounding_dim / (2 * img.height * token_max_dim * obj.kind_data.image_size);

				obj.kind_data.image_x = Math.max(min_x, Math.min(1 - min_x, obj.kind_data.image_x));
				obj.kind_data.image_y = Math.max(min_y, Math.min(1 - min_y, obj.kind_data.image_y));
			}

			elem.classList.add("token_modal");

			canvas = document.createElement("canvas");
			canvas.classList.add("token_modal_canvas");
			canvas.addEventListener("wheel", function(e) {
				var exp = (e.deltaY < 0 ? 1 : -1);

				e.preventDefault();

				if (!images.get(obj.kind_data.image_id).loaded) {
					return;
				}

				obj.kind_data.image_size *= 1.3 ** exp;

				fix_image();

				draw_preview();
			});
			canvas.addEventListener("mousedown", function(e) {
				if (!images.get(obj.kind_data.image_id).loaded) {
					return;
				}

				mouseloc = mouse_coords(e);
				imgloc = {"x": obj.kind_data.image_x, "y": obj.kind_data.image_y};
			});
			canvas.addEventListener("mousemove", function(e) {
				var zoom = get_zoom(),
					mc,
					mult = 1,
					token_max_dim = Math.max(obj.kind_data.width, obj.kind_data.height),
					img_obj = images.get(obj.kind_data.image_id),
					img,
					bounding_dim;

				if (!img_obj.loaded) {
					return;
				}

				img = img_obj.image;
				bounding_dim = img.width / img.height > obj.kind_data.width / obj.kind_data.height ? img.height : img.width;

				if (mouseloc === null) {
					return;
				}

				if (obj.kind_data.color !== null) {
					mult = constants.border_ratio;
				}

				mc = mouse_coords(e);

				obj.kind_data.image_x = imgloc.x - (mc.x - mouseloc.x) * bounding_dim / (token_max_dim * zoom * mult * obj.kind_data.image_size * img.width);
				obj.kind_data.image_y = imgloc.y - (mc.y - mouseloc.y) * bounding_dim / (token_max_dim * zoom * mult * obj.kind_data.image_size * img.height);

				fix_image();

				draw_preview();
			});
			canvas.addEventListener("mouseup", function(e) {
				mouseloc = null;
				imgloc = null;
			});
			canvas.addEventListener("mouseout", function(e) {
				mouseloc = null;
				imgloc = null;
			});
			elem.appendChild(canvas);

			child = document.createElement("div");
			child.classList.add("token_modal_form");
			elem.appendChild(child);

			child = document.createElement("label");
			child.for = "name";
			child.appendChild(document.createTextNode("Name"));
			elem.lastChild.appendChild(child);

			child = document.createElement("input");
			child.type = "text";
			child.name = "name";
			child.value = obj.name;
			child.id = "token_modal_name";
			child.addEventListener("change", function() {
				obj.name = this.value;
			});
			elem.lastChild.appendChild(child);

			child = document.createElement("label");
			child.for = "round";
			child.appendChild(document.createTextNode("Round"));
			elem.lastChild.appendChild(child);

			child = document.createElement("input");
			child.type = "checkbox";
			child.name = "round";
			child.checked = obj.kind_data.round;
			child.id = "token_modal_round";
			child.addEventListener("change", function() {
				obj.kind_data.round = this.checked;
				draw_preview();
			});
			elem.lastChild.appendChild(child);

			child = document.createElement("label");
			child.for = "color";
			child.appendChild(document.createTextNode("Color"));
			elem.lastChild.appendChild(child);

			colors = [
				"#888888",
				"#ff0000",
				"#ff8800",
				"#ffff00",
				"#00ff00",
				"#0000ff",
				"#ff00ff",
				"#964b00",
				"#444444",
				"#880000",
				"#884400",
				"#888800",
				"#008800",
				"#000088",
				"#880088",
				"#000000",
				null
			];

			colorbox = document.createElement("div");
			colorbox.classList.add("token_modal_colorbox");
			elem.lastChild.appendChild(colorbox);

			function change_handler() {
				if (this.checked) {
					obj.kind_data.color = this.value === "" ? null : this.value;
				}
				draw_preview();
			}

			for (i = 0 ; i < colors.length ; i++) {
				child = document.createElement("input");
				child.type = "radio";
				child.name = "color";
				child.checked = (obj.kind_data.color === colors[i]);
				if (colors[i] === null) {
					child.classList.add("token_modal_color_transparent");
					child.value = "";
				} else {
					child.style.backgroundColor = colors[i];
					child.value = colors[i];
				}
				child.id = "token_modal_color_" + i;
				child.classList.add("token_modal_color");
				child.addEventListener("change", change_handler);
				colorbox.appendChild(child);
			}

			child = document.createElement("label");
			child.appendChild(document.createTextNode("Width/Height"));
			elem.lastChild.appendChild(child);

			child = document.createElement("input");
			child.type = "number";
			child.min = 1;
			child.max = 10;
			child.name = "width";
			child.value = obj.kind_data.width;
			child.id = "token_modal_width";
			child.addEventListener("change", function() {
				obj.kind_data.width = parseInt(this.value);
				fix_image();
				draw_preview();
			});
			elem.lastChild.appendChild(child);

			child = document.createElement("input");
			child.type = "number";
			child.min = 1;
			child.max = 10;
			child.name = "height";
			child.value = obj.kind_data.height;
			child.id = "token_modal_height";
			child.addEventListener("change", function() {
				obj.kind_data.height = parseInt(this.value);
				fix_image();
				draw_preview();
			});
			elem.lastChild.appendChild(child);

			child = document.createElement("button");
			child.appendChild(document.createTextNode("cancel"));
			child.addEventListener("click", function() {
				modal.hide();
				callback();
			});
			elem.lastChild.appendChild(child);

			if (del_func !== undefined) {
				child = document.createElement("button");
				child.appendChild(document.createTextNode("delete"));
				child.addEventListener("click", function() {
					modal.clear();
					del_func(obj.id, function() {
						modal.hide();
						callback();
					});
				});
				elem.lastChild.appendChild(child);
			}

			if (save_func !== undefined) {
				child = document.createElement("button");
				child.appendChild(document.createTextNode("save"));
				child.addEventListener("click", function() {
					save_func(obj);
					modal.hide();
					callback();
				});
				elem.lastChild.appendChild(child);
			}

			child = document.createElement("button");
			child.appendChild(document.createTextNode("clone to pocket"));
			child.addEventListener("click", function() {
				modal.clear();
				pocket_objects.create(obj, function() {
					modal.hide();
					callback();
				});
			});
			elem.lastChild.appendChild(child);

			child = document.createElement("button");
			child.appendChild(document.createTextNode("clone to table"));
			child.addEventListener("click", function() {
				modal.clear();
				obj.kind_data.x = clone_loc.x;
				obj.kind_data.y = clone_loc.y;
				table_objects.create(obj, function() {
					modal.hide();
					callback();
				});
			});
			elem.lastChild.appendChild(child);

			onshow(function() {
				ctx = canvas.getContext("2d");
				draw_preview();
			});
		});
	};

	return out;
}());
