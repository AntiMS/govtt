/**
 * Handles drawing objects.
 *
 * Dependencies
 * ============
 * objects
 * table
 * modal
 */
(function() {
	/*
	Structure
	=========
	{
		"id": 1234,
		"name": "Drawing",
		"kind": "drawing",
		"kind_data": {
			"color": "#ffff00",
			"points": [
				{"x": 0.143207506396124, "y": -5.604535345832319},
				{"x": 0.5598741730627883, "y": -7.618424234721208},
				{"x": 2.6084852841739017, "y": -5.135785345832319}
			]
		}
	}
	*/

	var definition = {},
		newpoint = false,
		dragobj = null,
		constants;

	constants = {
		"point_radius": 0.15,
		"line_width": 0.1,
		"mouse_dist": 0.15,
		"clone_offset": 0.25
	};

	definition.name = "drawing";

	definition.z = function(obj) {
		return 3;
	};

	definition.draw = function(obj, ctx) {
		var tmppoint = table.to_canvas_coords(obj.kind_data.points[0]),
			i;

		if (obj.kind_data.points.length === 1) {
			ctx.fillStyle = obj.kind_data.color;
			ctx.beginPath();
			ctx.arc(tmppoint.x, tmppoint.y, table.zoom * constants.point_radius, 0, 2 * Math.PI);
			ctx.closePath();
			ctx.fill();
			return;
		}

		ctx.lineWidth = table.zoom * constants.line_width;
		ctx.lineCap = "round";
		ctx.lineJoin = "round";
		ctx.strokeStyle = obj.kind_data.color;
		ctx.beginPath();
		ctx.moveTo(tmppoint.x, tmppoint.y);
		for (i = 0 ; i < obj.kind_data.points.length ; i++) {
			tmppoint = table.to_canvas_coords(obj.kind_data.points[i]);
			ctx.lineTo(tmppoint.x, tmppoint.y);
		}
		ctx.stroke();
	};

	definition.in = function(obj, point) {
		var out = false,
			p1,
			p2,
			seg,
			seglen,
			t,
			projlen,
			proj,
			i;

		for (i = 0 ; i < obj.kind_data.points.length ; i++) {
			p1 = obj.kind_data.points[i];
			if ((point.x - p1.x) ** 2 + (point.y - p1.y) ** 2 < constants.point_radius ** 2) {
				return true;
			}
		}

		for (i = 1 ; i < obj.kind_data.points.length ; i++) {
			p1 = obj.kind_data.points[i-1];
			p2 = obj.kind_data.points[i];
			if (p1.x === p2.x && p1.y === p2.y) {
				continue;
			}

			seg = {"x": p2.x - p1.x, "y": p2.y - p1.y};
			seglen = Math.sqrt(seg.x**2 + seg.y**2);
			t = {"x": point.x- p1.x, "y": point.y - p1.y};
			projlen = (seg.x*t.x + seg.y*t.y) / seglen;
			if (projlen > seglen || projlen < 0) {
				continue;
			}
			proj = {"x": projlen * seg.x / seglen, "y": projlen * seg.y / seglen};
			if ((t.x - proj.x) ** 2 + (t.y - proj.y) ** 2 < constants.mouse_dist ** 2) {
				return true;
			}
		}

		return false;
	};

	definition.bounding_box = function(obj) {
		var x1 = Number.MAX_VALUE,
			y1 = Number.MAX_VALUE,
			x2 = -Number.MAX_VALUE,
			y2 = -Number.MAX_VALUE,
			p;

		if (obj.kind_data.points.length === 1) {
			x1 = obj.kind_data.points[0].x - constants.point_radius;
			y1 = obj.kind_data.points[0].y - constants.point_radius;
			x2 = obj.kind_data.points[0].x + constants.point_radius;
			y2 = obj.kind_data.points[0].y + constants.point_radius;
		} else {
			for (i = 0 ; i < obj.kind_data.points.length ; i++) {
				p = obj.kind_data.points[i];
				x1 = Math.min(x1, p.x - constants.line_width / 2);
				y1 = Math.min(y1, p.y - constants.line_width / 2);
				x2 = Math.max(x2, p.x + constants.line_width / 2);
				y2 = Math.max(y2, p.y + constants.line_width / 2);
			}
		}

		return {"min": {"x": x1, "y": y1}, "max": {"x": x2, "y": y2}};
	};

	definition.drag = function(obj, coords, start) {
		var p0,
			pn = obj.kind_data.points[obj.kind_data.points.length - 1],
			i;

		if (!newpoint && dragobj === null) {
			p0 = obj.kind_data.points[0];
			if ((start.x - p0.x) ** 2 + (start.y - p0.y) ** 2 < constants.mouse_dist ** 2) {
				obj.kind_data.points.reverse();
			} else if ((start.x - pn.x) ** 2 + (start.y - pn.y) ** 2 >= constants.mouse_dist ** 2) {
				dragobj = table_objects.copy(obj);
			}
			if (dragobj === null) {
				pn = {"x": coords.x, "y": coords.y};
				obj.kind_data.points.push(pn);
				newpoint = true;
			}
		}

		if (newpoint) {
			pn.x = coords.x;
			pn.y = coords.y;
		} else if (dragobj !== null) {
			for (i = 0 ; i < obj.kind_data.points.length ; i++) {
				obj.kind_data.points[i].x = dragobj.kind_data.points[i].x + coords.x - start.x;
				obj.kind_data.points[i].y = dragobj.kind_data.points[i].y + coords.y - start.y;
			}
		}

		table_objects.save_local(obj);
	};

	definition.drag_end = function(obj, coords) {
		if (dragobj !== null || newpoint) {
			table_objects.save(obj.id);
		}
		newpoint = false;
		dragobj = null;
	};

	definition.click = function(obj, point, callback) {
		function transform(f) {
			var cx,
				cy,
				minx = 1 << 30,
				miny = 1 << 30,
				maxx = - (1 << 30),
				maxy = - (1 << 30),
				points = obj.kind_data.points,
				i;

			for (i = 0 ; i < points.length ; i++) {
				if (points[i].x < minx) {
					minx = points[i].x;
				}
				if (points[i].y < miny) {
					miny = points[i].y;
				}
				if (points[i].x > maxx) {
					maxx = points[i].x;
				}
				if (points[i].y > maxy) {
					maxy = points[i].y;
				}
			}

			cx = (minx + maxx) / 2;
			cy = (miny + maxy) / 2;

			for (i = 0 ; i < points.length ; i++) {
				points[i].x -= cx;
				points[i].y -= cy;
				f(points[i]);
				points[i].x += cx;
				points[i].y += cy;
			}
		}

		modal.modal(function(elem) {
			var child,
				colorbox,
				colors = [
					"#888888",
					"#ff0000",
					"#ff8800",
					"#ffff00",
					"#00ff00",
					"#0000ff",
					"#ff00ff",
					"#964b00",
					"#444444",
					"#880000",
					"#884400",
					"#888800",
					"#008800",
					"#000088",
					"#880088",
					"#000000"
				];

			elem.classList.add("drawing");

			child = document.createElement("label");
			child.for = "color";
			child.appendChild(document.createTextNode("Color"));
			elem.appendChild(child);

			colorbox = document.createElement("div");
			colorbox.classList.add("drawing_modal_colorbox");
			elem.appendChild(colorbox);

			function change_handler() {
				if (this.checked) {
					obj.kind_data.color = this.value === "" ? null : this.value;
				}
			}

			for (i = 0 ; i < colors.length ; i++) {
				child = document.createElement("input");
				child.type = "radio";
				child.name = "color";
				child.checked = (obj.kind_data.color === colors[i]);
				child.style.backgroundColor = colors[i];
				child.value = colors[i];
				child.id = "drawing_modal_color_" + i;
				child.classList.add("drawing_modal_color");
				child.addEventListener("change", change_handler);
				colorbox.appendChild(child);
			}

			child = document.createElement("label");
			child.for = "color";
			child.appendChild(document.createTextNode("Transform"));
			elem.appendChild(child);

			child = document.createElement("div");
			child.classList.add("drawing_modal_transforms");
			elem.appendChild(child);

			(function(l) {
				var i;

				function click_handler() {
					transform(l[parseInt(this.dataset.i)].func);
				}

				for (i = 0 ; i < l.length ; i++) {
					child = document.createElement("button");
					child.appendChild(document.createTextNode(l[i].name));
					child.dataset.i = i;
					child.addEventListener("click", click_handler);
					elem.lastChild.appendChild(child);
				}
			}([
				{"name": "flip h", "func": function(p) {
					p.x = -p.x;
				}},
				{"name": "flip v", "func": function(p) {
					p.y = -p.y;
				}},
				{"name": "rot r", "func": function(p) {
					var t;
					t = p.y;
					p.y = p.x;
					p.x = -t;
				}},
				{"name": "rot l", "func": function(p) {
					var t;
					t = p.y;
					p.y = -p.x;
					p.x = t;
				}},
				{"name": "shrink", "func": function(p) {
					p.x /= 1.2;
					p.y /= 1.2;
				}},
				{"name": "grow", "func": function(p) {
					p.x *= 1.2;
					p.y *= 1.2;
				}},
			]));

			child = document.createElement("div");
			child.classList.add("drawing_modal_buttons");
			elem.appendChild(child);

			child = document.createElement("button");
			child.appendChild(document.createTextNode("cancel"));
			child.addEventListener("click", function() {
				modal.hide();
			});
			elem.lastChild.appendChild(child);

			child = document.createElement("button");
			child.appendChild(document.createTextNode("delete"));
			child.addEventListener("click", function() {
				modal.clear();
				table_objects.del(obj.id, function() {
					table.draw();
					modal.hide();
				});
			});
			elem.lastChild.appendChild(child);

			child = document.createElement("button");
			child.appendChild(document.createTextNode("clone"));
			child.addEventListener("click", function() {
				var t = table_objects.copy(obj),
					i;

				delete t.id;
				for (i = 0 ; i < t.kind_data.points.length ; i++) {
					t.kind_data.points[i].x += constants.clone_offset;
					t.kind_data.points[i].y += constants.clone_offset;
				}

				modal.clear();
				table_objects.create(t, function() {
					table.draw();
					modal.hide();
				});
			});
			elem.lastChild.appendChild(child);

			child = document.createElement("button");
			child.appendChild(document.createTextNode("save"));
			child.addEventListener("click", function() {
				table_objects.save(obj);
				table.draw();
				modal.hide();
			});
			elem.lastChild.appendChild(child);
		});
	};

	definition.move_by = function(obj, pt) {
		var i;

		for (i = 0 ; i < obj.kind_data.points.length ; i++) {
			obj.kind_data.points[i].x += pt.x;
			obj.kind_data.points[i].y += pt.y;
		}
	};

	object_definitions.register(definition);
}());
