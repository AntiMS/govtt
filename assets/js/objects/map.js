/**
 * Handles maps
 *
 * Dependencies
 * ============
 * modal
 * objects
 * table
 */
(function() {
	/*
	Structure
	=========
	{
		"id": 1234,
		"name": "Map",
		"kind": "map",
		"kind_data": {
			"x": 2,
			"y": 0,
			"width": 4,
			"height": 1,
			"data": "AEE=",
			"locked": false
		}
	}

	Info
	====
	* Table coordinate (0,0) is in the middle of a square. (Not between squares.)
	* (x,y) is the "center" of the bounding box containing the entire map.
	  * Except not exactly. It's the coordinates of the center assuming width and height are both odd.
	  * If the dimension is even, the coord is the coordinate of the center of the first square with a coordinate greater than the center.
	  * So, in table coordinates, the bounding box of the above example is (-0.5,-0.5)-(3.5,0.5).

	Just To Illustrate
	==================
	The above example:
	          (2,0)=(x,y)
	               |
	+-----+-----+--+--+-----+
	|     |     |  |  |     |
	|     |     |  V  |     |
	|     |     |  *  |     |
	|     |     |     |     |
	|     |     |     |     |
	+-----+-----+-----+-----+

	Encoded Format
	==============
	The format of the data itself is a binary format base64-encoded.

	The first byte is reserved for a version number.

	After that, it's fixed-width data (where the width equals one byte).

	The data is interpreted in order from the top left corner of (the bounding box of) the map proceeding right (in English reading order.)

	The upper four bits of each byte are a "count". The lower four are a color.

	So, for instance, a byte of 0x31 means "three whites". That means, from wherever the previous byte left off, the next three squares are to be white.

	This *can* wrap around to the next line. That is if the width is 3 and the data is (in hex) 00(version), 13(one orange), 41(four whites), 18(one brown), the result will be:

	+------+------+------+
	|orange|white |white |
	+------+------+------+
	|white |white |brown |
	+------+------+------+

	There's a "special" color value 0xf which means "copy the previous row colors into this row." This color cannot validly appear on the first row.

	The 0xf color can also wrap across rows. As an example, if we added to the previous example an 0x6f (00, 13, 41, 18, 6f), we'd get:

	+------+------+------+
	|orange|white |white |
	+------+------+------+
	|white |white |brown |
	+------+------+------+
	|white |white |brown |
	+------+------+------+
	|white |white |brown |
	+------+------+------+

	Note, there are multiple valid ways to construct this same grid. For instance (hex) 00, 13, 41, 18, 21, 18, 21, 18.

	The encoder is smart enough to try to optimize for data size by strategically using the "f" color where it can.

	If a map has more than 15 (0xf) of a particular color (or more than 15 "f's") sequentially, (that is if the count field needs to be greater than 15/0xf and thus overflows), the way it's dealt with is just use multiple bytes to represent that.

	For instance, if there are 36 whites in a row, those will be encoded as (hex) f1, f1, 61. (0xf=15 plus 0xf=15 plus 0x6=6 equals 36).

	I the last row can end before accounting for all squares in the last bit of the bounding box if the remaining squares are all transparent.

	Behavior is undefined if the data overflows the bounding box.
	*/

	var definition = {},
		colors = [null, "#ffffff", "#ff8888", "#ffcc88", "#ffff88", "#88ff88", "#8888ff", "#ff88ff", "#aa6622", "#aaaaaa", "#000000"],
		mode = "rectangle",
		history_id = null,
		history = [],
		maxhistory = 5,
		copied = null,
		tintbox = null,
		out = {};

	function check_history(obj) {
		if (obj.id !== history_id) {
			history = [];
			history_id = obj.id;
		}
	}

	function push_history(obj) {
		check_history(obj);

		history.push(obj);
		if (history.length > maxhistory) {
			history = history.slice(history.length - maxhistory);
		}
	}

	function pop_history() {
		var out = history[history.length-1];
		history = history.slice(0, history.length-1);
		return out;
	}

	function save_local(obj) {
		if (obj.decoded === true) {
			obj = obj.encode();
		}
		table_objects.save_local(obj);
	}

	function save(obj, old_obj, callback) {
		if (typeof(obj) === "number") {
			obj = table_objects.by_id(obj);
		}
		if (obj.decoded === true) {
			obj = obj.encode();
		}
		if (obj.kind_data.deleted === true) {
			table_objects.del(obj.id, callback);
			return;
		}
		if (old_obj !== undefined) {
			push_history(old_obj);
		}
		table_objects.save(obj, callback);
	}

	function decode(obj) {
		var data,
			out = {};

		data = atob(obj.kind_data.data);

		// Currently unused.
		// version = data.codePointAt(0);

		data = data.substring(1);

		data = (function() {
			var count = null,
				color = null,
				row = [],
				out = [];
			while ((data.length > 0 || count > 0) && (out.length <= 0 || out.length < obj.kind_data.height || out[out.length-1].length < obj.kind_data.width)) {
				if (count === null || count <= 0) {
					count = data.codePointAt(0) >> 4;
					color = data.codePointAt(0) & 0xf;
					data = data.substring(1);
				}
				if (color === 0xf) {
					row.push(out[out.length-1][row.length]);
				} else {
					row.push(color);
				}
				count--;
				if (row.length >= obj.kind_data.width) {
					out.push(row);
					row = [];
				}
			}
			return out;
		}());

		function to_data_coords(point) {
			return {
				"x": point.x - (obj.kind_data.x - Math.floor(obj.kind_data.width / 2)),
				"y": point.y - (obj.kind_data.y - Math.floor(obj.kind_data.height / 2))
			};
		}

		function x_oob_low(point) {
			return point.x < 0;
		}
		function y_oob_low(point) {
			return point.y < 0;
		}
		function x_oob_hi(point) {
			return point.x >= obj.kind_data.width;
		}
		function y_oob_hi(point) {
			return point.y >= obj.kind_data.height;
		}

		function oob(point) {
			return x_oob_low(point) || y_oob_low(point) || x_oob_hi(point) || y_oob_hi(point);
		}

		function optimize() {
			function stripzeroes(maxi, getval, fix) {
				var zeroes = true,
					i;

				while (data !== null && zeroes) {
					for (i = 0 ; i < maxi && zeroes ; i++) {
						zeroes = zeroes && getval(i) === 0;
					}
					if (zeroes && maxi > 0) {
						fix();
						if (obj.kind_data.width === 0 || obj.kind_data.height === 0) {
							data = null;
							obj.kind_data.width = 0;
							obj.kind_data.height = 0;
						}
					}
				}
			}

			// First row
			stripzeroes(obj.kind_data.width, function(i) { return data[0][i]; }, function() {
					obj.kind_data.height--;
					data = data.slice(1);
					if (obj.kind_data.height % 2 === 0) {
						obj.kind_data.y++;
					}
			});

			// Last row
			stripzeroes(obj.kind_data.width, function(i) { return data[obj.kind_data.height-1][i]; }, function() {
					obj.kind_data.height--;
					data = data.slice(0, obj.kind_data.height);
					if (obj.kind_data.height % 2 === 1) {
						obj.kind_data.y--;
					}
			});

			// First column
			stripzeroes(obj.kind_data.height, function(i) { return data[i][0]; }, function() {
					var i;
					obj.kind_data.width--;
					for (i = 0 ; i < obj.kind_data.height ; i++) {
						data[i] = data[i].slice(1);
					}
					if (obj.kind_data.width % 2 === 0) {
						obj.kind_data.x++;
					}
			});

			// Last column
			stripzeroes(obj.kind_data.height, function(i) { return data[i][obj.kind_data.width-1]; }, function() {
					var i;
					obj.kind_data.width--;
					for (i = 0 ; i < obj.kind_data.height ; i++) {
						data[i] = data[i].slice(0, obj.kind_data.width);
					}
					if (obj.kind_data.width % 2 === 1) {
						obj.kind_data.x--;
					}
			});
		}

		out.set = function(point, val) {
			var p = to_data_coords(point),
				i;
			while (x_oob_low(p)) {
				for (i = 0 ; i < obj.kind_data.height ; i++) {
					data[i].unshift(0);
				}
				obj.kind_data.width++;
				if (obj.kind_data.width % 2 === 1) {
					obj.kind_data.x--;
				}
				p = to_data_coords(point);
			}
			while (y_oob_low(p)) {
				data.unshift([]);
				for (i = 0 ; i < obj.kind_data.width ; i++) {
					data[0].push(0);
				}
				obj.kind_data.height++;
				if (obj.kind_data.height % 2 === 1) {
					obj.kind_data.y--;
				}
				p = to_data_coords(point);
			}
			while (x_oob_hi(p)) {
				for (i = 0 ; i < obj.kind_data.height ; i++) {
					data[i].push(0);
				}
				obj.kind_data.width++;
				if (obj.kind_data.width % 2 === 0) {
					obj.kind_data.x++;
				}
				p = to_data_coords(point);
			}
			while (y_oob_hi(p)) {
				data.push([]);
				for (i = 0 ; i < obj.kind_data.width ; i++) {
					data[data.length - 1].push(0);
				}
				obj.kind_data.height++;
				if (obj.kind_data.height % 2 === 0) {
					obj.kind_data.y++;
				}
				p = to_data_coords(point);
			}
			data[p.y][p.x] = val;
		};

		out.at = function(point) {
			var p = to_data_coords(point);
			if (oob(p)) {
				return 0;
			}
			return data[p.y][p.x];
		};

		out.color_at = function(point) {
			return colors[out.at(point)];
		};

		out.encode = function() {
			var x,
				y,
				count = 0,
				val,
				write,
				same = false,
				out = "";

			optimize();

			if (data === null) {
				return {
					"id": obj.id,
					"name": "Map",
					"kind": "map",
					"kind_data": {
						"height": 0,
						"width": 0,
						"x": 0,
						"y": 0,
						"data": "AA==",
						"deleted": true
					}
				};
			}

			val = data[0][0];

			out += String.fromCharCode(0); // Version. Currently unused, but reserved.

			for (y = 0 ; y < obj.kind_data.height ; y++) {
				for (x = 0 ; x < obj.kind_data.width ; x++) {
					write = true;
					same = same && y > 0 && data[y][x] === data[y-1][x];
					if (data[y][x] === val) {
						count++;
						write = false;
					} else if (same) {
						val = 0xf;
						count++;
						write = false;
					}
					if (count >= 0x10) {
						count = 0xf;
						write = true;
					}
					if (write) {
						out += String.fromCharCode((count & 0xf) << 4 | val);
						count = 1;
						same = y > 0 && data[y][x] === data[y-1][x];
						val = data[y][x];
					}
				}
			}
			out += String.fromCharCode((count & 0xf) << 4 | val);

			obj.kind_data.data = btoa(out);

			return obj;
		};

		out.iterate = function(f) {
			for (x = obj.kind_data.x - Math.floor(obj.kind_data.width / 2) ; x < obj.kind_data.x + Math.ceil(obj.kind_data.width / 2) ; x++) {
				for (y = obj.kind_data.y - Math.floor(obj.kind_data.height / 2) ; y < obj.kind_data.y + Math.ceil(obj.kind_data.height / 2) ; y++) {
					if (f({"x": x, "y": y}) === false) {
						return;
					}
				}
			}
		};

		out.decoded = true;

		return out;
	}

	function true_in(data, point) {
		return data.at({"x": Math.round(point.x), "y": Math.round(point.y)}) !== 0;
	}

	definition.name = "map";

	definition.z = function() {
		return 0;
	};

	definition.draw = function(obj, ctx) {
		var min = table.min_coords(),
			max = table.max_coords(),
			x,
			y,
			color,
			loc,
			tb0,
			data = decode(obj);

		for (x = Math.floor(min.x) ; x <= Math.ceil(max.x) ; x++) { // Should this be <=?
			for (y = Math.floor(min.y) ; y <= Math.ceil(max.y) ; y++) {
				color = data.color_at({"x": x, "y": y});
				if (color === null) {
					continue;
				}
				loc = table.to_canvas_coords({"x": x, "y": y});
				ctx.fillStyle = color;
				ctx.fillRect(loc.x - table.zoom * 0.97 / 2, loc.y - table.zoom * 0.97 / 2, table.zoom * 0.97, table.zoom * 0.97);
			}
		}

		if (tintbox !== null) {
			ctx.fillStyle = "rgba(255, 255, 192, 0.5)";
			tb0 = table.to_canvas_coords(tintbox[0]);
			ctx.fillRect(tb0.x - table.zoom / 2, tb0.y - table.zoom / 2, (1 + tintbox[1].x - tintbox[0].x) * table.zoom, (1 + tintbox[1].y - tintbox[0].y) * table.zoom);
		}
	};

	definition.in = function(obj, point) {
		return mode === "cut" || mode === "copy" || mode === "paste" || true_in(decode(obj), point);
	};

	definition.bounding_box = function(obj) {
		var th = obj.kind_data.width % 2 === 0 ? 0.5 : 0,
			tv = obj.kind_data.height % 2 === 0 ? 0.5 : 0;
		return {"min": {
			"x": obj.kind_data.x - obj.kind_data.width / 2 - th,
			"y": obj.kind_data.y - obj.kind_data.height / 2 - tv
		}, "max": {
			"x": obj.kind_data.x + obj.kind_data.width / 2 - th,
			"y": obj.kind_data.y + obj.kind_data.height / 2 - tv
		}};
	};

	definition.subobject_within = function(obj, rect) {
		var data = decode(table_objects.copy(obj)),
			x,
			y,
			out;

		for (x = obj.kind_data.x - Math.floor(obj.kind_data.width / 2) ; x < obj.kind_data.x + Math.ceil(obj.kind_data.width / 2) ; x++) {
			for (y = obj.kind_data.y - Math.floor(obj.kind_data.height / 2) ; y < obj.kind_data.y + Math.ceil(obj.kind_data.height / 2) ; y++) {
				if (x < rect.min.x + 0.5 || x > rect.max.x - 0.5 || y < rect.min.y + 0.5 || y > rect.max.y - 0.5) {
					data.set({"x": x, "y": y}, 0);
				}
			}
		}

		out = data.encode();

		if (out.deleted === true) {
			return null;
		}

		delete obj.id;

		return out;
	};

	definition.delete_within = function(obj, rect, callback) {
		var orig = table_objects.copy(obj),
			data = decode(obj),
			min = {"x": Math.round(rect.min.x) + 1, "y": Math.round(rect.min.y) + 1},
			max = {"x": Math.round(rect.max.x) - 1, "y": Math.round(rect.max.y) - 1},
			x,
			y;

		for (x = min.x ; x <= max.x ; x++) {
			for (y = min.y ; y <= max.y ; y++) {
				data.set({"x": x, "y": y}, 0);
			}
		}
		save(data, orig, callback);
	};

	definition.click = function(obj, point, callback) {
		var data = decode(obj),
			val;

		if ((mode === "cut" || mode === "copy" || mode === "paste") && !true_in(data, point)) {
			return false;
		}

		point = {"x": Math.round(point.x), "y": Math.round(point.y)};
		val = data.at(point);

		modal.modal(function(elem) {
			var child,
				i,
				box;

			elem.classList.add("map_modal");

			child = document.createElement("label");
			child.for = "color";
			child.appendChild(document.createTextNode("Color"));
			elem.appendChild(child);

			box = document.createElement("div");
			box.classList.add("map_modal_colorbox");
			elem.appendChild(box);

			function change_handler() {
				var old;

				if (this.checked) {
					old = table_objects.copy(data.encode());
					data.set(point, parseInt(this.dataset.val));
					save(data, old);
					table.draw();
				}
			}

			for (i = 0 ; i < colors.length ; i++) {
				child = document.createElement("input");
				child.type = "radio";
				child.name = "color";
				child.checked = val === i;
				if (colors[i] === null) {
					child.classList.add("map_modal_color_transparent");
					child.value = "";
				} else {
					child.style.backgroundColor = colors[i];
					child.value = colors[i];
				}
				child.id = "map_modal_color_" + i;
				child.classList.add("map_modal_color");
				child.dataset.val = i;
				child.addEventListener("change", change_handler);
				box.appendChild(child);
			}

			child = document.createElement("label");
			child.for = "mode";
			child.appendChild(document.createTextNode("Drawing mode"));
			elem.appendChild(child);

			box = document.createElement("div");
			box.classList.add("map_modal_mode");
			elem.appendChild(box);

			(function(l) {
				var i;

				function change_handler() {
					if (this.checked) {
						mode = this.value;
					}
				}

				for (i = 0 ; i < l.length ; i++) {
					if (i > 0 && i % 4 === 0) {
						box.appendChild(document.createElement("br"));
					}

					child = document.createElement("input");
					child.type = "radio";
					child.name = "mode";
					child.checked = mode === l[i];
					child.value = l[i];
					child.id = "map_modal_mode_" + l[i];
					child.classList.add("map_modal_mode");
					child.addEventListener("change", change_handler);
					box.appendChild(child);
					box.appendChild(document.createTextNode(l[i]));
				}
			}(["rectangle", "eraserect", "freehand", "erasor", "circle", "cut", "copy", "paste"]));

			child = document.createElement("label");
			child.for = "locked";
			child.appendChild(document.createTextNode("Locked"));
			elem.appendChild(child);

			child = document.createElement("input");
			child.type = "checkbox";
			child.name = "locked";
			child.id = "map_modal_locked";
			child.checked = obj.kind_data.locked;
			child.addEventListener("change", function(e) {
				e.preventDefault();

				obj.kind_data.locked = this.checked;
				table_objects.save(obj);
			});
			elem.appendChild(child);

			child = document.createElement("button");
			child.appendChild(document.createTextNode("done"));
			child.addEventListener("click", function() {
				modal.hide();
			});
			elem.appendChild(child);

			check_history(obj);
			child = document.createElement("button");
			child.appendChild(document.createTextNode("undo"));
			if (history.length === 0) {
				child.disabled = true;
			}
			child.addEventListener("click", function() {
				var hist = pop_history();
				if (hist !== undefined) {
					save(hist);
					table.draw();
				}
			});
			elem.appendChild(child);
		});

	};

	(function() {
		var draghandlers,
			endhandlers,
			dragdata = null;

		draghandlers = {
			"freehand": function(data, p, s) {
				var startval = data.at(s);
				if (data.at(p) !== startval) {
					data.set(p, startval);
					return data;
				}
			},
			"erasor": function(data, p, s) {
				if (data.at(p) !== 0) {
					data.set(p, 0);
					return data;
				}
			},
			"rectangle": function(data, p, s) {
				var minx = Math.min(s.x, p.x),
					miny = Math.min(s.y, p.y),
					maxx = Math.max(s.x, p.x),
					maxy = Math.max(s.y, p.y),
					x,
					y,
					at;

				data = decode(table_objects.copy(dragdata.encode()));
				at = data.at(s);

				for (x = minx ; x <= maxx ; x++) {
					for (y = miny ; y <= maxy ; y++) {
						data.set({"x": x, "y": y}, at);
					}
				}

				return data;
			},
			"eraserect": function(data, p, s) {
				var minx = Math.min(s.x, p.x),
					miny = Math.min(s.y, p.y),
					maxx = Math.max(s.x, p.x),
					maxy = Math.max(s.y, p.y),
					x,
					y;

				data = decode(table_objects.copy(dragdata.encode()));

				for (x = minx ; x <= maxx ; x++) {
					for (y = miny ; y <= maxy ; y++) {
						data.set({"x": x, "y": y}, 0);
					}
				}

				return data;
			},
			"circle": function(data, p, s) {
				var center = {"x": 0, "y": 0},
					radius,
					at,
					minx,
					miny,
					maxx,
					maxy;

				data = decode(table_objects.copy(dragdata.encode()));

				if (p.x == s.x && p.y == s.y) {
					return data;
				}

				if (Math.abs(p.y - s.y) > Math.abs(p.x - s.x)) { // down/up
					center.x = s.x;
					center.y = s.y + (p.y - s.y) / 2;
					radius = Math.abs(p.y - s.y) / 2;
				} else { // right/left
					center.x = s.x + (p.x - s.x) / 2;
					center.y = s.y;
					radius = Math.abs(p.x - s.x) / 2;
				}

				minx = Math.round(center.x - radius);
				miny = Math.round(center.y - radius);
				maxx = Math.round(center.x + radius);
				maxy = Math.round(center.y + radius);

				at = data.at(s);

				for (x = minx ; x <= maxx ; x++) {
					for (y = miny ; y <= maxy ; y++) {
						if ((x - center.x) ** 2 + (y - center.y) ** 2 <= (radius + 0.25) ** 2) {
							data.set({"x": x, "y": y}, at);
						}
					}
				}

				return data;
			},
			"cut": function(data, p, s) {
				var t;
				tintbox = [{"x": s.x, "y": s.y}, {"x": p.x, "y": p.y}];
				if (tintbox[1].x < tintbox[0].x) {
					t = tintbox[0].x;
					tintbox[0].x = tintbox[1].x;
					tintbox[1].x = t;
				}
				if (tintbox[1].y < tintbox[0].y) {
					t = tintbox[0].y;
					tintbox[0].y = tintbox[1].y;
					tintbox[1].y = t;
				}
			},
			"copy": function(data, p, s) {
				var t;
				tintbox = [{"x": s.x, "y": s.y}, {"x": p.x, "y": p.y}];
				if (tintbox[1].x < tintbox[0].x) {
					t = tintbox[0].x;
					tintbox[0].x = tintbox[1].x;
					tintbox[1].x = t;
				}
				if (tintbox[1].y < tintbox[0].y) {
					t = tintbox[0].y;
					tintbox[0].y = tintbox[1].y;
					tintbox[1].y = t;
				}
			},
			"paste": function(data, p, s) {
				var x,
					y,
					v;

				if (copied === null) {
					return;
				}

				data = decode(table_objects.copy(dragdata.encode()));

				p = {"x": Math.round(p.x), "y": Math.round(p.y)};
				for (y = p.y ; y < p.y + copied.length ; y++) {
					for (x = p.x ; x < p.x + copied[0].length ; x++) {
						v = copied[y-p.y][x-p.x];
						if (v !== 0) {
							data.set({"x": x, "y": y}, v);
						}
					}
				}

				return data;
			},
		};
		endhandlers = {
			"cut": function(data) {
				var x,
					y,
					row,
					p,
					old;

				copied = [];
				old = table_objects.copy(data.encode());
				for (y = tintbox[0].y ; y <= tintbox[1].y ; y++) {
					row = [];
					for (x = tintbox[0].x ; x <= tintbox[1].x ; x++) {
						p = {"x": x, "y": y};
						row.push(data.at(p));
						data.set(p, 0);
					}
					copied.push(row);
				}
				save(data, old);
				mode = "paste";
				tintbox = null;
			},
			"copy": function(data) {
				var x,
					y,
					row;

				copied = [];
				for (y = tintbox[0].y ; y <= tintbox[1].y ; y++) {
					row = [];
					for (x = tintbox[0].x ; x <= tintbox[1].x ; x++) {
						row.push(data.at({"x": x, "y": y}));
					}
					copied.push(row);
				}
				mode = "paste";
				tintbox = null;
			},
		};

		definition.drag = function(obj, point, start) {
			var data,
				p = {"x": Math.round(point.x), "y": Math.round(point.y)},
				s = {"x": Math.round(start.x), "y": Math.round(start.y)},
				handler_ret;

			if (obj.kind_data.locked) {
				return false;
			}

			if (dragdata === null) {
				dragdata = decode(table_objects.copy(obj));
			}

			data = decode(obj);

			handler_ret = draghandlers[mode](data, p, s);

			if (handler_ret !== undefined && handler_ret !== null) {
				save_local(handler_ret);
				table.draw();
			}
		};
		definition.drag_end = function(obj, callback) {
			if (obj.kind_data.locked) {
				return false;
			}

			if (mode in endhandlers) {
				endhandlers[mode](dragdata);
			} else {
				save(obj.id, table_objects.copy(dragdata.encode()));
			}
			dragdata = null;
			callback();
		};
	}());

	definition.create_override = function(obj, objects, callback) {
		var existing = null,
			old;
		objects.iterate(function(o) {
			if (o.kind === "map") {
				if (existing !== null) {
					console.log("Warning, more than one existing map found when 'create' was called for a map");
					return false; // Stop iterating.
				}
				existing = o;
			}
		});
		if (existing === null) {
			return false; // Allow the oridinary "create" process to complete
		}
		old = existing;
		existing = decode(table_objects.copy(existing));
		obj = decode(obj);
		obj.iterate(function(p) {
			var v = obj.at(p);
			if (v !== 0) {
				existing.set(p, v);
			}
		});
		save(existing, old, callback);
	};

	object_definitions.register(definition);

	return out;
}());
