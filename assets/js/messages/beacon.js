/**
 * Handles "beacon" messages.
 *
 * Dependencies
 * ============
 * channel
 * table
 */
(function() {
	channel.register_handler("beacon", function(name, data) {
		var id = table_objects.new_id();

		table_objects.save_local({
			"id": id,
			"name": "Beacon",
			"kind": "beacon",
			"kind_data": {
				"x": data.x,
				"y": data.y,
				"timeout": setTimeout(function() {
					table_objects.del_local(id, table.draw);
				}, 10000)
			}
		});
		table.draw();
	});
}());
