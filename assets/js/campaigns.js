/**
 * Allows campaign selection.
 *
 * Dependencies
 * ============
 * function_registry
 * images
 * login
 * objects
 * server
 * channel
 * hash_parse
 */
var campaigns = (function() {
	var parent_elem,
		out = {
			"campaign_loaded": function_registry.create(), // Function registry called when a campaign is loaded.
			"campaign": null, // The campaign object for the campaign currently loaded.
			"loaded": false // True if the campaign is loaded. False otherwise.
		};

	channel.register_handler("campaigninitdone", function(name, data) {
		pocket_objects.load(out.campaign.id, function() {
			out.loaded = true;
			out.campaign_loaded.call_functions();
		});
	});

	function campaign_handler(campaign) {
		out.campaign = campaign;
		parent_elem.style.display = "none";

		channel.connect(out.campaign.id);
	}

	login.logged_in.register(function() {
		parent_elem = document.getElementById("campaigns");

		server.campaigns(function(campaigns) {
			var loading_elem = document.getElementById("campaigns_loading"),
				campaigns_elem = document.getElementById("campaigns_campaigns"),
				button_elem,
				hash_campaign = parseInt(hash_parse.get_parameter("campaign")),
				i;

			if (!isNaN(hash_campaign)) {
				for (i = 0 ; i < campaigns.length ; i++) {
					if (campaigns[i].id === hash_campaign) {
						campaign_handler(campaigns[i]);
						return;
					}
				}
			}

			if (campaigns.length === 0) {
				campaigns_elem.appendChild(document.createTextNode("No campaigns found."));
				button_elem = document.createElement("button");
				button_elem.appendChild(document.createTextNode("logout"));
				button_elem.addEventListener("click", function(e) {
					e.preventDefault();

					server.logout(function() {
						location.reload();
					});
				});
				campaigns_elem.appendChild(button_elem);
			}

			function handler() {
				campaign_handler(JSON.parse(this.dataset.campaign));
			}

			for (i = 0 ; i < campaigns.length ; i++) {
				button_elem = document.createElement("button");
				button_elem.appendChild(document.createTextNode(campaigns[i].name));
				button_elem.dataset.campaign = JSON.stringify(campaigns[i]);
				button_elem.title = campaigns[i].id + ": " + campaigns[i].name;
				button_elem.addEventListener("click", handler);
				campaigns_elem.appendChild(button_elem);
			}
			loading_elem.style.display = "none";
			campaigns_elem.style.display = "block";
		});
	});

	return out;
}());
