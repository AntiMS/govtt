/**
 * Handles the image listing in the sidepanel.
 *
 * Dependencies
 * ============
 * campaigns
 * images
 * keymap
 * modal
 * sidepanel
 * objects
 * objects/token
 * table
 */
(function() {
	var side_elem;

	function change_handler() {
		if (side_elem !== undefined && side_elem.classList.contains("images")) {
			sidepanel.refresh();
		}
	}

	images.image_added.register(change_handler);
	images.image_changed.register(change_handler);

	sidepanel.register_section({
		"title": "Images",
		"name": "images",
		"key": keymap.keycode("i"),
		"populate": function(elem) {
			var child;

			side_elem = elem;
			elem.classList.add("images");

			child = document.createElement("button");
			child.appendChild(document.createTextNode("add image"));
			child.addEventListener("click", function(e) {
				e.preventDefault();

				modal.modal(function(elem, on_show) {
					var child;

					elem.classList.add("fetch_image_modal");

					child = document.createElement("label");
					child.appendChild(document.createTextNode("Name"));
					child.for = "name";
					elem.appendChild(child);

					child = document.createElement("input");
					child.type = "text";
					child.name = "name";
					child.id = "fetch_image_modal_name";
					elem.appendChild(child);

					child = document.createElement("label");
					child.appendChild(document.createTextNode("Url"));
					child.for = "url";
					elem.appendChild(child);

					child = document.createElement("input");
					child.type = "text";
					child.name = "url";
					child.id = "fetch_image_modal_url";
					elem.appendChild(child);

					child = document.createElement("label");
					child.appendChild(document.createTextNode("Upload"));
					child.for = "url";
					elem.appendChild(child);

					child = document.createElement("input");
					child.type = "file";
					child.name = "file";
					child.id = "fetch_image_modal_file";
					child.addEventListener("change", function() {
						var fr = new FileReader();

						fr.onload = function() {
							document.getElementById("fetch_image_modal_url").value = fr.result;
						};

						fr.readAsDataURL(this.files[0]);
					});
					elem.appendChild(child);

					child = document.createElement("button");
					child.appendChild(document.createTextNode("cancel"));
					child.addEventListener("click", function(e) {
						e.preventDefault();

						modal.hide();
					});
					elem.appendChild(child);

					child = document.createElement("button");
					child.appendChild(document.createTextNode("fetch"));
					child.addEventListener("click", function(e) {
						var button_elem = this,
							err_elem = document.getElementById("fetch_image_modal_error"),
							name = document.getElementById("fetch_image_modal_name").value,
							url = document.getElementById("fetch_image_modal_url").value,
							added_handler,
							added_ids = [],
							returned_id,
							remove_if_match;

						e.preventDefault();

						button_elem.disabled = true;

						while(err_elem.firstChild) {
							err_elem.removeChild(err_elem.firstChild);
						}

						if (name === "" || url === "") {
							err_elem.appendChild(document.createTextNode("Name and url are required"));
							button_elem.disabled = false;
						} else {
							remove_if_match = function() {
								var i;
								if (returned_id === undefined) {
									return;
								}
								for (i = 0 ; i < added_ids.length ; i++) {
									if (added_ids[i] === returned_id) {
										images.image_added.unregister(added_handler);
										sidepanel.refresh();
										modal.hide();
									}
								}
							};
							added_handler = function(img) {
								added_ids.push(img.id);
								remove_if_match();
							};
							images.image_added.register(added_handler);
							server.fetch_image(campaigns.campaign.id, url, name, function(id) {
								returned_id = id;
								remove_if_match();
							}, function(msg) {
								err_elem.appendChild(document.createTextNode(msg));
								button_elem.disabled = false;
								image.image_added.unregister(added_handler);
							});
						}
					});
					elem.appendChild(child);

					child = document.createElement("div");
					child.id = "fetch_image_modal_error";
					elem.appendChild(child);

					on_show(function () {
						document.getElementById("fetch_image_modal_name").focus();
					});
				});
			});
			elem.appendChild(child);

			function get_iterate_func(archived) {
				return function(img) {
					var a;

					if (!img.mine || img.archived !== archived) {
						return;
					}

					a = document.createElement("a");

					if (!archived) {
						a.appendChild(document.createElement("img"));
						a.lastChild.src = img.url;
					}

					a.dataset.image_id = img.id;

					a.appendChild(document.createTextNode(img.name));

					a.addEventListener("click", function() {
						var img = images.get(parseInt(this.dataset.image_id)),
							that = this;

						modal.modal(function(elem) {
							var child;

							elem.classList.add("image");

							if (!archived) {
								child = document.createElement("img");
								child.src = img.url;
								elem.appendChild(child);
							}

							child = document.createElement("label");
							child.for = "name";
							child.appendChild(document.createTextNode("Name"));
							elem.appendChild(child);

							child = document.createElement("input");
							child.type = "text";
							child.name = "name";
							child.id = "new_image_name";
							child.value = img.name;
							elem.appendChild(child);

							child = document.createElement("button");
							child.appendChild(document.createTextNode("cancel"));
							child.addEventListener("click", function(e) {
								modal.hide();
							});
							elem.appendChild(child);

							child = document.createElement("button");
							child.appendChild(document.createTextNode("new token"));
							child.addEventListener("click", function(e) {
								var loc = table.place_location({"width": 1, "height": 1}, object_definitions.definition("token").z());
								token.edit_modal({
									"name": img.name,
									"kind": "token",
									"kind_data": {
										"round": true,
										"color": null,
										"x": 0,
										"y": 0,
										"width": 1,
										"height": 1,
										"image_id": img.id,
										"image_x": 0.5,
										"image_y": 0.5,
										"image_size": 1
									}
								}, table.draw, loc);
							});
							elem.appendChild(child);

							child = document.createElement("button");
							child.appendChild(document.createTextNode("new table image"));
							child.addEventListener("click", function(e) {
								var size = 150 / table.zoom,
									avgdim = (img.image.width + img.image.height) / 2,
									dims = {"width": size * img.image.width / avgdim, "height": size * img.image.height / avgdim},
									loc = table.place_location(dims, object_definitions.definition("image").z());
								table_objects.create({
									"name": img.name,
									"kind": "image",
									"kind_data": {
										"image_id": img.id,
										"image_size": size,
										"x": loc.x,
										"y": loc.y,
										"locked": false
									}
								}, table.draw);

								modal.hide();
							});
							elem.appendChild(child);

							child = document.createElement("button");
							child.appendChild(document.createTextNode(archived ? "unarchive" : "archive"));
							child.addEventListener("click", function(e) {
								var func = archived ? server.unarchive_image : server.archive_image;
								func(img.id, sidepanel.refresh);
								modal.hide();
							});
							elem.appendChild(child);

							child = document.createElement("button");
							child.appendChild(document.createTextNode("rename"));
							child.addEventListener("click", function(e) {
								var name = document.getElementById("new_image_name").value;
								server.rename_image(img.id, name, sidepanel.refresh);
								modal.hide();
							});
							elem.appendChild(child);
						});
					});

					elem.appendChild(a);
				};
			}

			images.iterate(get_iterate_func(false));
			images.iterate(get_iterate_func(true));
		}
	});
}());
