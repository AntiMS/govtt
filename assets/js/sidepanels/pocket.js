/**
 * Handles the image listing in the sidepanel.
 *
 * Dependencies
 * ============
 * campaigns
 * keymap
 * objects
 * images
 * sidepanel
 * table
 * objects/token
 */
(function() {
	sidepanel.register_section({
		"title": "Pocket",
		"name": "pocket",
		"key": keymap.keycode("p"),
		"populate": function(elem) {
			elem.classList.add("pocket");

			pocket_objects.load(campaigns.campaign.id, function () {
				pocket_objects.iterate(function(obj) {
					var a = document.createElement("a");

					a.appendChild(document.createElement("img"));
					a.lastChild.src = images.get(obj.kind_data.image_id).url;

					a.appendChild(document.createTextNode(obj.name));

					a.dataset.obj = JSON.stringify(obj);

					a.addEventListener("click", function() {
						var obj = JSON.parse(this.dataset.obj);

						token.edit_modal(obj,
							function() {
								table.draw();
								sidepanel.refresh();
							},
							table.place_location(
								{
									"width": obj.kind_data.width,
									"height": obj.kind_data.height
								},
								object_definitions.definition("token").z()
							),
							pocket_objects.save,
							pocket_objects.del
						);
					});

					elem.appendChild(a);
				});
			});
		}
	});
}());
