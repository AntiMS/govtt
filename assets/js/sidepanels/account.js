/**
 * Provides account operations in a sidebar.
 *
 * Dependencies
 * ============
 * keymap
 * login
 * modal
 * server
 * sidepanel
 */
(function() {
	sidepanel.register_section({
		"title": "Account",
		"name": "account",
		"key": keymap.keycode("a"),
		"populate": function(elem) {
			server.campaign_users(campaigns.campaign.id, function(users) {
				var i,
					display_name,
					child,
					obj;

				for (i = 0 ; i < users.length ; i++) {
					if (login.username === users[i].username) {
						display_name = users[i].display_name;
					}
				}

				elem.classList.add("account");

				child = document.createElement("div");
				child.appendChild(document.createTextNode("Username: " + login.username));
				elem.appendChild(child);

				child = document.createElement("div");
				child.id = "account_display_name";
				child.appendChild(document.createTextNode("Display Name: " + display_name));
				elem.appendChild(child);

				child = document.createElement("button");
				child.appendChild(document.createTextNode("change display name"));
				child.addEventListener("click", function(e) {
					modal.modal(function(elem) {
						var child;

						elem.classList.add("change_display_name");

						child = document.createElement("label");
						child.appendChild(document.createTextNode("Display Name"));
						child.for = "display_name";
						elem.appendChild(child);

						child = document.createElement("input");
						child.type = "display_name";
						child.name = "display_name";
						child.value = display_name;
						child.id = "change_display_name_display_name";
						elem.appendChild(child);

						child = document.createElement("button");
						child.appendChild(document.createTextNode("cancel"));
						child.addEventListener("click", function(e) {
							e.preventDefault();

							modal.hide();
						});

						elem.appendChild(child);
						child = document.createElement("button");
						child.appendChild(document.createTextNode("change"));
						child.addEventListener("click", function(e) {
							var new_dn = document.getElementById("change_display_name_display_name").value,
								dn_disp = document.getElementById("account_display_name");

							server.change_display_name(campaigns.campaign.id, new_dn, function() {
								sidepanel.refresh();

								while (dn_disp.firstChild) {
									dn_disp.removeChild(dn_disp.firstChild);
								}

								dn_disp.appendChild(document.createTextNode("Display Name: " + new_dn));

								display_name = new_dn;

								modal.hide();
							});
						});
						elem.appendChild(child);
					});
				});
				elem.appendChild(child);

				child = document.createElement("button");
				child.appendChild(document.createTextNode("change password"));
				child.addEventListener("click", function(e) {
					e.preventDefault();

					modal.modal(function(elem) {
						var child;

						elem.classList.add("change_pw");

						child = document.createElement("label");
						child.appendChild(document.createTextNode("Old Password"));
						child.for = "old_password";
						elem.appendChild(child);

						child = document.createElement("input");
						child.type = "password";
						child.name = "old_password";
						child.id = "change_pw_old";
						elem.appendChild(child);

						child = document.createElement("label");
						child.appendChild(document.createTextNode("New Password"));
						child.for = "new_password";
						elem.appendChild(child);

						child = document.createElement("input");
						child.type = "password";
						child.name = "new_password";
						child.id = "change_pw_new";
						elem.appendChild(child);

						child = document.createElement("label");
						child.appendChild(document.createTextNode("New Password (again)"));
						child.for = "new_password_2";
						elem.appendChild(child);

						child = document.createElement("input");
						child.type = "password";
						child.name = "new_password_2";
						child.id = "change_pw_new_2";
						elem.appendChild(child);

						child = document.createElement("div");
						child.id = "change_pw_message";
						elem.appendChild(child);

						child = document.createElement("button");
						child.appendChild(document.createTextNode("cancel"));
						child.addEventListener("click", function(e) {
							e.preventDefault();

							modal.hide();
						});

						elem.appendChild(child);
						child = document.createElement("button");
						child.appendChild(document.createTextNode("change"));
						child.addEventListener("click", function(e) {
							var o,
								n,
								n2,
								m;

							e.preventDefault();

							o = document.getElementById("change_pw_old").value;
							n = document.getElementById("change_pw_new").value;
							n2 = document.getElementById("change_pw_new_2").value;
							m = document.getElementById("change_pw_message");

							while (m.firstChild) {
								m.removeChild(m.firstChild);
							}

							if (o === "" || n === "" || n2 === "") {
								m.appendChild(document.createTextNode("All fields required."));
								return;
							}
							if (n !== n2) {
								m.appendChild(document.createTextNode("Passwords do not match."));
								return;
							}

							server.change_password(o, n, function() {
								modal.hide();
							}, function(msg) {
								m.appendChild(document.createTextNode(msg));
							});
						});
						elem.appendChild(child);
					});
				});
				elem.appendChild(child);

				child = document.createElement("button");
				child.appendChild(document.createTextNode("logout"));
				child.addEventListener("click", function(e) {
					e.preventDefault();

					server.logout(function() {
						location.reload();
					});
				});
				elem.appendChild(child);

				elem.appendChild(document.createTextNode("Users:"));

				child = document.createElement("div");
				child.classList.add("account_users");

				function click_handler(e) {
					e.preventDefault();

					notifications.direct_message(this.dataset.username, this.dataset.display_name);
				}

				for (i = 0 ; i < users.length ; i++) {
					child.appendChild(document.createElement("a"));
					child.lastChild.appendChild(document.createTextNode(users[i].display_name));
					child.lastChild.dataset.username = users[i].username;
					child.lastChild.dataset.display_name = users[i].display_name;
					child.lastChild.addEventListener("click", click_handler);
				}
				elem.appendChild(child);
			});
		}
	});
}());
