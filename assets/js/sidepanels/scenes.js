/**
 * Handles the scenes listing in the sidepanel.
 *
 * Dependencies
 * ============
 * campaigns
 * keymap
 * modal
 * scene
 * server
 * sidepanel
 * table
 */
(function() {
	var side_elem;

	table.scene_loaded.register_async(function(callback) {
		if (side_elem !== undefined && side_elem.classList.contains("scenes")) {
			sidepanel.refresh();
		}
		callback();
	});

	sidepanel.register_section({
		"name": "scenes",
		"title": "Scenes",
		"key": keymap.keycode("s"),
		"populate": function(elem) {
			var button;

			side_elem = elem;
			elem.classList.add("scenes");

			button = document.createElement("button");
			button.appendChild(document.createTextNode("campaign scene"));
			button.addEventListener("click", function(e) {
				if (table.scene_id !== campaigns.campaign.scene_id) {
					table.load_scene(campaigns.campaign.scene_id);
				}
			});
			elem.appendChild(button);

			button = document.createElement("button");
			button.appendChild(document.createTextNode("new scene"));
			button.addEventListener("click", function(e) {
				modal.modal(function(elem) {
					var child;

					elem.classList.add("new_scene");

					child = document.createElement("label");
					child.for = "name";
					child.appendChild(document.createTextNode("Name"));
					elem.appendChild(child);

					child = document.createElement("input");
					child.type = "text";
					child.name = "name";
					child.id = "new_scene_name";
					elem.appendChild(child);

					child = document.createElement("button");
					child.appendChild(document.createTextNode("cancel"));
					child.addEventListener("click", function(e) {
						e.preventDefault();

						modal.hide();
					});
					elem.appendChild(child);

					child = document.createElement("button");
					child.appendChild(document.createTextNode("create"));
					child.addEventListener("click", function(e) {
						var name;

						e.preventDefault();

						name = document.getElementById("new_scene_name").value;

						if (name === "" || name === undefined || name === null) {
							return;
						}

						server.new_scene(campaigns.campaign.id, name, function(id) {
							table.load_scene(id);
							modal.hide();
						});
					});
					elem.appendChild(child);
				});
			});
			elem.appendChild(button);

			scene.iterate(function(scene) {
				var a = document.createElement("a");

				if (scene.id === campaigns.campaign.scene_id) {
					a.classList.add("scene_campaign_scene");
				}
				if (scene.id === table.scene_id) {
					a.classList.add("scene_loaded_scene");
				}

				a.dataset.scene_id = scene.id;
				a.dataset.scene_name = scene.name;

				a.appendChild(document.createTextNode(scene.name));

				a.addEventListener("click", function() {
					var that = this;

					modal.modal(function(elem) {
						var child;

						elem.classList.add("scene");

						child = document.createElement("label");
						child.for = "name";
						child.appendChild(document.createTextNode("Name"));
						elem.appendChild(child);

						child = document.createElement("input");
						child.type = "text";
						child.name = "name";
						child.id = "new_scene_name";
						child.value = that.dataset.scene_name;
						elem.appendChild(child);

						child = document.createElement("button");
						child.appendChild(document.createTextNode("cancel"));
						child.addEventListener("click", function(e) {
							modal.hide();
						});
						elem.appendChild(child);

						child = document.createElement("button");
						child.appendChild(document.createTextNode("set for everybody"));
						child.addEventListener("click", function(e) {
							var scene_id = parseInt(that.dataset.scene_id);

							if (campaigns.campaign.scene_id === scene_id) {
								table.load_scene(scene_id);
								modal.hide();
								return;
							}

							table.set_scene(scene_id);
							modal.hide();
						});
						elem.appendChild(child);

						child = document.createElement("button");
						child.appendChild(document.createTextNode("set just for me"));
						child.addEventListener("click", function(e) {
							table.load_scene(parseInt(that.dataset.scene_id));
							modal.hide();
						});
						elem.appendChild(child);

						child = document.createElement("button");
						child.appendChild(document.createTextNode("rename"));
						child.addEventListener("click", function(e) {
							var name = document.getElementById("new_scene_name").value,
								id = parseInt(that.dataset.scene_id);
							server.rename_scene(id, name, sidepanel.refresh);
							modal.hide();
						});
						elem.appendChild(child);
					});
				});

				elem.appendChild(a);
			});
		}
	});
}());
