/**
 * Sidepanel for miscellaneous things that don't belong in other sidepanels.
 *
 * Has in its definition (fetchable via `sidepanel.definition_by_name("tools")`) an "on_populate" property which is a function registry which is called when the tools panel is shown, just after population. To it is passed the panel div element and an `add_button(name, handler_func)` method which will append a button to the list of buttons in the tools sidepanel.
 *
 * Dependencies
 * ============
 * campaigns
 * client_objects/beacon
 * dice
 * function_registry
 * hash_parse
 * keymap
 * modal
 * objects
 * sidepanel
 * table
 */
(function() {
	var on_populate = function_registry.create();

	if (hash_parse.get_parameter("lock_view") !== undefined) {
		table.before_draw.register(table.snap_to_bounding);
	}

	sidepanel.register_section({
		"on_populate": on_populate,
		"title": "Tools",
		"name": "tools",
		"key": keymap.keycode("t"),
		"populate": function(elem) {
			var obj,
				add_button = function(text, handler) {
					var button = document.createElement("button");
					button.appendChild(document.createTextNode(text));
					button.addEventListener("click", handler);
					elem.appendChild(button);
				};

			table_objects.iterate(function(o) {
				if (o.kind === "map") {
					obj = o;
				}
			});

			elem.classList.add("tools");

			add_button("add drawing", function(e) {
				e.preventDefault();

				table_objects.create({
					"kind": "drawing",
					"name": "Drawing",
					"kind_data": {
						"color": "#000000",
						"points": [table.place_location(
							{"width": 0.3, "height": 0.3},
							object_definitions.definition("drawing").z()
						)]
					}
				}, function() {
					table.draw();
				});
			});

			add_button("add rectangle", function(e) {
				var s,
					loc;

				e.preventDefault();

				s = Math.max(0.75, 40 / table.zoom);
				loc = table.place_location(
					{"width": s, "height": s},
					object_definitions.definition("rectangle").z()
				);

				table_objects.create({
					"kind": "rectangle",
					"name": "Rectangle",
					"kind_data": {
						"min": {"x": loc.x - s, "y": loc.y - s},
						"max": {"x": loc.x + s, "y": loc.y + s},
						"color": "#000000",
						"locked": false,
					}
				}, function() {
					table.draw();
				});
			});

			add_button("add label", function(e) {
				var obj = {
						"kind": "label",
						"name": "Label",
						"kind_data": {
							"text": "Label",
							"x": 0,
							"y": 0,
							"size": 0.25,
							"foreground": "#000000",
							"background": "#ffffff"
						}
					},
					def = object_definitions.definition("label"),
					box = def.bounding_box(obj),
					dims = {"width": box.max.x - box.min.x, "height": box.max.y - box.min.y},
					loc = table.place_location(
						dims,
						def.z()
					);

				e.preventDefault();

				obj.kind_data.x = loc.x;
				obj.kind_data.y = loc.y;

				table_objects.create(obj, table.draw);
			});

			if (obj === undefined) {
				add_button("add map", function(e) {
					var loc;

					e.preventDefault();

					loc = table.place_location(
						{"width": 1, "height": 1},
						object_definitions.definition("map").z()
					);

					table_objects.create({
						"kind": "map",
						"name": "Map",
						"kind_data": {
							"x": Math.round(loc.x),
							"y": Math.round(loc.y),
							"height": 1,
							"width": 1,
							"data": "ABE=" // One white square.
						}
					}, function() {
						table.draw();
						sidepanel.refresh();
					});
				});
			} else {
				add_button("remove map", function(e) {
					e.preventDefault();

					modal.modal(function(elem) {
						var child;

						elem.classList.add("delete_map");

						elem.appendChild(document.createTextNode("Are you sure?"));

						child = document.createElement("button");
						child.appendChild(document.createTextNode("cancel"));
						child.addEventListener("click", function(e) {
							e.preventDefault();

							modal.hide();
						});
						elem.appendChild(child);

						child = document.createElement("button");
						child.appendChild(document.createTextNode("delete"));
						child.addEventListener("click", function(e) {
							e.preventDefault();

							table_objects.del(obj.id, function() {
								table.draw();
								sidepanel.refresh();
							});
							modal.hide();
						});
						elem.appendChild(child);
					});
				});
			}

			add_button("beacon", function(e) {
				e.preventDefault();

				beacon.mouse_override();
			});

			add_button("roll dice", function(e) {
				e.preventDefault();

				dice.show();
			});

			if (table.before_draw.is_registered(table.snap_to_bounding)) {
				add_button("unlock view", function(e) {
					e.preventDefault();
					table.before_draw.unregister(table.snap_to_bounding);
					sidepanel.refresh();
				});
			} else {
				add_button("lock view", function(e) {
					e.preventDefault();
					table.before_draw.register(table.snap_to_bounding);
					table.draw();
					sidepanel.refresh();
				});
			}

			on_populate.call_functions(null, elem, add_button);
		}
	});
}());
