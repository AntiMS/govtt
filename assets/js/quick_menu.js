/**
 * Handles "quick" operations for keyboard shortcuts and the quick operations list.
 *
 * Dependencies
 * ============
 * keymap
 * modal
 */
var quick_menu = (function() {
	var out = {},
		registered = [],
		by_key = {};

	out.register = function(name, key, func) {
		var i,
			j,
			obj = {
				"name": name,
				"func": func
			};
		for (i = 0 ; i < registered.length ; i++) {
			if (registered[i].name > name) { // Could be optimized with a binary search... for now, I'm lazy.
				break;
			}
		}
		if (key !== undefined && key !== null) {
			if (by_key[key] === undefined) {
				by_key[key] = obj;
			} else {
				console.log("ERROR: Key " + key + " already registered");
			}
			obj.key = key;
		}
		registered.splice(i, 0, obj);
	};

	out.key = function(event) {
		var obj = by_key[event.keyCode];
		if (obj === undefined) {
			return false;
		}
		event.preventDefault();
		obj.func();
		return true;
	};

	out.show = function() {
		modal.modal(function(elem, onshow) {
			var child,
				row,
				box;

			elem.classList.add("quick_menu_modal");

			child = document.createElement("input");
			child.type = "text";
			child.name = "command";
			child.id = "quick_menu_modal_input";
			child.addEventListener("keyup", function(e) {
				var cs = document.getElementById("quick_menu_modal_box").children,
					i;
				if (e.keyCode === keymap.keycode("ENTER")) {
					for (i = 0 ; i < cs.length ; i++) {
						if (!cs[i].classList.contains("hidden")) {
							modal.hide();
							registered[cs[i].dataset.index].func();
							break;
						}
					}
					return;
				} else if (e.keyCode === keymap.keycode("ESCAPE")) {
					modal.hide();
					return;
				}
				for (i = 0 ; i < cs.length ; i++) {
					if (this.value === "" || cs[i].dataset.name.includes(this.value)) {
						cs[i].classList.remove("hidden");
					} else {
						cs[i].classList.add("hidden");
					}
				}
			});
			elem.appendChild(child);

			function row_click_handler() {
				modal.hide();
				registered[this.dataset.index].func();
				return false;
			}

			box = document.createElement("div");
			box.id = "quick_menu_modal_box";
			for (i = 0 ; i < registered.length ; i++) {
				row = document.createElement("a");
				row.dataset.name = registered[i].name;
				row.classList.add("quick_menu_modal_row");
				child = document.createElement("span");
				child.classList.add("quick_menu_modal_name");
				child.appendChild(document.createTextNode(registered[i].name));
				row.appendChild(child);
				if (registered[i].key !== undefined) {
					child = document.createElement("span");
					child.classList.add("quick_menu_modal_key");
					child.appendChild(document.createTextNode(keymap.char(registered[i].key)));
				}
				row.appendChild(child);
				row.dataset.index = i;
				row.addEventListener("click", row_click_handler);
				box.appendChild(row);
			}
			elem.appendChild(box);

			child = document.createElement("button");
			child.appendChild(document.createTextNode("exit"));
			child.addEventListener("click", function() {
				modal.hide();
			});
			elem.appendChild(child);

			onshow(function() {
				document.getElementById("quick_menu_modal_input").focus();
			});
		});
	};

	out.register("show quickmenu", keymap.keycode("."), out.show);

	return out;
}());
