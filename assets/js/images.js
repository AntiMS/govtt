/**
 * Handles campaign images.
 *
 * Image metadata is always of the following form:
 *
 * {
 *   "id": <integer>,
 *   "name": <string>,
 *   "mine": <bool -- indicates whether this image belongs to the current user>,
 *   "url": <string>,
 *   "archived": <bool>,
 *   // The above are all from the server and come straight from the channel message. The below are created/added on the client.
 *   "image": <HTML Image object>,
 *   "loading": <bool -- false unless the image has started but not finished loading>,
 *   "loaded": <bool -- true if finished loading (even if there was an error>,
 *   "error": <bool -- true if there was an error>,
 *   "onload": <function_registry -- undefined if loaded is true>
 * }
 *
 * Dependencies
 * ============
 * channel
 * function_registry
 * server
 */
var images = (function() {
	var image_objects = {},
		out = {};

	function save(img) {
		if (image_objects[img.id] !== undefined) {
			console.log("Warning: Saved image already present");
 		}
		img.url = '/image/' + img.id;
		img.loading = false;
		img.loaded = false;
		img.error = false;
		img.image = new Image();
		img.onload = function_registry.create();
		image_objects[img.id] = img;
		out.image_added.call_functions(null, img);
	}

	function update(img) {
		var prev = image_objects[img.id];
		if (prev === undefined) {
			console.log("Warning: Updated image not found");
			save(img);
			return;
		}
		prev.archived = img.archived;
		prev.name = img.name;
		out.image_changed.call_functions(null, prev);
	}

	function sort() {
		var k,
			out = [];

		for (k in image_objects) {
			out.push(parseInt(k));
		}

		out.sort(function(a, b) {
			return image_objects[a].name.localeCompare(image_objects[b].name);
		});

		return out;
	}

	/**
	 * Returns an image metadata object by id.
	 *
	 * Returns
	 * =======
	 */
	out.get = function(id) {
		return image_objects[id];
	};

	/**
	 * Iterates over all images, calling the given callback for each.
	 *
	 * Parameters
	 * ==========
	 * callback
	 *     A function called once for each registered image. For each call, an image metadata object will be passed as the only argument.
	 */
	out.iterate = function(callback) {
		var alphabetical = sort(),
			i;

		for (i = 0 ; i < alphabetical.length ; i++) {
			callback(image_objects[alphabetical[i]]);
		}
	};

	/**
	 * Loads a list of images by id and calls the given callback after. If already loaded, simply calls the callback.
	 *
	 * Parameters
	 * ==========
	 * callback
	 *     A callback to call when all specified images are loaded. Called variadically with all image objects referred to in the ids list, though no guarantees are made regarding order of the arguments.
	 * any number of other arguments
	 *     Integer ids of images to load.
	 */
	out.load = function(callback) {
		var i,
			t,
			to_remove = [],
			to_load = [],
			loaded_objects = [];

		for (i = 1 ; i < arguments.length ; i++) {
			to_load.push(arguments[i]);
		}

		function loaded() {
			var i = 0;
			while (i < to_load.length) {
				if (image_objects[to_load[i]].loaded) {
					loaded_objects.push(image_objects[to_load[i]]);
					to_load.splice(i, 1);
				} else {
					i++;
				}
			}
			if (to_load.length === 0 && callback !== undefined && callback !== null) {
				callback.apply(null, loaded_objects);
				callback = null;
			}
		}

		function get_onload(t) {
			return function () {
				var r = t.onload;
				t.image.onload = undefined;
				t.loading = false;
				t.loaded = true;
				t.error = !(t.image.complete && t.image.naturalWidth !== 0);
				delete t.onload;
				r.call_functions();
			};
		}

		for (i = 0 ; i < to_load.length ; i++) {
			if (image_objects[to_load[i]].loaded) {
				loaded_objects.push(image_objects[to_load[i]]);
				to_remove.push(i);
			} else {
				image_objects[to_load[i]].onload.register(loaded);
				if (!image_objects[to_load[i]].loading) {
					t = image_objects[to_load[i]];
					t.loading = true;
					t.image.onload = get_onload(t);
					t.image.src = t.url;
				}
			}
		}

		for (i = to_remove.length - 1 ; i >= 0 ; i--) {
			to_load.splice(to_remove[i], 1);
		}

		loaded();
	};

	/**
	 * A channel handler which receives "newimage" messages.
	 */
	channel.register_handler("newimage", function(name, data) {
		save(data);
	});

	/**
	 * A channel handler which receives "imagechanged" messages.
	 */
	channel.register_handler("imagechanged", function(name, data) {
		update(data);
	});

	/**
	 * A function registry called every time a new image is added. Called with the image metadata object as the sole argument.
	 */
	out.image_added = function_registry.create();

	/**
	 * A function registry called every time an image is changed. Called with the (new) image metadata object as the sole argument.
	 */
	out.image_changed = function_registry.create();

	return out;
}());
