/**
 * Parses the url hash. Hash should be of the form "name2[=value][,name2[=value]...]".
 */
var hash_parse = (function () {
	var parsed;

	(function () {
		var toparse,
			item;

		toparse = location.hash.slice(1).split("&");
		if (location.search !== undefined) {
			toparse = toparse.concat(location.search.slice(1).split("&"));
		}

		parsed = {};

		while (toparse.length > 0) {
			item = toparse[0];
			toparse = toparse.slice(1);
			if (item === "") {
				continue;
			}
			item = item.split("=");
			if (item.length == 1) {
				item = [item, null];
			} else {
				item = [item[0], item.slice(1).join("=")];
			}
			if (parsed[item[0]] !== undefined) {
				console.log("WARNING: duplicate hash item", item);
				continue;
			}
			parsed[item[0]] = item[1];
		}

		history.replaceState(null, null, '/' + location.search);
	}());

	return {
		/**
		 * Gets a parameter from the hash by name.
		 *
		 * Parameters
		 * ==========
		 * name
		 *   The name of the parameter to retrieve as a string.
		 *
		 * Returns
		 * =======
		 * Undefined if no such parameter is present. Null if the parameter is present with no value. The string value of the parameter otherwise.
		 */
		"get_parameter": function(name) {
			return parsed[name];
		}
	};
}());
