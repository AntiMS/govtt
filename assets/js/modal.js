/**
 * Displays a modal.
 */
var modal = (function() {
	var out = {"is_visible": false},
		parent,
		modal;


	function get_onshow() {
		var callback;
		return {
			"set_callback": function(cb) {
				callback = cb;
			},
			"call_callback": function() {
				if (callback !== undefined) {
					callback();
				}
			}
		};
	}

	out.clear = function() {
		modal.className = '';
		while (modal.firstChild) {
			modal.removeChild(modal.firstChild);
		}
	};

	out.modal = function(setup) {
		var onshow = get_onshow();

		parent = document.getElementById("modal");
		modal = document.getElementById("modal_modal");

		out.clear();
		setup(modal, onshow.set_callback);

		parent.style.display = "block";
		out.is_visible = true;
		onshow.call_callback();
	};

	out.hide = function() {
		parent.style.display = "none";
		out.is_visible = false;
		out.clear();
	};

	return out;
}());
