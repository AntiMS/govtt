/**
 * The "rectangle" used on the client to represent the region being selected during a clipboard cut/copy operation.
 *
 * Dependencies
 * ============
 * objects
 * table
 */
var clipboard_select = (function() {
	/*
	Structure
	=========
	{
		"id": 1234,
		"name": "Clipboard Select",
		"kind": "clipboard_select",
		"kind_data": {
			"min": {"x": 1.72, "y": -0.15},
			"max": {"x": 3.91, "y": 0.95}
		}
	}
	*/

	var out = {},
		definition = {},
		constants;

	constants = {
		"fill_style": "rgba(255, 255, 0, 0.50)"
	};

	definition.name = "clipboard_select";

	definition.client_only = true;

	definition.z = function(obj) {
		return 7;
	};

	definition.draw = function(obj, ctx) {
		var min = table.to_canvas_coords(obj.kind_data.min),
			max = table.to_canvas_coords(obj.kind_data.max);

		ctx.fillStyle = constants.fill_style;
		ctx.fillRect(min.x, min.y, max.x - min.x, max.y - min.y);
	};

	definition.bounding_box = function(obj) {
		return {"min": obj.kind_data.min, "max": obj.kind_data.max};
	};

	object_definitions.register(definition);

	// Returns a function which can be used to update the "other end" of the rectangle.
	out.create = function(p) {
		var obj = {
			"id": table_objects.new_id(),
			"name": "Clipboard Select",
			"kind": "clipboard_select",
			"kind_data": {
				"min": {"x": p.x, "y": p.y},
				"max": {"x": p.x, "y": p.y}
			}
		};

		table_objects.save_local(obj);

		return {
			"bounds": function() {
				return {"min": obj.kind_data.min, "max": obj.kind_data.max};
			},
			"update": function(p2) {
				obj.kind_data.min.x = Math.min(p.x, p2.x);
				obj.kind_data.min.y = Math.min(p.y, p2.y);
				obj.kind_data.max.x = Math.max(p.x, p2.x);
				obj.kind_data.max.y = Math.max(p.y, p2.y);
				table_objects.save_local(obj);
				table.draw();
			},
			"del": function() {
				table_objects.del_local(obj.id, table.draw);
			}
		};
	};

	return out;
}());
