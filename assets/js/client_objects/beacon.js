/**
 * A "beacon" is a mark placed placed on the table to direct others' attention to that location.
 *
 * Dependencies
 * ============
 * campaigns
 * keymap
 * objects
 * quick_menu
 * server
 * table
 */
var beacon = (function() {
	/*
	Structure
	=========
	{
		"id": 1234,
		"name": "Beacon",
		"kind": "beacon",
		"kind_data": {
			"x": 2.72,
			"y": -1.15,
			"timeout": 1234
		}
	}

	Info
	====
	The "timeout" is the value returned by "setTimeout(...)". Can be used to cancel/clear the timeout.
	*/

	var out = {},
		definition = {},
		constants;

	constants = {
		"radius": 0.5,
		"fill_style": "rgba(255, 255, 0, 0.75)"
	};

	definition.name = "beacon";

	definition.client_only = true;

	definition.z = function(obj) {
		return 6;
	};

	definition.draw = function(obj, ctx) {
		var coords = obj.kind_data,
			min_coords = table.min_coords(),
			max_coords = table.max_coords();

		coords = {"x": Math.min(Math.max(coords.x, min_coords.x), max_coords.x), "y": Math.min(Math.max(coords.y, min_coords.y), max_coords.y)};
		coords = table.to_canvas_coords(coords);

		ctx.beginPath();
		ctx.arc(coords.x, coords.y, table.zoom * constants.radius, 0, 2 * Math.PI, false);
		ctx.fillStyle = constants.fill_style;
		ctx.fill();
	};

	definition.in = function(obj, point) {
		return (point.x - obj.kind_data.x) ** 2 + (point.y - obj.kind_data.y) ** 2 <= constants.radius ** 2;
	};

	definition.bounding_box = function(obj) {
		return {"min": {
			"x": obj.kind_data.x - constants.radius,
			"y": obj.kind_data.y - constants.radius
		}, "max": {
			"x": obj.kind_data.x + constants.radius,
			"y": obj.kind_data.y + constants.radius
		}};
	};

	definition.click = function(obj, point, callback) {
		clearTimeout(obj.kind_data.timeout);

		table_objects.del_local(obj.id, function() {
			table.draw();
			callback();
		});
	};

	object_definitions.register(definition);

	out.place = function(p) {
		server.beacon(p, campaigns.campaign.id, table.scene_id);
	};

	out.mouse_override = function() {
		table.mouse_override({
			"cursor": "crosshair",
			"click": function(p) {
				out.place(p);
				table.clear_mouse_override();
			}
		});
	};

	quick_menu.register("beacon", keymap.keycode("b"), out.mouse_override);

	return out;
}());
