/**
 * Handles display and interaction with the "table". (Scene items, zooming, moving the table itself, real-time updates, etc.)
 *
 * Dependencies
 * ============
 * campaigns
 * channel
 * function_registry
 * images
 * keymap
 * objects
 * quick_menu
 * sidepanel
 * modal
 */
var table = (function() {
	var canvas,
		ctx,
		handlers,
		constants,
		mouse_override = null,
		out = {};

	constants = {
		"zoomfactor": 1.3,
		"maxzoom": 1000,
		"minzoom": 5,
		"movefactor": 250,
		"snapfactor": 1.01
	};

	// Quick-menu items
	(function() {
		quick_menu.register("left", keymap.keycode("LEFT"), function() {
			out.move(-1, 0);
		});
		quick_menu.register("up", keymap.keycode("UP"), function() {
			out.move(0, -1);
		});
		quick_menu.register("right", keymap.keycode("RIGHT"), function() {
			out.move(1, 0);
		});
		quick_menu.register("down", keymap.keycode("DOWN"), function() {
			out.move(0, 1);
		});
		quick_menu.register("zoom in", keymap.keycode("+"), function() {
			out.zoom_by(1);
		});
		quick_menu.register("zoom out", keymap.keycode("-"), function() {
			out.zoom_by(-1);
		});
		quick_menu.register("toggle sidepanel", keymap.keycode("SPACE"), function() {
			sidepanel.toggle();
		});
		quick_menu.register("cancel mouse operation", keymap.keycode("ESCAPE"), function() {
			out.clear_mouse_override();
			out.draw();
		});
	}());

	handlers = (function() {
		var dragstart = null,
			moved = null,
			object_in = null,
			wheeltimer = null;

		function event_coords(e) {
			var p = {};

			if (e.pageX && e.pageY) {
				p.x = e.pageX;
				p.y = e.pageY;
			} else {
				p.x = e.clientX;
				p.y = e.clientY;
			}

			return p;
		}

		function point_handler(f) {
			return function(e) {
				var p = out.to_table_coords(event_coords(e));

				e.preventDefault();

				return f(e, p);
			};
		}

		return {
			// Window
			"resize": function() {
				canvas.width = canvas.parentElement.clientWidth;
				canvas.height = canvas.parentElement.clientHeight;
				out.draw();
			},

			// Keyboard
			"keydown": function(e) {
				if (!modal.is_visible) {
					quick_menu.key(e);
				}
			},

			// Desktop
			"wheel": point_handler(function(e, p) {
				var obj = table_objects.at(p),
					amt = (e.deltaY < 0 ? 1 : -1),
					def;

				if (wheeltimer === null) {
					wheeltimer = setTimeout(function() {
						wheeltimer = null;
					}, 50);
				} else {
					return;
				}

				// Maybe do I want to remove the capability of letting objects handle wheel events?
				if (obj !== null && obj !== undefined) {
					def = object_definitions.definition(obj);
					if (def.wheel !== undefined) {
						object_definitions.definition(obj).wheel(obj, amt);
						out.draw();
						return;
					}
				}

				out.zoom_by(amt);
			}),
			"mousedown": point_handler(function(e, p) {
				moved = false;
				dragstart = p;
				object_in = table_objects.at(p);
			}),
			"mousemove": point_handler(function(e, p) {
				var def,
					obj,
					cursor;

				if (mouse_override !== null && mouse_override.cursor !== null) {
					cursor = mouse_override.cursor;
				}

				moved = true;

				if (dragstart !== null && mouse_override !== null && mouse_override.drag !== undefined) {
					if (mouse_override.drag(p, dragstart) !== false) {
						return;
					}
				}

				if (object_in !== null) {
					def = object_definitions.definition(object_in);
					if (def.drag === undefined) {
						object_in = null;
					} else {
						if (def.drag(object_in, p, dragstart) !== false) {
							out.draw();
							return;
						}
						object_in = null;
					}
				}

				if (dragstart === null) {
					if (cursor === undefined) {
						cursor = "grab";
						obj = table_objects.at(p);
						if (obj !== null) {
							def = object_definitions.definition(obj);
							if (def.cursor === undefined) {
								cursor = "pointer";
							} else {
								cursor = def.cursor(obj);
							}
						}
					}
					canvas.style.cursor = cursor;
					return;
				}

				out.location.x += dragstart.x - p.x;
				out.location.y += dragstart.y - p.y;

				out.draw();
			}),
			"mouseup": point_handler(function(e, p) {
				var def,
					dsmm = dragstart !== null && mouse_override !== null;

				if (dsmm && moved === false && mouse_override.click !== undefined) {
					mouse_override.click(p);
				} else if (dsmm && moved === true && mouse_override.drag_end !== undefined) {
					mouse_override.drag_end();
				} else if (object_in !== null) {
					def = object_definitions.definition(object_in);
					if (moved === false) {
						if (def.click !== undefined) {
							def.click(object_in, p, out.draw);
						}
					} else {
						if (def.drag_end !== undefined) {
							def.drag_end(object_in, out.draw);
						}
					}
				}

				object_in = null;
				dragstart = null;
				moved = null;
			}),
			"mouseout": function() {
				object_in = null;
				dragstart = null;
				moved = null;
			},

			// Mobile
			/*
			"pointerdown": function(e) {
			},
			"pointermove": function(e) {
			},
			"pointerup": function(e) {
			},
			"pointercancel": function(e) {
			},
			"pointerleave": function(e) {
			},
			*/
		};
	}());

	// loading/initialization.
	(function() {
		var handlers_registered = false,
			loaded_callback = null;

		function register_event_handlers() {
			if (handlers_registered) {
				return;
			}
			handlers_registered = true;

			window.addEventListener("resize", handlers.resize);
			canvas.addEventListener("wheel", handlers.wheel);
			window.addEventListener("keydown", handlers.keydown);
			canvas.addEventListener("mousedown", handlers.mousedown);
			canvas.addEventListener("mousemove", handlers.mousemove);
			canvas.addEventListener("mouseup", handlers.mouseup);
			canvas.addEventListener("mouseout", handlers.mouseout);
		}

		channel.register_handler("sceneset", function(name, data) {
			if (canvas === undefined) {
				canvas = document.getElementById("table_canvas");
				ctx = canvas.getContext("2d");
			}
			if (out.scene_id === undefined) {
				server.subscribe_scene(channel.key, data);
			} else if (campaigns.campaign.scene_id === out.scene_id) {
				out.load_scene(data);
			}
			campaigns.campaign.scene_id = data;
		});
		function scene_init_start() {
			var loading = document.getElementById("table_loading");

			out.scene_id = undefined;
			table_objects.empty();

			canvas.style.display = "none";
			loading.style.display = "block";

			out.scene_init_start.call_functions();
		}
		channel.register_handler("sceneinitstart", scene_init_start);
		function loaded() {
			var loading = document.getElementById("table_loading");

			register_event_handlers();
			out.location = {"x": 0, "y": 0};

			canvas.style.display = "block";
			loading.style.display = "none";

			handlers.resize();

			if (loaded_callback !== null && loaded_callback !== undefined) {
				loaded_callback();
			}
		}
		channel.register_handler("sceneinitdone", function(name, scene_id) {
			out.scene_id = scene_id;
			table_objects.collection_id = scene_id;
			if (campaigns.loaded) {
				out.scene_loaded.call_functions(function() {
					loaded();
				});
			}
		});
		campaigns.campaign_loaded.register(function() {
			if (out.scene_id !== undefined) {
				loaded();
			}
		});

		function scene_loading(callback) {
			loaded_callback = callback;
			scene_init_start();
		}
		// Just for me.
		out.load_scene = function(scene_id, callback) {
			scene_loading(callback);
			server.subscribe_scene(channel.key, scene_id);
		};
		// For everybody.
		out.set_scene = function(scene_id, callback) {
			scene_loading(callback);
			server.set_scene(campaigns.campaign.id, scene_id);
		};
	}());

	channel.register_handler("newobject", function(name, obj) {
		out.new_object.call_functions(function() {
			table_objects.create_local(obj);
			if (out.scene_id !== undefined) {
				out.draw();
			}
		}, obj);
	});
	channel.register_handler("changeobject", function(name, obj) {
		table_objects.save_local(obj);
		if (out.scene_id !== undefined) {
			out.draw();
		}
	});
	channel.register_handler("delobject", function(name, id) {
		table_objects.del_local(id);
		if (out.scene_id !== undefined) {
			out.draw();
		}
	});

	out.zoom = 40; // 1 grid unit is 40 pixels.

	// An amt of 1 zooms in, -1 zooms out.
	out.zoom_by = function(amt) {
		out.zoom *= constants.zoomfactor ** amt;
		out.zoom = Math.max(constants.minzoom, Math.min(constants.maxzoom, out.zoom));

		out.draw();
	};

	// Moves the current location by x
	out.move = function(x, y) {
		var dist = constants.movefactor / out.zoom;
		out.location.x += x * dist;
		out.location.y += y * dist;

		out.draw();
	};

	out.draw = function() {
		var i;

		out.before_draw.call_functions();

		ctx.clearRect(0, 0, canvas.clientWidth, canvas.clientHeight);

		table_objects.iterate(function(obj) {
			object_definitions.definition(obj).draw(obj, ctx);
		});

		out.after_draw.call_functions();
	};

	out.snap_to_bounding = function() {
		var bb = {"min": {
				"x": Number.MAX_VALUE,
				"y": Number.MAX_VALUE
			}, "max": {
				"x": -Number.MAX_VALUE,
				"y": -Number.MAX_VALUE
			}},
			calculated = false;

		table_objects.iterate(function(obj) {
			var obb = object_definitions.definition(obj).bounding_box(obj);
			bb.min.x = Math.min(bb.min.x, obb.min.x);
			bb.min.y = Math.min(bb.min.y, obb.min.y);
			bb.max.x = Math.max(bb.max.x, obb.max.x);
			bb.max.y = Math.max(bb.max.y, obb.max.y);
			calculated = true;
		});

		if (!calculated) {
			return;
		}

		out.location = {"x": (bb.min.x + bb.max.x) / 2, "y": (bb.min.y + bb.max.y) / 2};
		out.zoom = Math.min(
			canvas.clientWidth / ((bb.max.x - bb.min.x) * constants.snapfactor),
			canvas.clientHeight / ((bb.max.y - bb.min.y) * constants.snapfactor)
		);
	};

	out.place_location = function(dims, z) {
		var intersects = [],
			unconnected = [],
			candidates;
		function inbox(pt, box) {
			return box.min.x < pt.x && box.max.x > pt.x && box.min.y < pt.y && box.max.y > pt.y;
		}
		function boxesintersect(box1, box2) {
			return box1.max.x > box2.min.x && box1.min.x < box2.max.x && box1.max.y > box2.min.y && box1.min.y < box2.max.y;
		}
		(function() {
			table_objects.iterate(function(obj) {
				var def = object_definitions.definition(obj),
					bb;
				if (def.z() > z && def.client_only !== true) {
					bb = def.bounding_box(obj);
					bb.min.x -= dims.width / 2;
					bb.max.x += dims.width / 2;
					bb.min.y -= dims.height / 2;
					bb.max.y += dims.height / 2;
					if (inbox(out.location, bb)) {
						intersects.push(bb);
						//directcount++;
					} else {
						unconnected.push(bb);
					}
				}
			});
		}());
		if (intersects.length === 0) {
			return out.location;
		}
		(function() {
			var i,
				j,
				mods = true,
				bb;
			while (mods) {
				mods = false;
				for (i = 0 ; i < unconnected.length ; i++) {
					for (j = 0 ; j < intersects.length ; j++) {
						if (boxesintersect(unconnected[i], intersects[j])) {
							bb = unconnected[i];
							unconnected.splice(i, 1);
							intersects.push(bb);
							i--;
							mods = true;
							break;
						}
					}
				}
			}
		}());
		unconnected = null; // We don't need the unconnected ones any more. Might as well let it be garbage collected.
		(function() { // Not really necessary, but will probably speed up later steps. Also, doesn't account for objects completely contained in the union of two boxes.
			var i,
				j;
			function contained(a, b) { // Return true if box b is contained within box a.
				return b.min.x >= a.min.x && b.max.x <= a.max.x && b.min.y >= a.min.y && b.max.y <= a.max.y;
			}
			for (i = 0 ; i < intersects.length ; i++) {
				for (j = 0 ; j < intersects.length ; j++) {
					if (i === j) {
						continue;
					}
					if (contained(intersects[i], intersects[j])) {
						if (i > j) {
							i--;
						}
						intersects.splice(j, 1);
						j--;
					}
				}
			}
		}());
		candidates = (function() {
			var mods = true,
				cs = {
					"top": out.location.y,
					"right": out.location.x,
					"bottom": out.location.y,
					"left": out.location.x,
				},
				i;
			while (mods) {
				mods = false;
				for (i = 0 ; i < intersects.length ; i++) {
					if (inbox({"x": out.location.x, "y": cs.top}, intersects[i])) {
						cs.top = intersects[i].min.y;
						mods = true;
					}
					if (inbox({"x": cs.right, "y": out.location.y}, intersects[i])) {
						cs.right = intersects[i].max.x;
						mods = true;
					}
					if (inbox({"x": out.location.x, "y": cs.bottom}, intersects[i])) {
						cs.bottom = intersects[i].max.y;
						mods = true;
					}
					if (inbox({"x": cs.left, "y": out.location.y}, intersects[i])) {
						cs.left = intersects[i].min.x;
						mods = true;
					}
				}
			}
			return [
				{"x": out.location.x, "y": cs.top},
				{"x": cs.right, "y": out.location.y},
				{"x": out.location.x, "y": cs.bottom},
				{"x": cs.left, "y": out.location.y}
			];
		}());
		candidates = candidates.concat(function() {
			var corners,
				cs = [],
				inside,
				i,
				j;
			for (i = 0 ; i < intersects.length ; i++) {
				for (j = 0 ; j < intersects.length ; j++) {
					if (i === j || !boxesintersect(intersects[i], intersects[j])) {
						continue;
					}
					inside = {};
					inside.top = intersects[j].min.y > intersects[i].min.y;
					inside.right = intersects[j].max.x < intersects[i].max.x;
					inside.bottom = intersects[j].max.y < intersects[i].max.y;
					inside.left = intersects[j].min.x > intersects[i].min.x;
					if (inside.top && !inside.right) {
						cs.push({"x": intersects[i].max.x, "y": intersects[j].min.y});
					}
					if (inside.top && !inside.left) {
						cs.push({"x": intersects[i].min.x, "y": intersects[j].min.y});
					}
					if (inside.bottom && !inside.right) {
						cs.push({"x": intersects[i].max.x, "y": intersects[j].max.y});
					}
					if (inside.bottom && !inside.left) {
						cs.push({"x": intersects[i].min.x, "y": intersects[j].max.y});
					}
				}
			}
			for (i = 0 ; i < cs.length ; i++) {
				for (j = 0 ; j < intersects.length ; j++) {
					if (inbox(cs[i], intersects[j])) {
						cs.splice(i, 1);
						i--;
						break;
					}
				}
			}
			return cs;
		}());
		return (function() {
			var i,
				d,
				mindist = Number.MAX_VALUE,
				minpt;
			function dist(pt1, pt2) {
				return Math.sqrt((pt2.x - pt1.x) ** 2 + (pt2.y - pt1.y) ** 2);
			}
			for (i = 0 ; i < candidates.length ; i++) {
				d = dist(out.location, candidates[i]);
				if (d < mindist) {
					mindist = d;
					minpt = candidates[i];
				}
			}
			return minpt;
		}());
	};

	out.to_canvas_coords = function(loc) {
		return {
			"x": (loc.x - out.location.x) * out.zoom + canvas.clientWidth / 2,
			"y": (loc.y - out.location.y) * out.zoom + canvas.clientHeight / 2
		};
	};

	out.to_table_coords = function(loc) {
		return {
			"x": (loc.x - canvas.clientWidth / 2) / out.zoom + out.location.x,
			"y": (loc.y - canvas.clientHeight / 2) / out.zoom + out.location.y
		};
	};

	out.min_coords = function() {
		return out.to_table_coords({"x": 0, "y": 0});
	};

	out.max_coords = function() {
		return out.to_table_coords({"x": canvas.clientWidth - 1, "y": canvas.clientHeight - 1});
	};

	/*
	Registers an "override" for mouse events. Properties:
	 - cursor - Mouse cursor. The css value of the "cursor" property.
	 - handle - Called on mousedown. Takes the point clicked (in table coords, not canvas coords).
	*/
	out.mouse_override = function(o) {
		mouse_override = o;
	};
	out.clear_mouse_override = function() {
		if (mouse_override !== null && mouse_override.clear !== undefined) {
			mouse_override.clear();
		}
		mouse_override = null;
	};

	out.scene_init_start = function_registry.create();
	out.scene_loaded = function_registry.create();
	out.before_draw = function_registry.create();
	out.after_draw = function_registry.create();
	out.new_object = function_registry.create();

	return out;
})();
