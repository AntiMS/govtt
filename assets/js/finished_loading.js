/**
 * Un-hides the page contents.
 *
 * Dependencies
 * ============
 * login
 * campaigns # So this comes after campaigns?
 */
login.login_checked.register(function() {
	document.getElementById("hide_wrapper").style.display = "block";
	document.getElementById("login_username").focus(); // Hack
});
