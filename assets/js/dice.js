/**
 * Handles public dice rolls.
 *
 * Dependencies
 * ============
 * campaigns
 * keymap
 * modal
 * quick_menu
 * server
 */
var dice = (function() {
	var out = {};

	out.show = function () {
		modal.modal(function(elem, onshow) {
			var child,
				textbox;

			function dofocus() {
				textbox.focus();
				textbox.selectionStart = textbox.selectionEnd = textbox.value.length;
			}

			function submit() {
				var err = document.getElementById("rolldice_error");

				while (err.firstChild) {
					err.removeChild(err.firstChild);
				}

				server.rolldice(
					textbox.value,
					campaigns.campaign.id,
					modal.hide,
					function(msg) {
						err.appendChild(document.createTextNode(msg));
					}
				);
			}

			elem.classList.add("rolldice");

			child = document.createElement("label");
			child.appendChild(document.createTextNode("Expression"));
			child.for = "expression";
			elem.appendChild(child);

			child = document.createElement("input");
			child.type = "text";
			child.name = "expression";
			child.value = "1d20";
			child.id = "rolldice_expression";
			child.addEventListener("keydown", function(e) {
				if (e.keyCode === 13) {
					submit();
				}
			});
			elem.appendChild(child);

			textbox = child;

			child = document.createElement("button");
			child.appendChild(document.createTextNode("exit"));
			child.classList.add("rolldice_dialog_button");
			child.addEventListener("click", function(e) {
				e.preventDefault();

				modal.hide();
			});
			elem.appendChild(child);

			child = document.createElement("button");
			child.appendChild(document.createTextNode("roll"));
			child.classList.add("rolldice_dialog_button");
			child.addEventListener("click", function(e) {
				e.preventDefault();

				submit();
			});
			elem.appendChild(child);

			child = document.createElement("div");
			child.id = "rolldice_error";
			elem.appendChild(child);

			child = document.createElement("div");
			(function() {
				var buttons = [
					["b(1d20,1d20)", "w(1d20,1d20)", "1d20+1d4", "1d20+1d6"],
					["1d4", "1d6", "1d8", "1d10", "1d12"],
					["2d4", "2d6", "2d8", "2d10", "2d12"],
					["1d100"]],
					r,
					c,
					rc,
					bc;

				function click_handler(e) {
					e.preventDefault();

					textbox.value = this.dataset.expression;
					dofocus();
				}

				for (r = 0 ; r < buttons.length ; r++) {
					rc = document.createElement("div");
					for (c = 0 ; c < buttons[r].length ; c++) {
						bc = document.createElement("button");
						bc.appendChild(document.createTextNode(buttons[r][c]));
						bc.dataset.expression = buttons[r][c];
						bc.addEventListener("click", click_handler);
						rc.appendChild(bc);
					}
					child.appendChild(rc);
				}
			}());
			elem.appendChild(child);

			onshow(dofocus);
		});
	};

	quick_menu.register("roll dice", keymap.keycode("d"), out.show);

	return out;
}());
