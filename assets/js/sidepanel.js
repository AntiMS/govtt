/**
 * Handles side-panel interface.
 *
 * Dependencies
 * ============
 * campaigns
 * hash_parse
 */
var sidepanel = (function() {
	var sidepanel,
		title,
		back_button,
		buttons,
		content,
		definitions = [],
		current_definition = null,
		out = {};

	function back() {
		var button_elems = buttons.getElementsByTagName("button"),
			i;
		back_button.style.visibility = "hidden";
		content.style.display = "none";
		buttons.style.display = "block";
		content.className = '';
		while (title.firstChild) {
			title.removeChild(title.firstChild);
		}
		while (content.firstChild) {
			content.removeChild(content.firstChild);
		}
		current_definition = null;
		for (i = 0 ; i < button_elems.length ; i++) {
			button_elems[i].blur();
		}
	}

	campaigns.campaign_loaded.register(function() {
		var i,
			sidepanel_button = document.getElementById("sidepanel_button"),
			button;

		if (hash_parse.get_parameter("remove_sidepanel_button") !== undefined) {
			sidepanel_button.parentNode.removeChild(sidepanel_button);
			return;
		}

		sidepanel = document.getElementById("sidepanel");
		title = document.getElementById("sidepanel_title");
		back_button = document.getElementById("sidepanel_back_button");
		buttons = document.getElementById("sidepanel_buttons");
		content = document.getElementById("sidepanel_content");

		back_button.addEventListener("click", back);

		sidepanel_button.addEventListener("click", function(e) {
			e.preventDefault();
			if (e.relatedTarget) {
				// Revert focus back to previous blurring element
				e.relatedTarget.focus();
			} else {
				// No previous focus target, blur instead
				e.currentTarget.blur();
			}
			out.toggle();
		});

		function click_handler() {
			out.goto(Array.prototype.indexOf.call(this.parentNode.children, this));
		}

		for (i = 0 ; i < definitions.length ; i++) {
			button = document.createElement("button");
			button.appendChild(document.createTextNode(definitions[i].title));
			button.addEventListener("click", click_handler);
			buttons.appendChild(button);
		}
	});

	out.goto = function(def) {
		var i;

		if (typeof def === "string" || typeof def === "number") {
			def = out.get_definition(def);
		}

		out.show();

		def.populate(sidepanel_content);
		title.appendChild(document.createTextNode(def.title));
		back_button.style.visibility = "visible";
		content.style.display = "block";
		buttons.style.display = "none";

		current_definition = def;
	};

	out.hide = function() {
		sidepanel.style.display = "none";
		back();
	};

	out.show = function() {
		sidepanel.style.display = "block";
		back();
	};

	out.register_section = function(def) {
		definitions.push(def);
		quick_menu.register(def.name, def.key, function() {
			out.goto(def);
		});
	};

	out.refresh = function() {
		var def;
		if (current_definition != null) {
			def = current_definition;
			back();
			out.goto(def);
		}
	};

	out.toggle = function() {
		if (sidepanel.style.display === "block") {
			out.hide();
		} else {
			back();
			sidepanel.style.display = "block";
		}
	};

	out.get_definition = function(def) {
		var i;

		if (typeof def === "string") {
			for (i = 0 ; i < definitions.length ; i++) {
				if (def === definitions[i].name) {
					return definitions[i];
				}
			}
		} else if (typeof def === "number") {
			return definitions[def];
		}

		return null;
	};

	return out;
}());
