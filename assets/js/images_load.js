/**
 * Handles loading images when they're needed.
 *
 * Dependencies
 * ============
 * campaigns
 * hash_parse
 * images
 * objects
 * table
 */
var images_load = (function() {
	var out = {};

	function get_image_ids(objects) {
		var image_ids = [];
		objects.iterate(function(obj) {
			if (obj.kind_data.image_id !== undefined) {
				image_ids.push(obj.kind_data.image_id);
			}
		});
		return [...new Set(image_ids)];
	}

	/**
	 * Load all pocket objects when the campaign is loaded.
	 */
	campaigns.campaign_loaded.register(function() {
		var ids;
		if (hash_parse.get_parameter("skip_preload_my_images") !== undefined) {
			return;
		}
		ids = get_image_ids(pocket_objects);
		images.iterate(function(img) {
			if (img.mine && !img.archived) {
				ids.push(img.id);
			}
		});
		images.load.apply(null, [null].concat(ids));
	});

	function image_handler(img) {
		if (img.mine && !img.archived) {
			images.load(null, img.id);
		}
	}
	function object_handler(callback, obj) {
		if (obj.kind_data.image_id !== undefined) {
			images.load(callback, obj.kind_data.image_id);
		}
	}

	table.scene_loaded.register_async(function(callback) {
		images.load.apply(null, [callback].concat(get_image_ids(table_objects)));
		images.image_added.register(image_handler);
		images.image_changed.register(image_handler);
		table.new_object.register_async(object_handler);
	});

	table.scene_init_start.register(function() {
		images.image_added.unregister(image_handler);
		images.image_changed.unregister(image_handler);
		table.new_object.unregister(object_handler);
	});

	return out;
}());
