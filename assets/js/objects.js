/**
 * Handles a collection of objects.
 *
 * Dependencies
 * ============
 * server
 */
var table_objects,
	pocket_objects,
	object_definitions;

(function() {
	object_definitions = (function() {
		var definitions = {},
			out = {};

		out.register = function(definition) {
			definitions[definition.name] = definition;
		};

		out.definition = function(kind) {
			if (typeof kind !== "string") {
				kind = kind.kind;
			}

			return definitions[kind];
		};

		return out;
	}());

	function objects(create, load_source) {
		var list,
			ids,
			out = {};

		if (load_source === undefined) {
			load_source = function() {
				console.log("Warning: objects set with no load source loaded");
			};
		}

		function sort() {
			list.sort(function(a, b) {
				return object_definitions.definition(a).z(a) - object_definitions.definition(b).z(b);
			});
		}

		function populate_ids() {
			var i;

			ids = {};

			for (i = 0 ; i < list.length ; i++) {
				ids[list[i].id] = i;
			}
		}

		out.load = function(id, callback) {
			load_source(id, function(objs) {
				list = objs;
				out.collection_id = id;
				sort();
				populate_ids();
				callback();
			});
		};

		out.iterate = function(callback, reverse) {
			var i,
				test,
				incr;

			if (reverse === undefined) {
				reverse = false;
			}

			if (reverse) {
				i = list.length - 1;
				test = function() { return i >= 0; };
				incr = function() { i--; };

			} else {
				i = 0;
				test = function() { return i < list.length; };
				incr = function() { i++; };
			}

			for ( ; test() ; incr()) {
				if (callback(out.copy(list[i])) === false) {
					return;
				}
			}
		};

		out.at = function(coords) {
			var i,
				ret = null;

			out.iterate(function(obj) {
				if (object_definitions.definition(obj).in(obj, coords)) {
					ret = obj;
					return false;
				}
			}, true);

			return ret;
		};

		out.save_local = function(obj) {
			if (ids[obj.id] !== undefined) {
				list[ids[obj.id]] = obj;
			} else {
				ids[obj.id] = list.length;
				list.push(obj);
			}
			sort();
			populate_ids();
		};

		out.save = function(obj, callback) {
			if (typeof obj !== "number") {
				out.save_local(obj);
				obj = obj.id;
			}

			obj = list[ids[obj]];

			server.save_object(obj, callback);
		};

		out.create_local = function(obj) {
			if (ids[obj.id] !== undefined) {
				return;
			}

			list.push(obj);
			sort();
			populate_ids();
		};

		out.create = function(obj, callback) {
			var def = object_definitions.definition(obj);

			if (def.create_override !== undefined) {
				if (def.create_override(obj, out, callback) !== false) {
					return;
				}
			}
			create(obj, out.collection_id, function(id) {
				obj.id = id;
				out.create_local(obj);

				if (callback !== null && callback !== undefined) {
					callback(obj);
				}
			});
		};

		out.del_local = function(id, callback) {
			var k;

			if (ids[id] === undefined) {
				if (callback !== null && callback !== undefined) {
					callback();
				}
				return;
			}

			list.splice(ids[id], 1);
			for (k in ids) {
				if (ids[k] > ids[id]) {
					ids[k]--;
				}
			}
			delete ids[id];
			if (callback !== null && callback !== undefined) {
				callback();
			}
		};

		out.del = function(id, callback) {
			if (typeof id !== "number") {
				id = id.id;
			}
			server.delete_object(id, function() {
				out.del_local(id, callback);
			});
		};

		out.copy = function(obj) {
			var k,
				v,
				ret;

			if (typeof obj !== "object" || obj === null) {
				return obj;
			}

			ret = Array.isArray(obj) ? [] : {};

			for (k in obj) {
				v = out.copy(obj[k]);
				ret[k] = v;
			}

			return ret;
		};

		out.empty = function() {
			list = [];
			ids = {};
		};

		out.by_id = function(id) {
			var out = ids[id];
			if (out === undefined) {
				return null;
			}
			return list[out];
		};

		out.new_id = function() {
			var i = -1;

			while (ids[i] !== undefined) {
				i--;
			}

			return i;
		};

		return out;
	}

	table_objects = objects(server.new_table_object);
	pocket_objects = objects(server.new_pocket_object, server.pocket);
}());
