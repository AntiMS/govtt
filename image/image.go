/*
Manages storing and serving images. Images are stored in the directory "uploads/images" relative to the current working directory and named base-10 numbers corresponding with the image's id in the database.

HTTP Handlers
=============
This package registers several HTTP handlers detailed below:

/image/<image_id>
-----------------
Serves the content of an image to the client (only if the user has permissions on the campaign it is associated with).

Method: GET

Path parameters:

- image_id -- The id of the image.

The response content is the raw binary image data (PNG, JPG, GIF, etc data).

/images/fetch/<campaign_id>
---------------------------
Fetches an image from the URL given in the post body and associates it with the given campaign. The url may be of any of the following schemes: "http:", "https:", "data:". Only base64 data urls are supported. Data urls are urldecoded transparently if necessary.

Method: POST

Path parameters:

- campaign_id -- The id of the Campaign to which to associate the fetched image.

Response JSON structure:

	<id as an int>

The returned id is the id of the newly-fetched image.

This call, when successful, also sends a message to the default bus of the form:

	&bus.Message{Name: []string{"campaign", "<campaign_id>", "campaignevent", "newimage"}, Value: []map{"id": <image_id>, "name": "<image_name>", "user_id": <user_id>, "archived": <archived>})

/images/rename/<image_id>
-------------------------
Renames an image.

Method: POST

Path parameters:

- image_id -- The id of the Image to rename.

The POST body should just be the new name.

A successful response is a "204 No Content" response.

This call, when successful, also sends a message to the default bus of the form:

	&bus.Message{Name: []string{"campaign", "<campaign_id>", "campaignevent", "imagechanged"}, Value: []map{"id": <image_id>, "name": "<image_name>", "user_id": <user_id>, "archived": <archived>})

/images/archive/<image_id>
--------------------------
Archives an image

Method: POST

Path parameters:

- image_id -- The id of the Image to archive.

The POST body should be empty.

A successful response is a "204 No Content" response.

This call, when successful, also sends a message to the default bus of the form:

	&bus.Message{Name: []string{"campaign", "<campaign_id>", "campaignevent", "imagechanged"}, Value: []map{"id": <image_id>, "name": "<image_name>", "user_id": <user_id>, "archived": <archived>})

/images/unarchive/<image_id>
----------------------------
Unarchives an image

Method: POST

Path parameters:

- image_id -- The id of the Image to unarchive.

The POST body should be empty.

A successful response is a "204 No Content" response.

This call, when successful, also sends a message to the default bus of the form:

	&bus.Message{Name: []string{"campaign", "<campaign_id>", "campaignevent", "imagechanged"}, Value: []map{"id": <image_id>, "name": "<image_name>", "user_id": <user_id>, "archived": <archived>})
*/
package image

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"gitlab.com/AntiMS/govtt/auth"
	"gitlab.com/AntiMS/govtt/bus"
	"gitlab.com/AntiMS/govtt/initdb"
)

// Taken directly from https://stackoverflow.com/questions/25959386/how-to-check-if-a-file-is-a-valid-image
var magicTable = map[string]string{
	"\xff\xd8\xff":      "image/jpeg",
	"\x89PNG\r\n\x1a\n": "image/png",
	"GIF87a":            "image/gif",
	"GIF89a":            "image/gif",
}

const MAGIC_LEN = 8

var imagesDir string = filepath.Join("uploads", "images")

func serveImage(w http.ResponseWriter, r *http.Request, u auth.User) {
	split := strings.Split(r.URL.Path, "/")
	if len(split) < 3 {
		http.Error(w, "Invalid request.", http.StatusBadRequest)
		return
	}
	idStr := split[2]
	imageId, err := strconv.Atoi(idStr)
	if err != nil {
		http.Error(w, "Invalid request.", http.StatusBadRequest)
		return
	}

	query := `
		SELECT id FROM image WHERE id = ? AND campaign_id in (
			SELECT id FROM campaign WHERE id IN (
				SELECT campaign_id FROM campaign_user WHERE user_id = ?
			)
		);`
	rows, err := initdb.DB.Query(query, imageId, u.Id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	defer rows.Close()

	if !rows.Next() {
		http.Error(w, "Image not found.", http.StatusNotFound)
		return
	}

	rows.Close()

	http.ServeFile(w, r, filepath.Join(imagesDir, idStr))
}

func GetImages(campaignId int) ([]map[string]any, error) {
	query := "SELECT id, name, user_id, archived FROM image WHERE campaign_id = ?;"
	rows, err := initdb.DB.Query(query, campaignId)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	out := make([]map[string]any, 0)
	var id int
	var name string
	var userId int
	var archived bool
	for rows.Next() {
		err = rows.Scan(&id, &name, &userId, &archived)
		if err != nil {
			return nil, err
		}

		m := make(map[string]any)
		m["id"] = id
		m["name"] = name
		m["user_id"] = userId
		m["archived"] = archived
		out = append(out, m)
	}

	return out, nil
}

func fetchImage(w http.ResponseWriter, r *http.Request, u auth.User) {
	if r.Method != "POST" {
		http.Error(w, "Please use POST.", http.StatusMethodNotAllowed)
		return
	}

	err := os.MkdirAll(imagesDir, 0755)
	if err != nil {
		http.Error(w, "Could not create images dir. "+err.Error(), http.StatusInternalServerError)
		return
	}

	split := strings.Split(r.URL.Path, "/")
	if len(split) < 4 {
		http.Error(w, "Invalid request path; required campaign id.", http.StatusBadRequest)
		return
	}

	campaignIdStr := split[3]
	campaignId, err := strconv.Atoi(campaignIdStr)
	if err != nil {
		http.Error(w, "Invalid request. "+err.Error(), http.StatusBadRequest)
		return
	}

	query := `
		SELECT user_id FROM campaign_user WHERE campaign_id = ? AND user_id = ?;`
	rows, err := initdb.DB.Query(query, campaignId, u.Id)
	if err != nil {
		http.Error(w, "Database error checking authentication. "+err.Error(), http.StatusInternalServerError)
		return
	}

	defer rows.Close()

	if !rows.Next() {
		http.Error(w, "Unauthorized.", http.StatusInternalServerError)
		return
	}

	rows.Close()

	body, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "Error reading body. "+err.Error(), http.StatusInternalServerError)
		return
	}

	var req map[string]string

	err = json.Unmarshal(body, &req)
	if err != nil {
		http.Error(w, "Error parsing body. "+err.Error(), http.StatusInternalServerError)
		return
	}

	ur, err := url.Parse(req["url"])
	if err != nil {
		http.Error(w, "Error requesting image. "+err.Error(), http.StatusInternalServerError)
		return
	}

	var reader io.Reader
	if ur.Scheme == "data" {
		decoded, err := url.PathUnescape(ur.Opaque)
		if err != nil {
			decoded = ur.Opaque
		}
		split := strings.Split(decoded, ",")
		encoding := strings.Split(split[0], ";")[1]
		if strings.ToLower(encoding) != "base64" {
			http.Error(w, "Unrecognized data url encoding.", http.StatusInternalServerError)
			return
		}
		data64 := split[1]
		data, err := base64.StdEncoding.DecodeString(data64)
		if err != nil {
			http.Error(w, "Couldn't base64 decode data url. "+err.Error(), http.StatusInternalServerError)
			return
		}
		reader = bytes.NewBuffer(data)
	} else if ur.Scheme == "http" || ur.Scheme == "https" {
		resp, err := http.Get(req["url"])
		if err != nil {
			http.Error(w, "Error requesting. "+err.Error(), http.StatusInternalServerError)
			return
		}
		if resp.StatusCode != http.StatusOK {
			http.Error(w, fmt.Sprintf("Couldn't fetch image. Got status code %d.", resp.StatusCode), http.StatusInternalServerError)
			return
		}
		reader = resp.Body
	}

	f, err := os.CreateTemp("tmp", "img")
	if err != nil {
		http.Error(w, "Unable to create a temporary file. "+err.Error(), http.StatusInternalServerError)
		return
	}

	header := make([]byte, MAGIC_LEN)
	n, err := reader.Read(header)
	if err != nil {
		http.Error(w, "Failed to read image data. "+err.Error(), http.StatusInternalServerError)
		return
	}
	if n < MAGIC_LEN {
		http.Error(w, fmt.Sprintf("Read only %d bytes, but needed %d.", n, MAGIC_LEN), http.StatusInternalServerError)
		return
	}
	var imageHeaderOk bool = false
	for magic, _ := range magicTable {
		if bytes.HasPrefix(header, []byte(magic)) {
			imageHeaderOk = true
		}
	}
	if !imageHeaderOk {
		http.Error(w, "File appears not to be a valid image.", http.StatusInternalServerError)
		return
	}
	n, err = f.Write(header)
	if err != nil {
		http.Error(w, "Failed to write image data. "+err.Error(), http.StatusInternalServerError)
		return
	}
	if n < MAGIC_LEN {
		http.Error(w, fmt.Sprintf("Wrote only %d bytes to file. Intended %d.", n, MAGIC_LEN), http.StatusInternalServerError)
		return
	}

	_, err = io.Copy(f, reader)
	if err != nil {
		http.Error(w, "Could not copy file contents. "+err.Error(), http.StatusInternalServerError)
		return
	}

	f.Close()

	res, err := initdb.DB.Exec("INSERT INTO image (user_id, campaign_id, name) VALUES(?, ?, ?);", u.Id, campaignId, req["name"])
	if err != nil {
		http.Error(w, "Failed to write to the database."+err.Error(), http.StatusInternalServerError)
		return
	}

	id64, err := res.LastInsertId()
	if err != nil {
		http.Error(w, "Failed to retrieve image id. "+err.Error(), http.StatusInternalServerError)
		return
	}

	id := int(id64)

	err = os.Rename(f.Name(), filepath.Join(imagesDir, strconv.Itoa(id)))
	if err != nil {
		http.Error(w, "Could not rename file. "+err.Error(), http.StatusInternalServerError)
		return
	}

	msg := make(map[string]any)
	msg["id"] = id
	msg["name"] = req["name"]
	msg["user_id"] = u.Id
	msg["archived"] = false
	bus.Publish(&bus.Message{Name: []string{"campaign", strconv.Itoa(campaignId), "campaignevent", "newimage"}, Value: msg})

	jsonId, err := json.Marshal(id)
	if err != nil {
		http.Error(w, "Failed to marshal response JSON."+err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Cache-Control", "no-store max-age=0")

	http.ServeContent(w, r, "fetched.json", time.Time{}, bytes.NewReader(jsonId))
}

func doChangeImage(w http.ResponseWriter, r *http.Request, field string, value any, u auth.User) {
	if r.Method != "POST" {
		http.Error(w, "Please use POST.", http.StatusMethodNotAllowed)
		return
	}

	split := strings.Split(r.URL.Path, "/")
	if len(split) < 4 {
		http.Error(w, "Invalid request path; required image id.", http.StatusBadRequest)
		return
	}

	imageIdStr := split[3]
	imageId, err := strconv.Atoi(imageIdStr)
	if err != nil {
		http.Error(w, "Invalid request. "+err.Error(), http.StatusBadRequest)
		return
	}

	res, err := initdb.DB.Exec("UPDATE image SET "+field+" = ? WHERE id = ? AND user_id = ?;", value, imageId, u.Id)
	if err != nil {
		http.Error(w, "Failed to update image. "+err.Error(), http.StatusInternalServerError)
		return
	}
	rowsAffected, err := res.RowsAffected()
	if err != nil {
		http.Error(w, "Failed to retrieve the number of rows affected."+err.Error(), http.StatusInternalServerError)
		return
	}
	if rowsAffected <= 0 {
		http.Error(w, "Image not found or unauthorized.", http.StatusUnauthorized)
		return
	}

	rows, err := initdb.DB.Query("SELECT id, name, user_id, archived, campaign_id FROM image WHERE id = ?;", imageId)
	if err != nil {
		http.Error(w, "Failed to retrieve image record from database. "+err.Error(), http.StatusInternalServerError)
		return
	}

	defer rows.Close()

	if !rows.Next() {
		http.Error(w, "Image not found after updating.", http.StatusInternalServerError)
		return
	}

	var id int
	var name string
	var userId int
	var archived bool
	var campaignId int
	err = rows.Scan(&id, &name, &userId, &archived, &campaignId)
	if err != nil {
		http.Error(w, "Failed to get image data from database. "+err.Error(), http.StatusInternalServerError)
		return
	}

	if rows.Next() {
		http.Error(w, "More than one image with id.", http.StatusInternalServerError)
		return
	}

	rows.Close()

	m := make(map[string]any)
	m["id"] = id
	m["name"] = name
	m["user_id"] = userId
	m["archived"] = archived

	bus.Publish(&bus.Message{Name: []string{"campaign", strconv.Itoa(campaignId), "campaignevent", "imagechanged"}, Value: m})

	w.WriteHeader(http.StatusNoContent)
}

func renameImage(w http.ResponseWriter, r *http.Request, u auth.User) {
	name, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "Failed to read request body. "+err.Error(), http.StatusInternalServerError)
		return
	}
	if len(name) == 0 {
		http.Error(w, "Name must not be empty.", http.StatusBadRequest)
		return
	}

	doChangeImage(w, r, "name", name, u)
}

func archiveImage(w http.ResponseWriter, r *http.Request, u auth.User) {
	doChangeImage(w, r, "archived", true, u)
}

func unarchiveImage(w http.ResponseWriter, r *http.Request, u auth.User) {
	doChangeImage(w, r, "archived", false, u)
}

func init() {
	http.HandleFunc("/image/", auth.DecorateHandlerFunc(serveImage))
	http.HandleFunc("/images/fetch/", auth.DecorateHandlerFunc(fetchImage))
	http.HandleFunc("/images/rename/", auth.DecorateHandlerFunc(renameImage))
	http.HandleFunc("/images/archive/", auth.DecorateHandlerFunc(archiveImage))
	http.HandleFunc("/images/unarchive/", auth.DecorateHandlerFunc(unarchiveImage))
}
